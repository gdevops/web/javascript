
.. index::
   pair: nvm ; Nodejs
   pair: zsh ; nvm


.. _nvm_nodejs:

============================================================================================
**nvm** (Node Version Manager) simple bash script to manage multiple active node.js versions
============================================================================================


-  https://github.com/creationix/nvm
- :ref:`zsh_nvm`


.. contents::
   :depth: 3


README.rst
===========

- https://github.com/creationix/nvm/blob/v0.39.0/README.md

.. include:: README.rst


My installation with zsh
==========================

.. seealso::

   - :ref:`zsh_nvm`

::

    sudo wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.39.0/install.sh | bash

::

    => Downloading nvm from git to '/home/pvergain/.nvm'
    => Clonage dans '/home/pvergain/.nvm'...
    remote: Enumerating objects: 278, done.
    remote: Counting objects: 100% (278/278), done.
    remote: Compressing objects: 100% (249/249), done.
    remote: Total 278 (delta 33), reused 88 (delta 16), pack-reused 0
    Réception d'objets: 100% (278/278), 142.36 KiB | 477.00 KiB/s, fait.
    Résolution des deltas: 100% (33/33), fait.
    => Compressing and cleaning up git repository

    => Appending nvm source string to /home/pvergain/.bashrc
    => Appending bash_completion source string to /home/pvergain/.bashrc
    => Close and reopen your terminal to start using nvm or run the
    following to use it now:

    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


::

    sudo git clone https://github.com/lukechilds/zsh-nvm ~/.oh-my-zsh/custom/plugins/zsh-nvm

::

    Clonage dans '/home/pvergain/.oh-my-zsh/custom/plugins/zsh-nvm'...
    remote: Enumerating objects: 1, done.
    remote: Counting objects: 100% (1/1), done.
    remote: Total 517 (delta 0), reused 1 (delta 0), pack-reused 516
    Réception d'objets: 100% (517/517), 68.56 KiB | 307.00 KiB/s, fait.
    Résolution des deltas: 100% (271/271), fait.


nvm upgrade
-------------

::

    nvm upgrade

::

    Installed version is v0.34.0
    Checking latest version of nvm...
    You're already up to date

nvm install node
-----------------


::

    ✦ ❯ nvm install node
    Downloading and installing node v17.0.0...
    Downloading https://nodejs.org/dist/v17.0.0/node-v17.0.0-linux-x64.tar.xz...
    ############################################################################################################################################################################################################ 100,0%
    Computing checksum with sha256sum
    Checksums matched!
    Now using node v17.0.0 (npm v8.1.0)
    Creating default alias: default -> node (-> v17.0.0)



::

    Downloading and installing node v12.0.0...
    Downloading https://nodejs.org/dist/v12.0.0/node-v12.0.0-linux-x64.tar.xz...
    ####################################################################################################### 100,0%
    Computing checksum with sha256sum
    Checksums matched!
    Now using node v12.0.0 (npm v6.9.0)
    Creating default alias: default -> node (-> v12.0.0)



::

    nvm install node

::

    Downloading and installing node v11.14.0...
    Downloading https://nodejs.org/dist/v11.14.0/node-v11.14.0-linux-x64.tar.xz...
    ###################################################################################################### 100,0%
    Computing checksum with sha256sum
    Checksums matched!
    Now using node v11.14.0 (npm v6.7.0)
    Creating default alias: default -> node (-> v11.14.0)


    nvm install node


which node
-----------

::

    which node

::

    /home/pvergain/.nvm/versions/node/v17.0.0/bin/node


which npm
-----------

::

    which npm

::

    /home/pvergain/.nvm/versions/node/v17.0.0/bin/npm


npm --version
-------------

::

    8.1.0


node --version
-----------------

::


    v17.0.0



Versions
==========


.. toctree::
   :maxdepth: 3

   versions/versions
