
.. index::
   pair: nvm ; versions


.. _nvm_versions:

=========================
nvm versions
=========================

.. seealso::

   - https://github.com/creationix/nvm/releases

.. toctree::
   :maxdepth: 3


   0.35.2/0.35.2
   0.34.0/0.34.0
   0.33.11/0.33.11
