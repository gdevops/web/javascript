.. Tuto Javascript documentation master file, created by
   sphinx-quickstart on Tue Mar 19 18:31:18 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



.. figure:: _static/JavaScript_logo.png
   :align: center

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/javascript/rss.xml>`_


.. _javascript_tuto:

=====================================================================
**Javascript (/ˈdʒɑːvəˌskrɪpt/) ECMAScript (ES) specification**
=====================================================================

- https://262.ecma-international.org/
- https://tc39.es/ecma262/
- https://github.com/tc39/ecma262
- https://github.com/tc39/ecma262/commits/main
- https://developer.mozilla.org/en-US/docs/Web/JavaScript
- https://fr.wikipedia.org/wiki/JavaScript
- https://overapi.com/javascript
- https://htmldom.dev/
- https://devdocs.io/javascript/
- https://es.discourse.group/
- https://javascriptstars.com/
- https://github.com/trending/developers/javascript?since=daily
- https://github.com/trending/javascript?since=daily&spoken_language_code=
- https://x.com/freeCodeCamp
- Last version: :ref:`es_last`


.. figure:: images/ecma_international.png
   :align: center

   https://262.ecma-international.org/

.. figure:: _static/javascript_third_age.png
   :align: center

   https://x.com/swyx/status/1263123032328925186?s=20


.. toctree::
   :maxdepth: 5

   news/news
   tutorials/tutorials
   doc/doc
   methods/methods
   installation/installation
   glossary/glossary
   versions/versions


TODO
=====

- https://zanderle.com/musings/2021/1/5-javascript-things-you-should-knowithunderstand-as-a-python-developer/
