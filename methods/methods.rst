.. index::
   pair: Methods ; Javascript
   pair: getElementById ; Javascript

.. _javascript_methods:

===============================================================
**Javascript methods**
===============================================================


.. toctree::
   :maxdepth: 3
   
   getElementById/getElementById   
