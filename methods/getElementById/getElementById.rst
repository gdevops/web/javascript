.. index::
   pair: getElementById ; Javascript

.. _getElementById:

==================================
**document.getElementById**
==================================

- https://developer.mozilla.org/fr/docs/Web/API/Document/getElementById

Examples
=============

.. code-block:: javascript

    function trt_widgets() {
        console.log(`trt_widgets() document.readyState = ${document.readyState}`);

        const url = '{% url "accounts:fetch_update_user" %}';
        document.getElementById('id_filtre_mes_chronos').onchange = function() {
            const parameters  = {'id': {{user.id}} };
            parameters.filtre_mes_chronos = document.getElementById('id_filtre_mes_chronos').checked;
            id3_fetch(url, parameters).then(data => {
                location.reload(true);
            })
        }
        document.getElementById('id_visu_cartes').onchange = function() {
            const parameters  = {'id': {{user.id}} };
            parameters.visu_cartes = document.getElementById('id_visu_cartes').checked;
            id3_fetch(url, parameters).then(data => {
                location.reload(true);
            })
        }

        try {
            document.getElementById('id_ordre_intervention').onchange = function() {
                const id_demande_article = document.getElementById('id_demande_article').textContent.trim();
                const parameters = {'id_demande_article': id_demande_article};
                const url = '{% url "articles:ajax_update_demande_article" %}';
                parameters.id_ordre_intervention = document.getElementById('id_ordre_intervention').value;
                id3_fetch(url, parameters).then(json => { document.getElementById('libelle_ordre_intervention').textContent = json.libelle_ordre_intervention;});
            }
            document.getElementById('id_carte').onchange = function() {
                const id_demande_article = document.getElementById('id_demande_article').textContent.trim();
                const parameters = {'id_demande_article': id_demande_article};
                const url = '{% url "articles:ajax_update_demande_article" %}';
                parameters.id_carte = document.getElementById('id_carte').value;
                id3_fetch(url, parameters).then(json => { document.getElementById('libelle_carte').textContent = json.libelle_carte;});
            }
            document.getElementById('id_projet').onchange = function() {
                const id_demande_article = document.getElementById('id_demande_article').textContent.trim();
                const parameters = {'id_demande_article': id_demande_article};
                const url = '{% url "articles:ajax_update_demande_article" %}';
                parameters.id_projet = document.getElementById('id_projet').value;
                id3_fetch(url, parameters).then(json => { document.getElementById('libelle_projet').textContent = json.libelle_projet;});
            }
        } catch (error) {
            ;
        }


