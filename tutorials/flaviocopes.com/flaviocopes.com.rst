
.. _flaviocopes:

============================================================================================
Flavio Copes (https://x.com/flaviocopes)
============================================================================================


.. seealso::

   - https://flaviocopes.com/
   - https://x.com/flaviocopes
   - https://www.youtube.com/channel/UCt0ya0xGvXu01zfXjNrGqzg
