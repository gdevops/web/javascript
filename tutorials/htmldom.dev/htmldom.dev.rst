.. index::
   pair: htmldom.dev ; Javascript

.. _htmldom.dev:

==========================================================================================================
**htmldom.dev** Common tasks of managing HTML DOM with **vanilla JavaScript**.
==========================================================================================================

.. seealso::

   - https://htmldom.dev/
   - https://x.com/nghuuphuoc
   - https://github.com/phuoc-ng/html-dom
   - https://github.com/phuoc-ng/html-dom/graphs/contributors
   - https://github.com/phuoc-ng/html-dom/commits/master


.. toctree::
   :maxdepth: 3

   README
