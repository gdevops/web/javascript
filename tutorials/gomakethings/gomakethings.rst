.. index::
   pair: gomakethings; Tutorial

.. _gomakethings:

============================================================================================
gomakethings **Vanilla JavaScript** Tutorial
============================================================================================

.. seealso::

   - https://vanillajstoolkit.com/
   - https://vanillajstoolkit.com/plugins/

.. toctree::
   :maxdepth: 3

   form/form
   callbacks/callbacks
   documentation/documentation
