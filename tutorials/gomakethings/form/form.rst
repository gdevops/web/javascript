.. index::
   pair: FormData ; Serializing

.. _formdata_serial:

============================================================================================
Serializing form data with the **vanilla JS FormData() object**
============================================================================================

.. seealso::

   - https://gomakethings.com/serializing-form-data-with-the-vanilla-js-formdata-object/
   - https://javascript.info/formdata
   - https://www.youtube.com/watch?v=GWJhE7Licjs&feature=emb_logo

.. contents::
   :depth: 3

Introduction
==============

Let’s say you have a form. When it’s submitted, you want to get the
values of all of the fields and submit them to an API.

.. code-block:: html

    <form>
        <label for="title">Title</label>
        <input type="text" name="title" id="title" value="Go to Hogwarts">

        <label for="body">Body</label>
        <textarea id="body" name="body">Learn magic, play Quidditch, and drink some butter beer.</textarea>

        <input type="hidden" name="userId" value="1">

        <button>Submit</button>
    </form>

How do you easily get the values of all of the fields ?

Today, we’re going to look at an easy, native way to do that: FormData().


Vidéo
=====

.. seealso::

   - https://www.youtube.com/watch?v=GWJhE7Licjs&feature=emb_logo
   - https://gist.github.com/prof3ssorSt3v3/7be8dd12f4d022932a3f700e0cef1841


Browser compatibility
=========================

The FormData() constructor works in all modern browsers, and IE10 and
above.

Unfortunately, iterators and the for...of method do not work in IE at all,
and cannot be polyfilled.

Next week, we’ll look at some more backwards compatible ways to
serialize form data into arrays, objects, and search parameter strings.
