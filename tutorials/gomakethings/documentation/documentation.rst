.. index::
   pair: Javascript ; documentation

.. _javascript_documentation:

============================================================================================
Javascript documentation
============================================================================================

.. seealso::

   - https://gomakethings.com/whats-the-best-way-to-document-javascript/

.. contents::
   :depth: 3
