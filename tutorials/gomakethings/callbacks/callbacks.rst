
.. _callbacks_versus_evants:

============================================================================================
Callbacks vs. custom events in vanilla JS
============================================================================================

.. seealso::

   - https://gomakethings.com/callbacks-vs.-custom-events-in-vanilla-js/

.. contents::
   :depth: 3

Introduction
==============

In your websites and web apps, you may occasionally want to run some code
in response to something that happens.

These are often things above-and-beyond the core functionality of your
plugin or script. You might have a generic modal plugin, accordion script,
or toggle menu feature that you use, and you need to adjust it’s behavior
a little in certain situations.

For example…

- After a modal button is clicked, fetch some Ajax content and load it into the modal.
- After an accordion is expanded, find and collapse any other open accordions.
- After the menu is expanded, reduce the size of the logo to free up more space.

Let’s look at two approaches to handling this.
