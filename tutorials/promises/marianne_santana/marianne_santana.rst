.. index::
   ! Promise

.. _promise_marianna_santana:

==============================================================================
**Promises in 15 minutes** by https://x.com/amarianesantana
==============================================================================

.. seealso::

   - :ref:`promise`
   - https://x.com/amarianesantana
   - https://dev.to/marianesantana/promises-in-15-minutes-9l7
   - https://x.com/amarianesantana/status/1257783004736507905?s=20
   - https://github.com/marianesantana/promises


Good evening, fellows!

Let's simplify the use of Promises ?

Particularly, when I started with the concept of Promises when I learned
how to code, I saw a lot of online materials and tutorials that explained
Promises in a very confusing way, then I decided to write a simple text
explaining it in a very practical way. Of course, if you need to understand
'Promises Under the Hood' this article is not for you.

If you need to understand Promises in a short period of time to make a
solution, this is for you.

Basically, Promises were made to create better asynchronous callback
functions in Javascript, to make the code better organized.

To grasp the concept think like it literally means that we are making
a promise in real life.

For example:

    I promise that I will make you understand Promises in 15 minutes.


script.js
===========

.. literalinclude:: script.js
   :linenos:


callbacks.js
===============


.. literalinclude:: callbacks.js
   :linenos:

promises.js
===============


.. literalinclude:: promises.js
   :linenos:

promiseAll.js
===============

.. literalinclude:: promiseAll.js
   :linenos:
