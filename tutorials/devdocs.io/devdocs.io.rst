.. index::
   pair: Tuto; https://devdocs.io/javascript/

.. _devdocs_javascript:

=====================================================================
https://devdocs.io/javascript/
=====================================================================

- https://github.com/freeCodeCamp/devdocs
- https://devdocs.io/javascript/
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide


The JavaScript reference serves as a repository of facts about the
JavaScript language.

The entire language is described here in detail.

As you write JavaScript code, you'll refer to these pages often
(thus the title "JavaScript reference").

The JavaScript language is intended to be used within some larger environment,
be it a browser, server-side scripts, or similar.

For the most part, this reference attempts to be environment-agnostic
and does not target a web browser environment.

If you are new to JavaScript, start with `the guide <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide>`_.

Once you have a firm grasp of the fundamentals, you can use the reference
to get more details on individual objects and language constructs.
