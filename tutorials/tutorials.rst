.. index::
   pair: Javascript ; Tutorials

.. _javascript_tutorials:

============================================================================================
**Javascript tutorials**
============================================================================================

- https://devdocs.io/javascript/
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide
- https://webgems.io/javascript/
- https://www.freecodecamp.org/news/best-javascript-tutorial/
- https://kangax.github.io/compat-table/es6/


.. toctree::
   :maxdepth: 5

   devdocs.io/devdocs.io
   python/python
   1loc.dev/1loc.dev
   airbnb/airbnb
   atapas/atapas
   devhints.io/devhints.io
   for_django/for_django
   flaviocopes.com/flaviocopes.com
   gomakethings/gomakethings
   htmldom.dev/htmldom.dev
   javascript.info/javascript.info
   modern_js_cheatsheet/modern_js_cheatsheet
   mozilla/mozilla
   promises/promises
   overapi/overapi
   python_vs_javascript/python_vs_javascript
   tipsandtidbits/tipsandtidbits
   vanilla_js/vanilla_js
   w3schools/w3schools
   30secondsofcode/30secondsofcode
