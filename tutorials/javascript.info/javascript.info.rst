
.. _javascript_info:

============================================================================================
**javascript.info** The Modern JavaScript Tutorial
============================================================================================


.. seealso::

   - https://javascript.info/
   - https://github.com/javascript-tutorial/en.javascript.info
   - https://github.com/javascript-tutorial/fr.javascript.info
