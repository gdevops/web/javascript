
.. index::
   pair: 1loc.dev ; Javascript


.. _1loc_dev:

============================================================================================
**1loc.dev** What's your favorite JavaScript **single LOC (line of code)** ?
============================================================================================

.. seealso::

   - https://1loc.dev/
   - https://github.com/phuoc-ng/1loc
   - https://github.com/phuoc-ng/1loc/commits/master
   - https://github.com/phuoc-ng/1loc/graphs/contributors
   - https://x.com/nghuuphuoc


.. toctree::
   :maxdepth: 3

   README
