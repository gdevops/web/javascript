
.. _atapas_javascript:

==============================================================================
**My Favorite JavaScript Tips and Tricks**
==============================================================================

.. seealso::

   - https://dev.to/atapas/my-favorite-javascript-tips-and-tricks-4jn4


.. contents::
   :depth: 3

Motivation
===========

Most of the programming languages are open enough to allow programmers
doing things multiple ways for the similar outcome.

JavaScript is no way different. With JavaScript, we often find multiple
ways of doing things for a similar outcome, and that's confusing at times.

Some of the usages are better than the other alternatives and thus,
these are my favorites.

I am going to list them here in this article. I am sure, you will find
many of these in your list too.
