.. index::
   pair: Pythonistas; Javascript

.. _javascript_for_pythonistas:

============================================================================================
**Python vs JavaScript for Pythonistas**
============================================================================================

.. seealso::

   - https://realpython.com/python-vs-javascript/


.. toctree::
   :maxdepth: 3

   enumerations/enumerations
   variadic_functions/variadic_functions
   quirks/quirks
