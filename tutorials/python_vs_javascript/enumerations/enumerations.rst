.. index::
   ! Enumerations

.. _javascript_enumerations:

============================================================================================
**Enumerations**  (Object.freeze)
============================================================================================

.. seealso::

   - https://realpython.com/python-vs-javascript/#javascript-quirks
   - https://www.linguee.fr/anglais-francais/traduction/quirks.html
   - :ref:`web_javascript_technical_debt`

.. contents::
   :depth: 3

Description
=============

There’s no native enumeration type in pure JavaScript, but you can use
the enum type in TypeScript or emulate one with something similar to this:

.. code-block:: javascript

    const Sauce = Object.freeze({
      BBQ: Symbol('bbq'),
      CHILI: Symbol('chili'),
      GARLIC: Symbol('garlic'),
      KETCHUP: Symbol('ketchup'),
      MUSTARD: Symbol('mustard')
    });

**Freezing an object prevents you from adding or removing its attributes**.

This is different from a constant, which can be mutable !

A constant will always point to the same object, but the object itself
might change its value:

::

    > const fruits = ['apple', 'banana'];
    > fruits.push('orange'); // ['apple', 'banana', 'orange']

::

    > fruits = [];

::

    TypeError: Assignment to constant variable.

You can add an orange to the array, which is mutable, but you can’t
modify the constant that is pointing to it.
