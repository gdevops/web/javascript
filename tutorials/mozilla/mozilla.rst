.. index::
   pair: Mozilla ; Javascript

.. _mozilla_javascript_tutorial:

============================================================================================
**Mozilla** Javascript tutorial
============================================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/javascript
   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Une_r%C3%A9introduction_%C3%A0_JavaScript
