
.. _modern_js_cheatsheet:

============================================================================================
**modern-js-cheatsheet**  by https://x.com/mbeaudru/
============================================================================================

.. seealso::

   - https://github.com/mbeaudru/modern-js-cheatsheet/blob/master/README.md
   - https://x.com/mbeaudru/

.. toctree::
   :maxdepth: 3

   README
