.. index::
   ! vanilla-js

.. _vanilla_js:

============================================================================================
**vanilla-js** (Vanilla JavaScript)
============================================================================================

.. seealso::

   - http://vanilla-js.com/

.. contents::
   :depth: 3


http://youmightnotneedjquery.com/
====================================

.. seealso::

   - http://youmightnotneedjquery.com/
   - https://brunoacademie.netlify.app/posts/news/bootstrap-abandonne-jqeury/


https://tobiasahlin.com/blog/move-from-jquery-to-vanilla-javascript/
======================================================================

.. seealso::

   - https://tobiasahlin.com/blog/move-from-jquery-to-vanilla-javascript/


http://vanilla-js.com/ (joke)
================================

Vanilla JS is a fast, lightweight, cross-platform framework
for building incredible, powerful JavaScript applications.


https://plainjs.com/
======================

.. seealso::

   - https://plainjs.com/javascript/

Vanilla JavaScript for building powerful web applications

jQuery is one of the great libraries that helped us overcome JavaScript
browser issues when IE 6 and 7 were still around.

Today, it may be a good choice to drop jQuery and its cousins, because
modern browsers are pretty easy to deal with on their own.

Using plain JavaScript will make your applications load and react
blazingly fast.


https://learnvanillajs.com/
=============================

Vanilla JS is a term for coding with native JavaScript methods and browser
APIs instead of frameworks and libraries.

**(No, it’s not another framework.)**


https://vanillajstoolkit.com/
==========================================

A collection of JavaScript methods, helper functions, plugins, boilerplates,
polyfills, and learning resources.

Vanilla JS is a term for coding with native JavaScript features and
browser APIs instead of frameworks and libraries.


https://github.com/bradtraversy/vanillawebprojects (Mini projects built with HTML5, CSS & JavaScript. No frameworks or libraries)
==================================================================================================================================
