
.. _moderne_javascript_for_django:

============================================================================================
**Modern JavaScript for Django Developers**
============================================================================================

.. seealso::

   - https://www.saaspegasus.com/guides/modern-javascript-for-django-developers/
