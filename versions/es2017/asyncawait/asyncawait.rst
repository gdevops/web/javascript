.. index::
   ! async/await

.. _es2017_asyncawait:

================================================================================================================================
**async/await** Async/await for ECMAScript  (https://github.com/tc39/ecmascript-asyncawait)
================================================================================================================================

.. seealso::

   - https://github.com/tc39/ecmascript-asyncawait
   - https://tc39.es/ecmascript-asyncawait/
   - https://github.com/tc39/notes/blob/master/meetings/2016-07/jul-28.md#10iv-async-functions


.. contents::
   :depth: 3


Async Functions for ECMAScript
=================================

The introduction of Promises and Generators in ECMAScript presents an
opportunity to dramatically improve the language-level model for
writing asynchronous code in ECMAScript.

The spec text can be found here_.

This proposal is implemented in a regenerator which can compile ES5
code containing async and await down to vanilla ES5 to run in existing
browsers and runtimes.

This repo contains a complete example using a large number of the
features of the proposal. To run this example::

    npm install
    regenerator -r server.asyncawait.js | node

.. _here: https://tc39.es/ecmascript-asyncawait/
