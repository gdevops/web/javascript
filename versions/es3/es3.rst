.. index::
   pair: Javascript; ES3

.. _es3:

==========================================================================================================================================
**ECMAScript 3 (ES3)** (December 1999)
==========================================================================================================================================

.. seealso::

   - https://exploringjs.com/impatient-js/ch_history.html#timeline-of-ecmascript-versions



Adds many core features – “[…] regular expressions, better string
handling, new control statements [do-while, switch],
try/catch exception handling, […]”
