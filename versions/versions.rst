.. index::
   pair: ECMAScript; ECMAScript
   ! ECMAScript versions

.. _javascript_versions:

===========================================
Versions
===========================================


- https://github.com/tc39/ecma262/releases/
- https://exploringjs.com/impatient-js/ch_history.html#timeline-of-ecmascript-versions
- https://github.com/tc39/proposals/blob/master/finished-proposals.md
- https://en.wikipedia.org/wiki/ECMAScript#Versions
- https://tc39.es/
- https://github.com/tc39/ecma262/
- https://kangax.github.io/compat-table/es6/
- https://flaviocopes.com/ecmascript/#es-versions

.. toctree::
   :maxdepth: 5


   esnext/esnext
   es2022/es2022
   es2021/es2021
   es2020/es2020
   es2019/es2019
   es2018/es2018
   es2017/es2017
   es2016/es2016
   es2015/es2015
   es5/es5
   es4/es4
   es3/es3
   es2/es2
   es1/es1
