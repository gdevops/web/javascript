
.. _es2018_object_rest_spread:

=======================================================================================================================
**object-rest-spread** Rest/Spread Properties for ECMAScript  (https://github.com/tc39/proposal-object-rest-spread)
=======================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-object-rest-spread
   - https://github.com/tc39/proposal-object-rest-spread/blob/master/Issues.md
   - https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-23.md#restspread-properties-for-stage-4

.. toctree::
   :maxdepth: 3

   README
   Spread
   Rest
   Issues
