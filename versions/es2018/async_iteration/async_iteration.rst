
.. _es2018_async_iteration:

=======================================================================================================================
**async-iteration** Rest/Spread Properties for ECMAScript  (https://github.com/tc39/proposal-async-iteration)
=======================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-async-iteration
   - https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-25.md#conclusionresolution
   - https://github.com/tc39/ecma262/pull/1066
   - https://github.com/domenic
   - https://blog.domenic.me/
   - https://x.com/domenic

.. toctree::
   :maxdepth: 3

   README
