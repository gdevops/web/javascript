.. index::
   pair: Javascript; ES4

.. _es4:

========================================================================
**ECMAScript 4 (ES4)** (abandoned in July 2008)
========================================================================

.. seealso::

   - https://exploringjs.com/impatient-js/ch_history.html#timeline-of-ecmascript-versions



Would have been a massive upgrade (with static typing, modules, namespaces,
and more), but ended up being too ambitious and dividing the language’s stewards.
