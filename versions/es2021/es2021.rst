.. index::
   pair: Javascript; ES2021

.. _es2021:

===========================================================================================
**ECMAScript 2021 (ES12)**
===========================================================================================

- https://262.ecma-international.org/12.0/
- https://github.com/tc39/proposals/blob/master/finished-proposals.md
- https://dev.to/jsdev/es-2021-features-3edf

.. toctree::
   :maxdepth: 3


   string_replaceall/string_replaceall
