.. index::
   pair: Javascript; ES5

.. _es5:

========================================================================
**ECMAScript 5 (ES5)** (December 2009) Brought minor improvements
========================================================================

.. seealso::

   - https://exploringjs.com/impatient-js/ch_history.html#timeline-of-ecmascript-versions



Brought minor improvements, a few standard library features and strict mode
