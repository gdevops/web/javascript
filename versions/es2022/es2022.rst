.. index::
   pair: Javascript; ES2022

.. _es2022:
.. _es_last:

===========================================================================================
**ECMAScript 2022 (ES13) 2022-06**
===========================================================================================

- https://262.ecma-international.org/13.0/
- https://hacks.mozilla.org/2022/06/the-specification-for-javascript-has-a-new-license/
- https://github.com/tc39/proposals/blob/master/finished-proposals.md

.. toctree::
   :maxdepth: 3


   toplevel_await/toplevel_await
   new_license/new_license
