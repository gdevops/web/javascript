.. index::
   pair: Javascript; toplevel await

.. _es2022_toplevel_await:

===========================================================================================
**ECMAScript 2022 (ES13) toplevel await**
===========================================================================================

- https://2ality.com/2022/06/ecmascript-2022.html#top-level-await-in-modules

We can now use await at the top levels of modules and don’t have to enter
async functions or methods anymore:


.. code-block:: javascript

    // my-module.mjs
    const response = await fetch('https://example.com');
    const text = await response.text();
    console.log(text);
