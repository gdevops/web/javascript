.. index::
   pair: Javascript; ES2

.. _es2:

==========================================================================================================================================
**ECMAScript 2 (ES2)** (June 1998): Small update to keep ECMA-262 in sync with the ISO standard
==========================================================================================================================================

.. seealso::

   - https://exploringjs.com/impatient-js/ch_history.html#timeline-of-ecmascript-versions
