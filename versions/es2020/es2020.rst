.. index::
   pair: Javascript; ES2020

.. _es2020:

===========================================================================================
**ECMAScript 2020 (ES11)** **2020-06-17**
===========================================================================================


- https://262.ecma-international.org/11.0/
- https://github.com/tc39/ecma262/releases/tag/es2020
- https://www.ecma-international.org/ecma-262/11.0/
- https://github.com/tc39/ecma262/releases/tag/es2020-candidate-2020-04
- https://github.com/tc39/ecma262/releases/download/es2020-candidate-2020-04/ECMA-262.11th.edition.June.2020.pdf
- https://www.telerik.com/blogs/latest-features-javascript-ecmascript-2020
- https://dev.to/catalinmpit/javascript-es2020-the-features-you-should-know-45i2
- https://2ality.com/2019/12/ecmascript-2020.html
- :ref:`esnext_stage_4`
- :ref:`esnext_stage_3`
- :ref:`esnext_stage_2`
- :ref:`esnext_stage_1`
- :ref:`esnext_stage_0`


.. toctree::
   :maxdepth: 3


   bigint/bigint
   dynamic_import/dynamic_import
   for_in/for_in
   globalthis/globalthis
   import_meta/import_meta
   module_namespace_exports/module_namespace_exports
   nullish_coalescing/nullish_coalescing
   optional_chaining/optional_chaining
   promised_allsettled/promised_allsettled
   string_matchall/string_matchall
   conclusion/conclusion
