
.. _es2020_string_matchall:

============================================================================================================================================================================
**string.matchAll** ES Proposal, specs, tests, reference implementation, and polyfill/shim for String.prototype.matchAll (https://github.com/tc39/proposal-string-matchall)
============================================================================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-string-matchall
   - https://www.npmjs.com/package/string.prototype.matchall
   - https://v8.dev/features/string-matchall

.. contents::
   :depth: 3


Author
========

- Jordan Harband


Rationale
===========

If I have a string, and either a sticky or a global regular expression
which has multiple capturing groups, I often want to iterate through
all of the matches.

Currently, my options are the following:

.. code-block:: javascript

    var regex = /t(e)(st(\d?))/g;
    var string = 'test1test2';

    string.match(regex); // gives ['test1', 'test2'] - how do i get the capturing groups?

    var matches = [];
    var lastIndexes = {};
    var match;
    lastIndexes[regex.lastIndex] = true;
    while (match = regex.exec(string)) {
        lastIndexes[regex.lastIndex] = true;
        matches.push(match);
        // example: ['test1', 'e', 'st1', '1'] with properties `index` and `input`
    }
    matches; /* gives exactly what i want, but uses a loop,
            * and mutates the regex's `lastIndex` property */
    lastIndexes; /* ideally should give { 0: true } but instead
            * will have a value for each mutation of lastIndex */

    var matches = [];
    string.replace(regex, function () {
        var match = Array.prototype.slice.call(arguments, 0, -2);
        match.input = arguments[arguments.length - 1];
        match.index = arguments[arguments.length - 2];
        matches.push(match);
        // example: ['test1', 'e', 'st1', '1'] with properties `index` and `input`
    });
    matches; /* gives exactly what i want, but abuses `replace`,
          * mutates the regex's `lastIndex` property,
          * and requires manual construction of `match` */

The first example does not provide the capturing groups, so isn’t an option.

The latter two examples both visibly mutate lastIndex - this is not a
huge issue (beyond ideological) with built-in RegExps, however, with
subclassable RegExps in ES6/ES2015, this is a bit of a messy way to
obtain the desired information on all matches.

Thus, String#matchAll would solve this use case by both providing
access to all of the capturing groups, and not visibly mutating the
regular expression object in question.

Iterator versus Array
=========================

Many use cases may want an array of matches - however, clearly not all will.

Particularly large numbers of capturing groups, or large strings, might
have performance implications to always gather all of them into an array.

By returning an iterator, it can trivially be collected into an array
with the spread operator or Array.from if the caller wishes to, but it
need not.

Naming
==========

The name matchAll was selected to correspond with match, and to connote
that all matches would be returned, not just a single match.

This includes the connotation that the provided regex will be used with
a global flag, to locate all matches in the string.

An alternate name has been suggested, matches - this follows the
precedent set by keys/values/entries, which is that a plural noun
indicates that it returns an iterator.

However, includes returns a boolean.

When the word is not unambiguously a noun or a verb, "plural noun"
doesn't seem as obvious a convention to follow.

Update from committee feedback: **ruby uses the word scan** for this, but
the committee is not comfortable introducing a new word to JavaScript.

matchEach was suggested, but some were not comfortable with the naming
similarity to forEach while the API was quite different.

**matchAll seems to be the name everyone is most comfortable with**.

In the September 2017 TC39 meeting, there was a question raised about
whether "all" means "all overlapping matches" or "all non-overlapping
matches" - where “overlapping” means “all matches starting from each
character in the string”, and “non-overlapping” means “all matches
starting from the beginning of the string”.

We briefly considered either renaming the method, or adding a way to
achieve both semantics, but the objection was withdrawn.


Articles
=========

https://www.freecodecamp.org
-------------------------------

.. seealso::

   - https://www.freecodecamp.org/news/javascript-new-features-es2020/

**matchAll** is a new method added to the String prototype which is
related to Regular Expressions.

This returns an iterator which returns all matched groups one after
another.


https://www.jesuisundev.com
-------------------------------

.. seealso:

   - https://www.jesuisundev.com/es2020-les-nouveautes-dans-ton-javascript/


La méthode matchAll() de l’object String est également tagué ES2020.

Très simplement, cette nouvelle méthode fait le même taf que match,
mais **renvoie beaucoup plus d’informations**.

Avec un match normal, les valeurs qui match sont trouvées et renvoyées
tel quelles, sans aucune information utile.

.. code-block:: javascript
   :linenos:

    let text = 'Héros des temps modernes: superToto Toto Tata Titi superTiti';
    let regex = /Toto/g;
    for (const match of text.match(regex)) {
      console.log(match);
    }
    // Toto
    // Toto

Avec **matchAll**, tu as le droit à beaucoup plus d’informations pour
faciliter ton taf.

Notamment l’index où se trouve chaque match et l’input.

Même le groupe capturant si tu fais une regex plus complexe avec des
groupes capturants.


.. code-block:: javascript
   :linenos:


    let text = 'Héros des temps modernes: superToto Toto Tata Titi superTiti';
    let regex = /Toto/g;
    for (const match of text.matchAll(regex)) {
      console.log(match);
    }
    // ["Toto", index: 31, input: "Héros des temps modernes: superToto Toto Tata Titi superTiti", groups: undefined]
    // ["Toto", index: 36, input: "Héros des temps modernes: superToto Toto Tata Titi superTiti", groups: undefined]


https://2ality.com
--------------------

.. seealso::

   - https://2ality.com/2018/02/string-prototype-matchall.html
