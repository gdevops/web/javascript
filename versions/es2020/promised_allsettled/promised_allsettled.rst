
.. _es2020_promise_allsettled:

==============================================================================
**promise-allSettled** (https://github.com/tc39/proposal-promise-allSettled)
==============================================================================

.. seealso::

   - https://github.com/tc39/proposal-promise-allSettled
   - https://www.martinmck.com/posts/es2020-everything-you-need-to-know/
   - https://v8.dev/features/promise-combinators

.. contents::
   :depth: 3


Authors
========

- Jason Williams
- Robert Pamely
- Mathias Bynens

Why **allSettled** ?
======================

We say that a promise is settled if it is not pending, i.e. if it is
either fulfilled or rejected.

See promise states and fates for more background on the relevant terminology.

Furthermore, the name allSettled is commonly used in userland libraries
implementing this functionality. See below.



Promise.allSettled (https://www.martinmck.com/posts/es2020-everything-you-need-to-know/)
==========================================================================================

Let's say you are sitting an exam. When you receive your results, you
find out you got 99% of the questions correct.

In most walks of life, you would have passed with flying colours.
In this case, though - you receive a big red stamp on your results
letter telling you that you failed.

This is how Promise.all works.

Promise.all takes an array of promises, and concurrently fetches their
results. If they all succeed, your Promise.all succeeds. If one or more
fail, your promise rejects.
In some cases you may want this behaviour - but not always.

Enter Promise.allSettled
-----------------------------

Promise.allSettled of ES2020 is much kinder when it comes to your exam.

It will give you a pat on the back and tell you not to worry about that
1% of promises that failed.

A promise is regarded as "settled" when it comes back - pass or fail.
Promise.allSettled allows us to pass an array of promises and it will
resolve when they are all settled. The return value of the promise is
the array of results. Let's look at an example.

.. code-block:: javascript
   :linenos:

    const promises = [
      fetch('/api1'),
      fetch('/api2'),
      fetch('/api3'),
    ];

    Promise.allSettled(promises).
      then((results) => results.forEach((result) => console.log(result.status)));

    // "fulfilled"
    // "fulfilled"
    // "rejected"


Promise.allSettled (https://medium.com/javascript-in-plain-english/new-features-in-es2020-you-should-check-b4974d9d7edc)
=============================================================================================================================

This is quite similar to Promise.all , but there’s a significant
difference between them.

Promise.all waits for all the promises being fulfilled or an any promise
being rejected.

On the other hand, Promise.allSettled doesn’t care about that.

What it cares is to know if all the promises are done, whichever their
status is. So every input promise could be fulfilled or rejected, but
it doesn’t matter to Promise.allSettled.

Just all of them have to be done.


.. code-block:: javascript
   :linenos:

    const promises = [
      Promise.resolve(1),
      Promise.reject(2),
      Promise.resolve(3)
    ];

    const onResolve = (data, prefix) => {
      console.log(prefix, 'Resolved with', data);
    };

    const onReject = (err, prefix) => {
      console.log(prefix, 'Rejected with', err);
    };

    Promise.all(promises)
      .then(onResolve.bind(null, 'all'))
      .catch(onReject.bind(null, 'all'));

    // Result:
    // all Rejected with 2

    Promise.allSettled(promises)
      .then(onResolve.bind(null, 'allSettled'))
      .catch(onReject.bind(null, 'allSettled'));

    // Result:
    // allSettled Resolved with
    // [
    //   {
    //     "status": "fulfilled",
    //     "value": 1
    //   },
    //   {
    //     "status": "rejected",
    //     "reason": 2
    //   },
    //   {
    //     "status": "fulfilled",
    //     "value": 3
    //   }
    // ]

Articles
==========

https://www.freecodecamp.org
-------------------------------

.. seealso::

   - https://www.freecodecamp.org/news/javascript-new-features-es2020/

The Promise.allSettled method accepts an array of Promises and only
resolves when all of them are settled – either resolved or rejected.

This was not available natively before, even though some close implementations
like race and all were available. This brings "Just run all promises –
I don't care about the results" natively to JavaScript.


https://www.jesuisundev.com
-----------------------------

.. seealso::

   - https://www.jesuisundev.com/es2020-les-nouveautes-dans-ton-javascript/
   - http://bluebirdjs.com/docs/getting-started.html


L’ ES2020 introduit également une nouvelle façon de gérer plusieurs
promesses à la fois.
À la base, on avait juste Promise.all. Promise.all prend un tableau de
promesses et retourne une promesse si toutes les promesses du tableau
sont résolues.

Si une seule promesse reject dans ton tableau, tout est reject
immédiatement. Aucune cordialité

.. code-block:: javascript
   :linenos:

    const resolvedPromise = new Promise((resolve, reject) => setTimeout(resolve('superToto'), 500));

    const rejectedPromise = new Promise((resolve, reject) => setTimeout(reject(new Error('ERROR')), 500));

    Promise.all([resolvedPromise, rejectedPromise]).then(results => console.log(results)).catch(error => console.log(error)); // Error: ERROR


**Promise.allSettled** va renvoyer un tableau de toutes les promesses
avec leurs résultats (résolus ou rejetés).

Ça va attendre patiemment que toutes les promesses fassent leurs affaires,
sans interrompre quoique-ce, même en cas d’erreur.

Ensuite, ça va te renvoyer un joli tableau bien organisé.

.. code-block:: javascript
   :linenos:

    const resolvedPromise = new Promise((resolve, reject) => setTimeout(resolve('superToto'), 500));

    const rejectedPromise = new Promise((resolve, reject) => setTimeout(reject(new Error('ERROR')), 500));

    Promise.allSettled([resolvedPromise, rejectedPromise]).then(results => console.log(results));
    // [
    //   Object { status: "fulfilled", value: "superToto"},
    //   Object { status: "rejected", reason: Error: ERROR}
    // ]

On pouvait déjà le faire en hackant un peu via Bluebird_. Mais le fait
de pouvoir le faire nativement est super agréable !

**Très pratique, j’adore !**

.. _Bluebird:  http://bluebirdjs.com/docs/getting-started.html


https://2ality.com
--------------------

.. seealso::

   - https://2ality.com/2019/08/promise-combinators.html


In this blog post, we take a look at three static methods of Promise:

- Promise.all() and Promise.race() which JavaScript has had since
  ECMAScript 2015 (ES6) when Promises were added to the language.
- Promise.allSettled() which recently advanced to stage 4 and will
  therefore be part of ECMAScript 2020.
