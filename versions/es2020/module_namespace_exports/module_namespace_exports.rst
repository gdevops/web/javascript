.. index::
   ! Module Namespace Exports

.. _es2020_module_namespace_export:

============================================================================================================================================
**Module Namespace Exports** Proposal to add `export * as ns from "mod";` to ECMAScript (https://github.com/tc39/proposal-export-ns-from)
============================================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-export-ns-from
   - https://es.discourse.group/t/final-es2020-feature-set/218/2
   - https://es.discourse.group/t/final-es2020-feature-set/218
   - https://github.com/tc39/ecma262/pull/1174

.. contents::
   :depth: 3


README
========

.. toctree::
   :maxdepth: 3

   README

README
========

.. toctree::
   :maxdepth: 3

   ESTree



Articles
=========

https://www.freecodecamp.org
---------------------------------


.. seealso::

   - https://www.freecodecamp.org/news/javascript-new-features-es2020/


In JavaScript modules, it was already possible to use the following syntax::

    import * as utils from './utils.mjs'

However, no symmetric export syntax existed, until now::

    export * as utils from './utils.mjs'

This is equivalent to the following::

    import * as utils from './utils.mjs'
    export { utils }
