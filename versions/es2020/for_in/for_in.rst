.. index::
   pair: for-in ; Partially specifying object enumeration order in JavaScript

.. _es2020_for_in:

=======================================================================================================================
**for-in** Partially specifying object enumeration order in JavaScript (https://github.com/tc39/proposal-for-in-order)
=======================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-for-in-order
   - http://tc39.es/proposal-for-in-order/
   - https://github.com/tc39/ecma262/pull/1791

.. contents::
   :depth: 3


Specifying for-in enumeration order
======================================

(Partially.)

ECMA-262 leaves the order of for (a in b) ... almost totally unspecified,
but real engines tend to be consistent in at least some cases.

Furthermore, over the years implementations have observed that anyone
who wants to run code on the web needs to follow some constraints not
captured by the spec.

This is a stage 4 (finished) proposal to begin fixing that.


for-in mechanics (https://www.martinmck.com/posts/es2020-everything-you-need-to-know/)
===========================================================================================

for (x in obj) ... is a super useful syntax for many things - mainly
iterating over the keys of an object.

.. code-block:: javascript

    for (let key in obj) {
      console.log(key);
    }

This proposal is related to the order and semantics of which elements
are iterated in a for..in loop.

Before this proposal, most JavaScript engines had already applied
common sense - currently all major browsers loop over the properties
of an object in the order in which they were defined.

There were some nuances, however.

These mainly involved more advanced features like proxies. for..in loop
semantics have historically been left out the JavaScript spec, but this
proposal ensures everyone has a consistent point of reference as to how
for..in should work.


Articles
===========

https://www.freecodecamp.org
------------------------------

.. seealso::

  - https://www.freecodecamp.org/news/javascript-new-features-es2020/

The ECMA specification did not specify in which order for (x in y)
should run.
Even though browsers implemented a consistent order on their own before
now, this has been officially standardized in ES2020
