
.. _es2020_conclusion:

===========================
Conclusion
===========================

.. contents::
   :depth: 3

https://www.martinmck.com
============================

.. seealso::

   - https://www.martinmck.com/posts/es2020-everything-you-need-to-know/)


All in all, the newest features added to the ECMAScript specification
add even more utility, flexibility and power to the constantly evolving
and developing ecosystem that is JavaScript.

It's encouraging and exciting to see the community continue to thrive
and improve at such a rapid pace.

You may be thinking - "That all sounds great.. but how do I get started
using ES2020 features?"

When/how can I use this stuff?

You can use it now ! In the later versions of most modern browsers and Node,
you will have support for all of these features. caniuse.com is another
great resource to check levels of compatibility for ES2020 features
across browsers and node.

If you need to use these features on older browsers or versions of node,
you will need babel/typescript.


https://www.freecodecamp.org
============================

.. seealso::

   - https://www.freecodecamp.org/news/javascript-new-features-es2020/
   - https://www.youtube.com/watch?time_continue=1&v=Fag_8QjBwtY&feature=emb_logo
   - https://codedamn.com/
   - https://x.com/codedamncom

.. figure:: freecodecamp/es2020logo.jpg
   :align: center


I love the consistency and speed with which the JavaScript community
has evolved and is evolving.

It is amazing and truly wonderful to see how JavaScript came from a
language which was booed on, 10 years go, to one of the strongest, most
flexible and versatile language of all time today.

Do you wish to learn JavaScript and other programming languages in a
completely new way? Head on to a new platform for developers I'm working
on to try it out today!

What's your favorite feature of ES2020 ?

Tell me about it by tweeting and connecting with me on Twitter and Instagram!

This is a blog post composed from my video which is on the same topic.
It would mean the world to me if you could show it some love!


https://www.jesuisundev.com
=============================

.. seealso::

   - https://www.jesuisundev.com/es2020-les-nouveautes-dans-ton-javascript/
   - https://x.com/jesuisundev/

Et voilà, j’ai fait le tour de ce dont je voulais te parler sur l’ES2020 !

Il y a des autres changements tagués ici.

Mais à mon sens ils sont moins importants que ceux dont je te parle dans
cet article.

Fin de l’enquête, je te remercie pour la lecture et je finis avec l’épilogue.


Épilogue
------------

.. seealso::

   - https://www.jesuisundev.com/comprendre-javascript-en-5-minutes/
   - https://www.jesuisundev.com/bien-debuter-en-javascript/

Si tu veux te tenir au courant de ce que fait le TC39 sur ton Javascript,
check donc leur GitHub_.

Ils se passent plein de trucs.

Si tu es nouveau en Javascript je te conseille deux articles pour bien
comprendre et bien débuter en Javascript.

Et sinon je te dis rendez-vous à la prochaine mise à jour de spécification,
car je vais faire ça à chaque fois désormais !


.. _GitHub: https://github.com/tc39
