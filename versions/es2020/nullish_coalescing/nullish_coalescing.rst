.. index::
   ! Nullish Coalescing for JavaScript
   ! ES2020 ; Gabriel Ehrenberg

.. _es2020_nullish_coalescing:

=======================================================================================================================
**nullish coalescing (??)** Nullish Coalescing for JavaScript (https://github.com/tc39/proposal-nullish-coalescing)
=======================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-nullish-coalescing
   - https://en.wikipedia.org/wiki/Null_coalescing_operator
   - https://v8.dev/features/nullish-coalescing
   - https://github.com/tc39/notes/blob/master/meetings/2019-12/december-4.md#nullish-coalescing-for-stage-4

.. contents::
   :depth: 3


Authors
==========

- Gabriel Isenberg_ (github, twitter)
- Daniel Ehrenberg_ (github, twitter)
- Daniel Rosenwasser_ (github, twitter)

.. _Ehrenberg: https://x.com/littledan
.. _Isenberg: https://x.com/the_gisenberg
.. _Rosenwasser: https://x.com/drosenwasser


Overview and motivation
==========================

When performing property accesses, it is often desired to provide a
default value if the result of that property access is null or undefined.

At present, a typical way to express this intent in JavaScript is by
using the || operator.

.. code-block:: javascript

    const response = {
      settings: {
        nullValue: null,
        height: 400,
        animationDuration: 0,
        headerText: '',
        showSplashScreen: false
      }
    };


    const undefinedValue = response.settings.undefinedValue || 'some other default'; // result: 'some other default'
    const nullValue = response.settings.nullValue || 'some other default'; // result: 'some other default'

This works well for the common case of null and undefined values, but
there are a number of falsy values that might produce surprising results:

.. code-block:: javascript

    const headerText = response.settings.headerText || 'Hello, world!'; // Potentially unintended. '' is falsy, result: 'Hello, world!'
    const animationDuration = response.settings.animationDuration || 300; // Potentially unintended. 0 is falsy, result: 300
    const showSplashScreen = response.settings.showSplashScreen || true; // Potentially unintended. false is falsy, result: tru


The nullary coalescing operator is intended to handle these cases better
and serves as an equality check against nullary values (null or undefined).

Syntax
========

Base case. If the expression at the left-hand side of the ?? operator
evaluates to undefined or null, its right-hand side is returned.

.. code-block:: javascript

    const response = {
      settings: {
        nullValue: null,
        height: 400,
        animationDuration: 0,
        headerText: '',
        showSplashScreen: false
      }
    };

    const undefinedValue = response.settings.undefinedValue ?? 'some other default'; // result: 'some other default'
    const nullValue = response.settings.nullValue ?? 'some other default'; // result: 'some other default'
    const headerText = response.settings.headerText ?? 'Hello, world!'; // result: ''
    const animationDuration = response.settings.animationDuration ?? 300; // result: 0
    const showSplashScreen = response.settings.showSplashScreen ?? true; // result: false


Prior Art
============

- https://en.wikipedia.org/wiki/Null_coalescing_operator

Articles
=========


Mozilla
---------

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator

The nullish coalescing operator (??) is a logical operator that returns
its right-hand side operand when its left-hand side operand is null or
undefined, and otherwise returns its left-hand side operand.

Contrary to the logical OR (||) operator, the left operand is returned
if it is a falsy value which is not null or undefined.

In other words, if you use || to provide some default value to another
variable foo, you may encounter unexpected behaviors if you consider
some falsy values as usable (eg. '' or 0). See below for more examples.


::

    const foo = null ?? 'default string';
    console.log(foo);
    // expected output: "default string"

    const baz = 0 ?? 42;
    console.log(baz);
    // expected output: 0


https://javascript.info
----------------------------

.. seealso::

   - https://javascript.info/nullish-coalescing-operator

Slides
--------

.. seealso::

   - https://onedrive.live.com/?authkey=%21AHA3b8iHjOXvY7Q&id=5D3264BDC1CB4F5B%215320&cid=5D3264BDC1CB4F5B


https://www.freecodecamp.org
------------------------------

.. seealso::

   - https://www.freecodecamp.org/news/javascript-new-features-es2020/


Nullish coalescing adds the ability to truly check nullish values instead
of **falsey values**. What is the difference between nullish and falsey
values, you might ask ?

**In JavaScript, a lot of values are falsey, like empty strings, the
number 0, undefined, null, false, NaN, and so on**.

However, a lot of times you might want to check if a variable is nullish
that is if it is either undefined or null, like when it's okay for a
variable to have an empty string, or even a false value.

In that case, you'll use the new nullish coalescing operator, **??**


https://www.jesuisundev.com
-----------------------------

.. seealso::

   - https://www.jesuisundev.com/es2020-les-nouveautes-dans-ton-javascript/


L’ES2020 vient également avec un nouvel opérateur logique !

Pour bien comprendre de quoi on parle, rappelons rapidement comment
fonctionnent les opérateurs logiques existants.

Il existe aujourd’hui trois opérateurs logiques:

- le OU logique (||)
- le ET logique (&&)
- et le NON logique (!).

On va s’intéresser aux deux premiers.

OU logique
+++++++++++

L’opérateur OU logique va tenter de transformer la première expression
en true (les gens parlent de **truthy**).

Si elle peut être convertie, alors cette valeur est renvoyée.
Sinon c’est l’autre valeur qui est renvoyée. Examples :

.. code-block:: javascript
   :linenos:

    let name = "superToto";
    console.log(name || "Toto"); // "superToto"

    let name = false;
    console.log(name || "Toto"); // "Toto"

    let name = undefined;
    console.log(name || "Toto"); // "Toto"

    let name = null;
    console.log(name || "Toto"); // "Toto"


ET logique
+++++++++++


L’opérateur ET logique va tenter de transformer la première expression
en false (les gens parlent de **falsy**). Si elle peut être convertie,
alors cette valeur est renvoyée.

Sinon c’est l’autre valeur qui est renvoyée. Exemples :

.. code-block:: javascript
   :linenos:

    let name = "superToto";
    console.log(name && "Toto"); // "Toto"

    let name = false;
    console.log(name && "Toto"); // false

    let name = undefined;
    console.log(name && "Toto"); // undefined

    let name = null;
    console.log(name && "Toto"); // null

Le problème que beaucoup de développeurs rencontrent avec tout ça et
la manière dont est traitée undefined et null.

Par défaut, Javascript transforme ces valeurs en false alors qu’elles
ne sont ni vraies ni fausses, juste vides.

**Nullish coalescing vient régler ce problème**.

Le nouvel opérateur Nullish coalescing ??
+++++++++++++++++++++++++++++++++++++++++++++++++

Le nouvel opérateur Nullish coalescing, en utilisant la syntaxe ??, va
vérifier si le premier opérande est strictement vide (null ou undefined).
Si oui, c’est l’autre valeur qui est renvoyée.
Sinon, la première valeur est renvoyée. Exemples :

.. code-block:: javascript
   :linenos:

    let name = "superToto";
    console.log(name ?? "Toto"); // "superToto"

    let name = false;
    console.log(name ?? "Toto"); // false

    let name = undefined;
    console.log(name ?? "Toto"); // "Toto"

    let name = null;
    console.log(name ?? "Toto"); // "Toto"

Boum ! Tu peux gérer tes valeurs **Nullish** comme tu traites les **Falsy**
et **Truthy** ! Pas mal non ?


https://2ality.com/
--------------------

.. seealso::

   - https://2ality.com/2019/08/nullish-coalescing.html
