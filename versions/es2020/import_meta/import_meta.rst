.. index::
   ! import.meta

.. _es2020_import_meta:

=======================================================================================================================
**import.meta** import.meta proposal for JavaScript  (https://github.com/tc39/proposal-import-meta)
=======================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-import-meta
   - https://docs.google.com/presentation/d/1p1BGFY05-iCiop8yV0hNyWU41_wlwwfv6HIDkRNNIBQ/edit?usp=sharing
   - https://github.com/tc39/proposal-import-meta/pull/21

.. contents::
   :depth: 3


Author
=======

- Domenic

import.meta
=============

This repository contains a proposal for adding an **import.meta metaproperty**
to JavaScript, for holding host-specific metadata about the current module.

It is currently in stage 4 of the TC39 process.

Previously this problem space was discussed with the module-loading
community in whatwg/html#1013.

The result was a presentation at the May 2017 TC39 meeting of these slides_.

The committee's feedback about the options presented there led to the
particular form chosen in this proposal; these discussions are summarized
in the rest of this README.

You can view the in-progress spec draft and take part in the discussions
on the issue tracker.


.. _slides: https://docs.google.com/presentation/d/1p1BGFY05-iCiop8yV0hNyWU41_wlwwfv6HIDkRNNIBQ/edit?usp=sharing


Motivation and use cases
===========================

It is often the case that a host environment can provide useful
module-specific information, to code evaluating within a module.

Several examples follow:

The module's URL or filename

This is provided in Node.js's CommonJS module system via the in-scope __filename
variable (and its counterpart __dirname).
This allows easy resolution of resources relative to the module file,
via code such as

.. code-block:: javascript

    const fs = require("fs");
    const path = require("path");
    const bytes = fs.readFileSync(path.resolve(__dirname, "data.bin"));

Without __dirname available, code like fs.readFileSync("data.bin") would
resolve data.bin relative to the current working directory.

This is generally not useful for library authors, which bundle their
resources with their modules which could be located anywhere relative
to the CWD.

Very similar use cases exist in browsers, where the URL would stand in
place of the filename.


The initiating <script>
=========================

This is provided in browsers for classic scripts (i.e. non-module scripts)
via the **global document.currentScript property**.

It is often used for configuration of an included library.
The page writes code such as:

.. code-block:: html

    <script data-option="value" src="library.js"></script>

and the library then looks up the value of data-option via code like

.. code-block:: javascript

    const theOption = document.currentScript.dataset.option;

As an aside, the mechanism of using an (effectively) global variable
for this, instead of a lexically-scoped value, is problematic, as it
means the value is only set at top-level, and must be saved there if
it is to be used in any asynchronous code.

Am I the "main" module ?
============================

In Node.js, it is common practice to branch on whether you are the
"main" or "entry" module for a program, via code such as:

.. code-block:: javascript

    if (module === process.mainModule) {
      // run tests for this library, or provide a CLI interface, or similar
    }

That is, it allows a single file to serve as a library when imported,
or as a side-effecting program when run directly.

Note how this particular formulation relies on comparing the host-provided
scoped value module with the host-provided global value process.mainModule.

Other miscellaneous use cases
===================================

Other cases that can be envisioned include:

- Other Node.js utilities, such as module.children or require.resolve()
- Information on the "package" a module belongs to, either via Node.js's
  package.json or a web package
- A pointer to the containing DocumentFragment for JavaScript modules
  embedded inside HTML modules.

Constraints
==============

Given that this information is so often host-specific, we want JavaScript
to provide a general extensibility mechanism that hosts can use, instead
of requiring standardization in JavaScript for every piece of metadata.

Also, as alluded to above, this is information is best provided lexically,
instead of via e.g. a temporarily-set global variable.


Proposed solution
=================

This proposal adds an **import.meta meta-property**, which is itself an
object.

It is created by the ECMAScript implementation, with a null prototype.
The host environment can return a set of properties (as key/value pairs)
that will be added to the object.

Finally, as an escape hatch, the object can be modified arbitrarily
by the host if necessary.

The **import.meta meta-property** is only syntactically valid in modules,
as it is meant for meta-information about the currently running module,
and should not be repurposed for information about the currently-running
classic script.

Example
=========

The following code uses a couple properties that we anticipate adding
to **import.meta in browsers**:

.. code-block:: javascript

    (async () => {
      const response = await fetch(new URL("../hamsters.jpg", import.meta.url));
      const blob = await response.blob();

      const size = import.meta.scriptElement.dataset.size || 300;

      const image = new Image();
      image.src = URL.createObjectURL(blob);
      image.width = image.height = size;

      document.body.appendChild(image);
    })();

When this module is loaded, no matter its location, it will load a
sibling file hamsters.jpg, and display the image.

The size of the image can be configured using the script element used
to import it, such as with

.. code-block:: html

    <script type="module" src="path/to/hamster-displayer.mjs" data-size="500"></script>


Articles
============

https://www.martinmck.com
------------------------------


**import.meta** is a convenience property that provides an object containing
the base URL of the currently running module.

If you are familiar with node, this functionality is available out of
the box with CommonJS through the __dirname or __filename properties.

.. code-block:: javascript

    const fs = require("fs");
    const path = require("path");
    // resolves data.bin relative to the directory of this module
    const bytes = fs.readFileSync(path.resolve(__dirname, "data.bin"));

What about the browser though ?
+++++++++++++++++++++++++++++++++++

This is where **import.meta** becomes useful. If you want to import a relative
path from a JavaScript module running in the browser, you can use **import.meta**
to do so.

.. code-block:: javascript

    // Will import cool-image relative to where this module is running.
    const response = await fetch(new URL("../cool-image.jpg", import.meta.url));

This feature is very useful for library authors - as they do not know
how and where you will run your code.

https://www.freecodecamp.org
--------------------------------

.. seealso::

   - https://www.freecodecamp.org/news/javascript-new-features-es2020/

The **import.meta** object was created by the ECMAScript implementation,
with a null prototype.

Consider a module, module.js::

    <script type="module" src="module.js"></script>

You can access meta information about the module using the
**import.meta** object:

::

    console.log(import.meta); // { url: "file:///home/user/module.js" }

It returns an object with a **url property indicating the base URL of the module**.

This will either be the URL from which the script was obtained (for external scripts),
or the document base URL of the containing document (for inline scripts).

https://exploringjs.com/impatient-js
-------------------------------------

.. seealso::

   - https://exploringjs.com/impatient-js/ch_modules.html#preview-import.meta.url


“import.meta” is an ECMAScript feature proposed by Domenic Denicola.

The object import.meta holds metadata for the current module.

Its most important property is import.meta.url, which contains a string
with the URL of the current module file. For example: 'https://example.com/code/main.mjs'
