.. index::
   pair: Javascript; ES1

.. _es1:

==========================================================================================================================================
**ECMAScript 1 (ES1)** (June 1997): First version of the standard.
==========================================================================================================================================

.. seealso::

   - https://exploringjs.com/impatient-js/ch_history.html#timeline-of-ecmascript-versions
