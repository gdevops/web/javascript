.. index::
   pair: stage 0; ESNext

.. _esnext_stage_0:

===========================================================================================
**ES.Next stage 0**
===========================================================================================

.. seealso::

   - https://github.com/tc39/proposals/


stage-0-proposals
===================

.. toctree::
   :maxdepth: 3

   ../stage-0-proposals.md
