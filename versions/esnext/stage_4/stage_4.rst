.. index::
   pair: Stage 4; ES2020

.. _esnext_stage_4:

===========================================================================================
**ES.Next stage 4**
===========================================================================================

.. seealso::

   - https://tc39.es/ecma262/2020/
   - https://github.com/tc39/ecma262/releases/tag/es2020-candidate-2020-04
   - https://github.com/tc39/ecma262/releases/download/es2020-candidate-2020-04/ECMA-262.11th.edition.June.2020.pdf
   - https://github.com/tc39/proposals/blob/master/finished-proposals.md



.. toctree::
   :maxdepth: 3

   ../finished-proposals


Stage 4 proposals
====================

.. seealso::

   - :ref:`string_replaceall`
