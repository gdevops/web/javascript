# Stage 0 Proposals

Stage 0 proposals are either

  - planned to be presented to the committee by a TC39 champion, or
  - have been presented to the committee and not rejected definitively,
    but have not yet achieved any of the criteria to get into stage 1.

<table>
<colgroup>
<col style="width: 38%" />
<col style="width: 21%" />
<col style="width: 21%" />
<col style="width: 19%" />
</colgroup>
<thead>
<tr class="header">
<th>Proposal</th>
<th>Author</th>
<th>Champion</th>
<th>Last Presented</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="https://github.com/allenwb/ESideas/blob/master/ES7MetaProps.md">Additional metaproperties</a></td>
<td>Allen Wirfs-Brock</td>
<td>Allen Wirfs-Brock</td>
<td></td>
</tr>
<tr class="even">
<td><a href="https://web.archive.org/web/20160804042547/http://wiki.ecmascript.org/doku.php?id=strawman:defensible_classes">Defensible Classes</a></td>
<td>Mark Miller<br />Doug Crockford</td>
<td>Mark Miller<br />Doug Crockford</td>
<td></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/zenparsing/es-function-bind">Function bind syntax</a></td>
<td>Kevin Smith</td>
<td>Brian Terlson<br />Matthew Podwysocki</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2015-03/mar-25.md#6vi-function-bind-and-private-fields-redux-kevin-smith">March 2015</a></td>
</tr>
<tr class="even">
<td><a href="https://goo.gl/8MmCMG">Function expression decorators</a></td>
<td>Igor Minar</td>
<td>Igor Minar</td>
<td></td>
</tr>
<tr class="odd">
<td><a href="https://goo.gl/r1XT9b">Method parameter decorators</a></td>
<td>Igor Minar</td>
<td>Igor Minar</td>
<td></td>
</tr>
<tr class="even">
<td><a href="https://github.com/benjamn/reify/blob/master/PROPOSAL.md">Nested <code>import</code> declarations</a></td>
<td>Ben Newman</td>
<td>Ben Newman</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2016-07/jul-27.md#10iiic-nested-import-declaration">July 2016</a></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/erights/Orthogonal-Classes">Orthogonal Classes</a></td>
<td>Mark Miller<br />Allen Wirfs-Brock</td>
<td>Mark Miller<br />Allen Wirfs-Brock</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2017-03/mar-22.md#10iiia-orthogonal-classes">March 2017</a></td>
</tr>
<tr class="even">
<td><a href="https://github.com/caitp/TC39-Proposals/blob/master/tc39-reflect-isconstructor-iscallable.md"><code>Reflect.{isCallable,isConstructor}</code></a></td>
<td>Caitlin Potter</td>
<td>Caitlin Potter</td>
<td></td>
</tr>
<tr class="odd">
<td><a href="https://web.archive.org/web/20160804042554/http://wiki.ecmascript.org/doku.php?id=strawman:relationships">Relationships</a></td>
<td>Mark Miller<br />Waldemar Horwat</td>
<td>Mark Miller<br />Waldemar Horwat</td>
<td></td>
</tr>
<tr class="even">
<td><a href="https://github.com/mathiasbynens/String.prototype.at"><code>String.prototype.at</code></a></td>
<td>Mathias Bynens</td>
<td>Mathias Bynens<br />Rick Waldron</td>
<td></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/dslomov-chromium/ecmascript-structured-clone">Structured Clone</a></td>
<td>Dmitry Lomov</td>
<td>Dmitry Lomov</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2014-01/jan-30.md#structured-clone">Jan 2014</a></td>
</tr>
<tr class="even">
<td><a href="https://github.com/jasnell/proposal-url">WHATWG URL</a></td>
<td>James M Snell</td>
<td>James M Snell</td>
<td></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/rbuckton/proposal-shorthand-improvements">Object Shorthand Improvements</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2017-09/sept-28.md#13i-object-shorthand-improvements">Sep 2017</a></td>
</tr>
<tr class="even">
<td><a href="https://github.com/jasnell/proposal-deprecated"><code>deprecated</code></a></td>
<td>James M Snell</td>
<td>James M Snell</td>
<td></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/zkat/proposal-as-patterns"><code>as</code> destructuring patterns</a></td>
<td>Kat Marchán</td>
<td>Kat Marchán</td>
<td></td>
</tr>
<tr class="even">
<td><a href="https://github.com/devsnek/proposal-symbol-thenable"><code>Symbol.thenable</code></a></td>
<td>Gus Caplan</td>
<td>Jordan Harband<br />Myles Borins</td>
<td><a href="https://github.com/tc39/notes/blob/def2ee0c04bc91612576237314a4f3b1fe2edaef/meetings/2018-05/may-24.md#symbolthenable-for-stage-1-or-2">May 2018</a></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/rricard/proposal-refcollection/">RefCollection</a></td>
<td>Robin Ricard</td>
<td>Robin Ricard</td>
<td></td>
</tr>
<tr class="even">
<td><a href="https://github.com/rickbutton/proposal-deep-path-properties-for-record/">Deep Path Properties for Records</a></td>
<td>Rick Button</td>
<td>Rick Button</td>
<td></td>
</tr>
</tbody>
</table>

See also the [active proposals](README.md), [stage 1
proposals](stage-1-proposals.md), [finished
proposals](finished-proposals.md), and [inactive
proposals](inactive-proposals.md) documents.
