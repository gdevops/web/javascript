.. index::
   pair: stage 3; ESNext

.. _esnext_stage_3:

===========================================================================================
**ES.Next stage 3**
===========================================================================================

.. seealso::

   - https://github.com/tc39/proposals/


.. figure:: stage_3_proposal.png
   :align: center

   https://github.com/tc39/proposals/


- https://github.com/tc39/proposal-private-methods
- https://github.com/tc39/proposal-class-fields
- http://github.com/tc39/proposal-static-class-features/
- https://github.com/tc39/proposal-hashbang
- https://github.com/tc39/proposal-numeric-separator
- https://github.com/tc39/proposal-top-level-await
- https://github.com/tc39/proposal-weakrefs
- https://github.com/tc39/proposal-regexp-match-Indices
- https://github.com/tc39/proposal-string-replaceall
- https://github.com/tc39/proposal-promise-any
- https://github.com/tc39/proposal-atomics-wait-async
- https://github.com/tc39/proposal-logical-assignment


.. toctree::
   :maxdepth: 3

   class_fields/class_fields
   numeric_separator/numeric_separator
   top_level_await/top_level_await
