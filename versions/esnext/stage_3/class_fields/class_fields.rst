.. index::
   ! class-fields

.. _class_fields:

=============================================================================================================================================================
**ESNext.class-fields** Orthogonally-informed combination of public and private fields proposals (https://github.com/tc39/proposal-class-fields)
=============================================================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-class-fields
   - https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-30.md#private-fields-and-methods-refresher
   - https://docs.google.com/presentation/d/1lPEfTLk_9jjjcjJcx0IAKoaq10mv1XrTZ-pgERG5YoM/edit#slide=id.p

.. toctree::
   :maxdepth: 3

   README
   PRIVATE_SYNTAX_FAQ
