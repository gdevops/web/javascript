.. index::
   ! numeric_separator

.. _numeric_separator:

=============================================================================================================================================================
**ESNext.numeric_separator** A proposal to add numeric literal separators in Javascript  (https://github.com/tc39/proposal-numeric-separator)
=============================================================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-numeric-separator

.. toctree::
   :maxdepth: 3

   README
