.. index::
   ! top-level-await

.. _top_level_await:

=============================================================================================================================================================
**ESNext.top-level-await** top-level `await` proposal for ECMAScript (stage 3)   (https://github.com/tc39/proposal-top-level-await/)
=============================================================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-top-level-await/
   - https://github.com/tc39/notes/blob/master/meetings/2019-06/june-6.md#top-level-await-for-stage-3
   - https://docs.google.com/presentation/d/1K0u9QImMPK24pLVhAZHDjxI7ZVagymVU6bYWIhYQTYU/edit#slide=id.p

.. contents::
   :depth: 3

https://github.com/tc39/proposal-top-level-await/
====================================================

.. toctree::
   :maxdepth: 3

   README


Articles
=========


https://flaviocopes.com
-------------------------

.. seealso::

   - https://flaviocopes.com/javascript-await-top-level/


v8 recently introduced top-level await for ES modules.

It’s a new proposed standard for ECMAScript, which has reached stage 3.


Right now we can use await only inside async functions.
So it’s common to declare an immediately invoked async function
expression to wrap it:

::

    (async () => {
      await fetch(/* ... */)
    })()

or also declare a function and then call it::

    const doSomething = async () => {
      await fetch(/* ... */)
    }

doSomething()

**Top-level await** will allow us to simply run::

    await fetch(/* ... */)

without all this boilerplate code.


With a caveat: **this only works in ES modules**.

You can’t use this syntax outside of ES modules.

Normal scripts, and CommonJS modules, will continue to use immediately
invoked function expressions or creating ad-hoc function like always.


Implementations
=================

Nodejs 14.3.0 (2020-05-18)
------------------------------

.. seealso::

   - :ref:`nodejs_14_3_0`
