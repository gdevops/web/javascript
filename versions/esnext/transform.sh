#!/usr/bin/bash

pandoc README.md --to commonmark -o README.md

pandoc finished-proposals.md --to commonmark -o finished-proposals.md
pandoc inactive-proposals.md --to commonmark -o inactive-proposals.md

pandoc stage-0-proposals.md --to commonmark -o stage-0-proposals.md
pandoc stage-1-proposals.md --to commonmark -o stage-1-proposals.md
