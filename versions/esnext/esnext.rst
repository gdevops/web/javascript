.. index::
   pair: Javascript; ESNext

.. _esnext:

===========================================================================================
**ES.Next** is the **next** edition of the ECMAScript Language Specification
===========================================================================================

.. seealso::

   - https://en.wikipedia.org/wiki/ECMAScript#ES.Next
   - https://github.com/tc39/proposals/blob/master/README.md
   - https://kangax.github.io/compat-table/esnext/


**ES.Next** is a dynamic name that refers to whatever the next version is
at the time of writing.

**ES.Next** features are finished proposals (aka :ref:`stage 4 proposals <4_stages_proposal>`) as
listed at finished proposal that are not part of a ratified specification.

The language committee follows a "living spec" model so these changes
are part of the standard and ratification is a formality.

Subsequent ECMAScript versions (ES2018, etc.) are **always ratified in June**.


.. toctree::
   :maxdepth: 3

   tc39/tc39
   README
   finished-proposals
   stage_4/stage_4
   stage_3/stage_3
   stage_2/stage_2
   stage_1/stage_1
   stage_0/stage_0
   stage-1-proposals
   stage-0-proposals
   inactive-proposals
