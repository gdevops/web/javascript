# Inactive Proposals

Inactive proposals are proposals that at one point were presented to the
committee but were subsequently abandoned, withdrawn, or
rejected.

<table>

<colgroup>

<col style="width: 31%" />

<col style="width: 26%" />

<col style="width: 42%" />

</colgroup>

<thead>

<tr class="header">

<th>

Proposal

</th>

<th>

Champion

</th>

<th>

Rationale

</th>

</tr>

</thead>

<tbody>

<tr class="odd">

<td>

<a href="https://github.com/benjamingr/RegExp.escape"><code>RegExp.escape</code></a>

</td>

<td>

Domenic Denicola, Benjamin
Gruenbaum

</td>

<td>

<a href="https://github.com/tc39/notes/blob/master/meetings/2015-07/july-28.md#62-regexpescape">Rejected</a>:
in favor of exploring a template tag function
solution

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/tc39/ecma262/blob/master/workingdocs/callconstructor.md">Callable
class constructors</a>

</td>

<td>

Yehuda Katz and Allen Wirfs-Brock

</td>

<td>

Withdrawn: can be solved with
decorators

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/ljharb/proposal-is-error"><code>Error.isError</code></a>

</td>

<td>

Jordan Harband

</td>

<td>

Withdrawn: in favor of
<a href="https://github.com/ljharb/proposal-error-stacks"><code>Error</code>
stack traces</a>
proposal

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/DavidBruant/Map-Set.prototype.toJSON"><code>{Set,Map}.prototype.toJSON</code></a>

</td>

<td>

David Bruant and Jordan Harband

</td>

<td>

Rejected: better solved by a custom replacer function.

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/dslomov/typed-objects-es7">Typed Objects</a>

</td>

<td>

Till Schneidereit (previously Dmitry Lomov, Niko Matsakis)

</td>

<td>

Postponed: waiting for WebAssembly requirements to become clearer

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/leobalter/object-enumerables">Object
enumerables</a>

</td>

<td>

Leo Balter & John-David
Dalton

</td>

<td>

Rejected

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/tc39/proposal-cancelable-promises">Cancelable
Promises</a>

</td>

<td>

Domenic Denicola

</td>

<td>

Withdrawn

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/bmeck/UnambiguousJavaScriptGrammar">Proposed
Grammar change to ES Modules</a>

</td>

<td>

Bradley Farias

</td>

<td>

Rejected: No consensus on this specific solution.

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/caridy/proposal-dynamic-modules">Dynamic
Module Reform</a>

</td>

<td>

Caridy Patiño

</td>

<td>

Withdrawn: we decided to preserve the current semantics

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/tc39/ecmascript_simd/">SIMD.JS - SIMD
APIs</a>

</td>

<td>

Peter Jensen, Yehuda
Katz

</td>

<td>

<a href="https://github.com/tc39/notes/blob/master/meetings/2017-03/mar-21.md#conclusionresolution-10">Stage
1</a>: Start with SIMD in WASM; implementations not pursuing SIMD.js for
now.

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/tc39/proposal-ptc-syntax">Updates to Tail
Calls to include an explicit syntactic opt-in</a>

</td>

<td>

Brian Terlson & Eric
Faust

</td>

<td>

Inactive

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/sebmarkbage/ecmascript-shallow-equal">Object.shallowEqual</a>

</td>

<td>

Sebastian
Markbage

</td>

<td>

Withdrawn

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/jasnell/proposal-construct"><code>%constructor%.construct</code></a>

</td>

<td>

James M
Snell

</td>

<td>

Withdrawn

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/jasnell/proposal-istypes"><code>Builtins.typeOf()</code>
and <code>Builtins.is()</code></a>

</td>

<td>

James M Snell

</td>

<td>

Withdrawn: can be solved in other ways

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/zkat/proposal-collection-literals">Tagged
Collection Literals</a>

</td>

<td>

Kat Marchán

</td>

<td>

Withdrawn: pursuing <code>when new</code> syntax and other
alternatives

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/tc39-transfer/proposal-date-time-string-format"><code>Date.parse</code>
fallback semantics</a>

</td>

<td>

Morgan Phillips

</td>

<td>

Inactive; likely replaced by
<a href="https://github.com/gibson042/ecma262-proposal-uniform-interchange-date-parsing">uniform
parsing of quasi-standard <code>Date.parse</code>
input</a>

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/mikewest/tc39-proposal-literals">Distinguishing
literal strings</a>

</td>

<td>

Mike West & Adam Klein

</td>

<td>

Withdrawn: <a href="https://github.com/WICG/trusted-types">Trusted
Types</a> no longer a dependent

</td>

</tr>

<tr class="even">

<td>

Annex B — HTML Attribute Event Handlers

</td>

<td>

Allen Wirfs-Brock

</td>

<td>

Withdrawn per
<a href="https://github.com/tc39/ecma262/issues/1595#issuecomment-509348434">PR
comment</a>

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/tc39/notes/blob/master/meetings/2017-05/may-23.md#normative-icu-reference">Normative
ICU Reference</a>

</td>

<td>

Domenic Denicola

</td>

<td>

Withdrawn; champion is no longer participating in TC39

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/domenic/zones">Zones</a>

</td>

<td>

Domenic Denicola & Miško Hevery

</td>

<td>

Withdrawn; champion is no longer participating in TC39

</td>

</tr>

<tr class="odd">

<td>

<a href="https://github.com/domenic/proposal-blocks">Blöcks</a>

</td>

<td>

Domenic Denicola

</td>

<td>

Withdrawn; champion is no longer participating in
TC39

</td>

</tr>

<tr class="even">

<td>

<a href="https://github.com/jridgewell/proposal-regexp-atomic-and-possessive">RegExp
Atomic Groups & Possessive Quantifiers</a>

</td>

<td>

Justin Ridgewell

</td>

<td>

Never presented; engines are not interested in the feature, mainly
because it doesn’t solve backtracking for most users

</td>

</tr>

</tbody>

</table>

See also the [active proposals](README.md), [stage 1
proposals](stage-1-proposals.md), [stage 0
proposals](stage-0-proposals.md), and [finished
proposals](finished-proposals.md) documents.
