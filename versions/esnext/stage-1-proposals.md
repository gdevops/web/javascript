# Stage 1 Proposals

Stage 1 proposals represent problems that the committee is interested in
spending time exploring solutions to.

Proposals follow [this process
document](https://tc39.github.io/process-document/).

<table>
<colgroup>
<col style="width: 34%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 24%" />
</colgroup>
<thead>
<tr class="header">
<th>Proposal</th>
<th>Author</th>
<th>Champion</th>
<th><sub>Last Presented</sub></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-export-default-from"><code>export v from &quot;mod&quot;;</code> statements</a></td>
<td>Lee Byron</td>
<td>Ben Newman<br />John-David Dalton</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-07/jul-27.md#export-default-from">July 2017</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-observable">Observable</a></td>
<td>Jafar Husain</td>
<td>Jafar Husain<br />Mark Miller</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-05/may-25.md#17iiia-observable-proposal-to-stage-2">May 2017</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-ses">SES (Secure EcmaScript)</a></td>
<td>Mark Miller<br />Chip Morningstar<br />Caridy Patiño</td>
<td>Mark Miller<br />Chip Morningstar<br />Caridy Patiño</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-6.md#ses-compartments">February 2020</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/rwaldron/proposal-math-extensions"><code>Math</code> Extensions</a></td>
<td>Rick Waldron</td>
<td>Rick Waldron</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2016-07/jul-26.md#9iie-math-extensions">July 2016</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-setmap-offrom"><code>of</code> and <code>from</code> on collection constructors</a></td>
<td>Leo Balter</td>
<td>Leo Balter</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2016-09/sept-29.md#11iic-set-map-weakset-and-weakmap-of-and-from-methods">September 2016</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-generator-arrow-functions">Generator arrow functions</a></td>
<td>Sergey Rubanov</td>
<td>Brendan Eich</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2016-09/sept-27.md#11ic-generator-arrow-functions">September 2016</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-promise-try"><code>Promise.try</code></a></td>
<td>Jordan Harband</td>
<td>Jordan Harband</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2016-11/nov-29.md#11iib-promisetry">November 2016</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-Math.signbit"><code>Math.signbit</code>: IEEE-754 sign bit</a></td>
<td>JF Bastien</td>
<td>JF Bastien</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-05/may-23.md#16ib-mathsignbit-proposal">May 2017</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-error-stacks">Error stacks</a></td>
<td>Jordan Harband</td>
<td>Jordan Harband</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-01/jan-25.md#15iiia-error-stacks-seeking-stage-1">January 2017</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-do-expressions"><code>do</code> expressions</a></td>
<td>Dave Herman</td>
<td>Dave Herman</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-07/july-24.md#update-on-do-expressions">July 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://docs.google.com/presentation/d/1Ta_IbravBUOvu7LUhlN49SvLU-8G8bIQnsS08P3Z4vY/edit?usp=sharing">Float16 on TypedArrays, DataView, <code>Math.hfround</code></a></td>
<td>Leo Balter</td>
<td>Leo Balter</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-05/may-23.md#16ig-float16-on-typedarrays-dataview-mathhfround-for-stage-1">May 2017</a></sub></td>
</tr>
<tr class="even">
<td>Change <code>Number.parseInt</code>/<code>parseFloat</code> to not coerce <code>null</code>/<code>undefined</code>/<code>NaN</code> (repo link TBD)</td>
<td></td>
<td>Brendan Eich</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-07/jul-26.md#13iib-consider-changing-numberparseint-and-numberparsefloat">July 2017</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-binary-ast">Binary AST</a></td>
<td>Shu-yu Guo</td>
<td>Shu-yu Guo</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-05/may-24.md#binary-ast">May 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-pipeline-operator">Pipeline Operator</a></td>
<td>Daniel Ehrenberg</td>
<td>Daniel Ehrenberg</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-03/mar-22.md#10ive-pipeline-operator">March 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-extended-numeric-literals">Extensible numeric literals</a></td>
<td>Daniel Ehrenberg</td>
<td>Daniel Ehrenberg</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-30.md#extended-numeric-literals-status-update-and-consider-restoring-numeric-separators-to-stage-3">January 2019</a></td>
</tr>
<tr class="even">
<td><a href="https://github.com/michaelficarra/proposal-first-class-protocols">First-class protocols</a></td>
<td>Michael Ficarra</td>
<td>Michael Ficarra</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-07/july-25.md#updates-on-first-class-protocols">July 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-partial-application">Partial application</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-07/july-25.md#partial-application">July 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-cancellation">Cancellation API</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton<br />Brian Terlson</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-07/july-25.md#cancellation-update">July 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-string-prototype-codepoints"><code>String.prototype.codePoints</code></a></td>
<td>Ingvar Stepanyan</td>
<td>Mathias Bynens</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-05/may-22.md#stringprototypecodepoints-for-stage-2">May 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/keithamus/object-freeze-seal-syntax"><code>Object.freeze</code> + <code>Object.seal</code> syntax</a></td>
<td>Keith Cirkel</td>
<td>Keith Cirkel</td>
<td><sub><a href="https://github.com/keithamus/object-freeze-seal-syntax">November 2017</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/samuelgoto/proposal-block-params">Block Params</a></td>
<td>Sam Goto</td>
<td>Sam Goto</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-11/nov-30.md#9iiia-block-params-to-stage-1">November 2017</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-number-fromstring"><code>{BigInt,Number}.fromString</code></a></td>
<td>Mathias Bynens</td>
<td>Mathias Bynens</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-23.md#13iic-bigintnumberfromstring-for-stage-1">January 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-seeded-random"><code>Math.seededRandoms()</code></a></td>
<td>Tab Atkins</td>
<td>Tab Atkins</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-23.md#13iif-mathseededrandoms-for-stage-1">January 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/justinfagnani/proposal-mixins">Maximally minimal mixins</a></td>
<td>Justin Fagnani</td>
<td>Justin Fagnani</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-23.md#13iiie-maximally-minimal-mixins-proposal">January 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/keithamus/proposal-array-last">Getting last element of Array</a></td>
<td>Keith Cirkel</td>
<td>Keith Cirkel</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-24.md#13iiim-getting-last-item-from-array-for-stage-2">January 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/collection-methods">Collection methods</a></td>
<td>Michał Wadas</td>
<td>Sathya Gunasekaran</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-23.md#13iiik-new-set-builtin-methods-for-stage-2">January 2018</a> </sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-richer-keys">Richer Keys</a></td>
<td>Bradley Farias</td>
<td>Bradley Farias</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-30.md#richer-keys-for-stage-2">January 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-slice-notation/">Slice notation</a></td>
<td>Sathya Gunasekaran</td>
<td>Sathya Gunasekaran</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-03/mar-22.md#slice-notation-for-stage-1">March 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/tc39-module-keys">Module Keys</a></td>
<td>Mike Samuel</td>
<td>Mike Samuel</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-05/may-23.md#module-keys-strawman-for-stage-1">May 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-class-static-block#readme">Class Static Block</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-05/may-23.md#class-static-block">May 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-class-access-expressions">class Access Expressions</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-05/may-23.md#class-access-expressions">May 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-pattern-matching">Pattern Matching</a></td>
<td>Kat Marchán<br />Brian Terlson</td>
<td>Jordan Harband</td>
<td><sub>March 2020</sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/nodejs/dynamic-modules">Dynamic Modules</a></td>
<td>Bradley Farias</td>
<td>Bradley Farias</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-07/july-25.md#dynamic-modules">July 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-javascript-standard-library">JavaScript Standard Library</a></td>
<td>Michael Saboff<br />Mattijs Hoitink</td>
<td>Michael Saboff<br />Mattijs Hoitink</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-06/june-5.md#javascript-standard-library-for-stage-2--part-2-">June 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-modules-pragma"><code>&quot;use module&quot;</code></a></td>
<td>Dave Herman</td>
<td>Dave Herman</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-07/jul-26.md#9ivb-modulescript-pragma-for-stage-2">July 2017</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-uniform-interchange-date-parsing">uniform parsing of quasi-standard <code>Date.parse</code> input</a></td>
<td>Richard Gibson</td>
<td>Richard Gibson</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-09/sept-26.md#uniform-parsing-of-quasi-standard-dateparse-input">September 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-json-parse-with-source">JSON.parse source text access</a></td>
<td>Richard Gibson</td>
<td>Richard Gibson</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-5.md#jsonparse-source-text-access-for-stage-2">February 2020</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-idl">IDL for ECMAScript</a></td>
<td>Daniel Ehrenberg</td>
<td>Daniel Ehrenberg</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-09/sept-27.md#idl-for-javascript">September 2018</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/sebmarkbage/ecmascript-asset-references">Asset References</a></td>
<td>Sebastian Markbage</td>
<td>Sebastian Markbage</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-11/nov-28.md#asset-references-for-stage-1">November 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-freeze-prototype">Freezing prototypes</a></td>
<td>Kevin Gibbons</td>
<td>Kevin Gibbons</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-31.md#freezing-prototypes-for-stage-1">January 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/littledan/proposal-new-initialize"><code>new.initialize</code></a></td>
<td>Daniel Ehrenberg</td>
<td>Daniel Ehrenberg</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-31.md#newinitialize-for-stage-1">January 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-private-declarations">Private declarations</a></td>
<td>Justin Ridgewell</td>
<td>Justin Ridgewell</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-03/mar-28.md#private-declarations-for-stage-1">March 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-emitter">Emitter</a></td>
<td>Shu-yu Guo<br />Pedram Emrouznejad</td>
<td>Shu-yu Guo<br />Pedram Emrouznejad</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-06/june-5.md#emitter-for-stage-1">June 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-dynamic-code-brand-checks">Dynamic Code Brand Checks</a></td>
<td>Mike Samuel</td>
<td>Mike Samuel</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-5.md#dynamic-code-brand-checks-for-stage-2">December 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-reverseIterator">Reverse iteration</a></td>
<td>Leo Balter<br />Jordan Harband</td>
<td>Leo Balter<br />Jordan Harband</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-07/july-23.md#symbolreverse">July 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-object-iteration">Improving iteration on Objects</a></td>
<td>Jonathan Keslin</td>
<td>Jonathan Keslin</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-5.md#object-iteration-for-stage-2">February 2020</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-record-tuple">Record &amp; Tuple</a></td>
<td>Robin Ricard<br />Richard Button</td>
<td>Robin Ricard<br />Richard Button</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-1.md#records--tuples-for-stage-1">October 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-Declarations-in-Conditionals">Declarations in Conditionals</a></td>
<td>Devin Rousso</td>
<td>Devin Rousso</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-2.md#declarations-in-conditionals">October 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-uuid">UUID</a></td>
<td>Benjamin Coe<br />Robert Kieffer <br />Christoph Tavan</td>
<td>Benjamin Coe</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-3.md#uuid-for-stage-1">October 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-readonly-collections">Readonly Collections</a></td>
<td>Mark Miller<br />Peter Hoddie</td>
<td>Mark Miller<br />Peter Hoddie</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-3.md#readonly-collections-for-stage-1">October 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-eventual-send">Support for Distributed Promise Pipelining</a></td>
<td>Mark Miller<br />Chip Morningstar<br />Michael Fig</td>
<td>Mark Miller<br />Chip Morningstar<br />Michael Fig</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-3.md#eventual-send-support-for-distributed-promise-pipelining">October 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-wavy-dot">Wavy Dot: Syntactic Support for Promise Pipelining</a></td>
<td>Mark Miller<br />Chip Morningstar<br />Michael Fig</td>
<td>Mark Miller<br />Chip Morningstar<br />Michael Fig</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-5.md#update-on-promise-pipelining">December 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-oom-fails-fast">OOM Fails Fast</a></td>
<td>Mark Miller</td>
<td>Mark Miller</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-5.md#update-on-oom-must-fail-fast">December 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-array-filtering">Array filtering</a></td>
<td>Justin Ridgewell</td>
<td>Justin Ridgewell</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-5.md#status-update-on-array-filtering">February 2020</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-operator-overloading">Operator overloading</a></td>
<td>Daniel Ehrenberg</td>
<td>Daniel Ehrenberg</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-5.md#operator-overloading-for-stage-1">December 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-module-attributes">Module Attributes</a></td>
<td>Myles Borins<br />Sven Sauleau<br />Dan Clark<br />Daniel Ehrenberg</td>
<td>Myles Borins<br />Sven Sauleau<br />Dan Clark<br />Daniel Ehrenberg</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-6.md#module-attributes-status-update">February 2020</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://docs.google.com/presentation/d/1DsjZAzBjn2gCrr4l0uZzCymPIWZTKM8KzcnMBF31HAg/edit#slide=id.g7d23d45064_0_196">Async initialization</a></td>
<td>Bradley Farias</td>
<td>Bradley Farias</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-4.md#async-initialization-for-stage-1">February 2020</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/littledan/proposal-decimal">Decimal</a></td>
<td>Daniel Ehrenberg<br />Andrew Paprocki</td>
<td>Daniel Ehrenberg<br />Andrew Paprocki</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-4.md#bigdecimal-for-stage-1">February 2020</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/Agoric/proposal-preserve-virtualizability">Preserve Host Virtualizability</a></td>
<td>Mark Miller<br />J.F. Paradis<br />Caridy Patiño<br />Dan Finley<br />Alan Schmitt</td>
<td>Mark Miller<br />J.F. Paradis<br />Caridy Patiño<br />Dan Finley<br />Alan Schmitt</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-4.md#preserve-host-virtualizability">February 2020</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/claudepache/es-legacy-function-reflection">Legacy reflection features for functions in JavaScript</a></td>
<td>Mark Miller<br />Claude Pache<br />Jack Works</td>
<td>Mark Miller<br />Claude Pache<br />Jack Works</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-5.md#legacy-reflection-features-for-functions-in-javascript-for-stage-1">February 2020</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/rbuckton/proposal-csprng">Cryptographically Secure Random Number Generation</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-5.md#arraybufferfillrandom-for-stage-1">February 2020</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/surma/arraybufferview-stride-proposal">ArrayBuffer view stride argument</a></td>
<td>Surma</td>
<td>Shu-yu Guo</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-5.md#arraybuffer-view-stride-argument-for-stage-1">February 2020</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/Jack-Works/proposal-Number.range">Number.range &amp; BigInt.range</a></td>
<td>Jack Works</td>
<td>Jack Works</td>
<td><sub>March 2020</sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/ljharb/proposal-private-fields-in-in">Ergonomic brand checks for Private Fields</a></td>
<td>Jordan Harband</td>
<td>Jordan Harband</td>
<td><sub>March 2020</sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/bmeck/proposal-compartments">Compartments</a></td>
<td>Bradley Farias</td>
<td>Bradley Farias</td>
<td><sub>March 2020</sub></td>
</tr>
</tbody>
</table>

See also the [active proposals](README.md), [stage 0
proposals](stage-0-proposals.md), [finished
proposals](finished-proposals.md), and [inactive
proposals](inactive-proposals.md) documents.
