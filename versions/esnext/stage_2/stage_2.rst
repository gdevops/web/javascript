.. index::
   pair: stage 2; ESNext

.. _esnext_stage_2:

===========================================================================================
**ES.Next stage 2**
===========================================================================================

.. seealso::

   - https://github.com/tc39/proposals/


.. figure:: stage_2_proposal.png
   :align: center

   https://github.com/tc39/proposals/

- https://github.com/tc39/proposal-function.sent
- http://github.com/tc39/proposal-decorators
- https://github.com/tc39/proposal-throw-expressions
- https://github.com/tc39/set-methods
- https://github.com/tc39/proposal-realms
- https://github.com/domenic/proposal-arraybuffer-transfer/
- https://github.com/tc39/proposal-regexp-unicode-sequence-properties
- https://github.com/tc39/proposal-temporal
- https://github.com/tc39/proposal-collection-normalization
- https://github.com/tc39/proposal-array-is-template-object
- https://github.com/tc39/proposal-iterator-helpers
- https://github.com/tc39/proposal-array-is-template-object
- https://github.com/tc39/proposal-using-statement
- https://github.com/tc39/proposal-upsert
- https://github.com/tc39/dynamic-import-host-adjustment/blob/master/README.md

.. toctree::
   :maxdepth: 3

   temporal/temporal
   upsert/upsert
