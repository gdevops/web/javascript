.. index::
   ! temporal

.. _esnext_temporal:

=======================================================================================================================================================
**ESNext.temporal** Provides standard objects and functions for working with dates and times  (https://github.com/tc39/proposal-temporal)
=======================================================================================================================================================

.. seealso::

   - :ref:`temporal`
   - https://github.com/tc39/proposal-temporal
   - https://github.com/tc39/proposal-temporal/blob/main/polyfill
   - https://maggiepint.com/2017/04/09/fixing-javascript-date-getting-started/

.. contents::
   :depth: 3


Authors & champions
====================

- Maggie Pint
- Matt Johnson
- Philippe Dunkel
- Brian Terlson

Temporal
==============

Provides standard objects and functions for working with dates and times.

NOTE: The Polyfill_, specification text and documentation are under
continuing development and should be understood to be unstable.


.. _Polyfill:  https://github.com/tc39/proposal-temporal/blob/main/polyfill

Champions
-------------

- Maggie Pint (@maggiepint)
- Philipp Dunkel (@pipobscure)
- Matt Johnson (@mj1856)
- Brian Terlson (@bterlson)
- Shane Carr (@sffc)

Status (**stage 2**)
======================

This proposal is **currently stage 2**

Stage 3 Reviewers:

- Richard Gibson
- Bradley Farias
- Daniel Ehrenberg


Overview / Motivation
========================

.. seealso::

   - https://maggiepint.com/2017/04/09/fixing-javascript-date-getting-started/

**Date has been a long-standing pain point in ECMAScript**.

This proposes Temporal, a global Object that acts as a top-level
namespace (like Math), that brings a modern date/time API to the
ECMAScript language.

For a detailed breakdown of motivations, see: `Fixing JavaScript Date`_

Principles
--------------

- All temporal APIs are non-mutating. All temporal objects are
  effectively immutable.
- All date values are based on the Proleptic Gregorian Calendar.
  Other calendar systems are out-of-scope for this proposal.
  However, we will consider how future APIs may interact with this one
  such that extending it to support other calendars may be possible
  in a future proposal.
- All time-of-day values are based on a standard 24-hour clock.
- Leap seconds are not represented.


.. _`Fixing JavaScript Date`:  https://maggiepint.com/2017/04/09/fixing-javascript-date-getting-started/
