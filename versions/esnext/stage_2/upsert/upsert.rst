.. index::
   pair: upsert; ESNext

.. _esnext_upsert:
.. _javascript_upsert:

===========================================================================================
**upsert**
===========================================================================================

.. seealso::

   - https://github.com/tc39/proposal-upsert
   - https://dev.to/laurieontech/javascript-map-is-getting-upsert-314b
