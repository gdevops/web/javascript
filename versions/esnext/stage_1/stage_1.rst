.. index::
   pair: stage 1; ESNext

.. _esnext_stage_1:

===========================================================================================
**ES.Next stage 1**
===========================================================================================

.. seealso::

   - https://github.com/tc39/proposals/


stage-1-proposals
===================

.. toctree::
   :maxdepth: 3

   ../stage-1-proposals.md


Proposals
==========

.. toctree::
   :maxdepth: 3

   observable/observable
