.. index::
   ! observable

.. _esnext_observable:

=======================================================================================================================================================
**ESNext.observable** Observables for ECMAScript (https://github.com/tc39/proposal-observable)
=======================================================================================================================================================

.. seealso::

   - https://github.com/tc39/proposal-observable
   - :ref:`deno_x_observable`


.. contents::
   :depth: 3

.. _deno_observable_implementation:

Deno observable implementation
================================

.. seealso::

   - :ref:`deno_x_observable`
   - https://deno.land/x/observable/
   - https://github.com/bsunderhus/deno-observable/tree/master/


https://github.com/tc39/proposal-observable
=============================================

.. toctree::
   :maxdepth: 3

   README
   dom-event-dispatch
   ObservableEventTarget
   Can Observable be built on Cancel Tokens
   Why error and complete
