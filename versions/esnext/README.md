# [ECMAScript](https://github.com/tc39/ecma262) proposals

  - [Stage 1 Proposals](stage-1-proposals.md)
  - [Stage 0 Proposals](stage-0-proposals.md)
  - [Finished Proposals](finished-proposals.md)
  - [Inactive Proposals](inactive-proposals.md)

## Active proposals

Proposals follow [this process
document](https://tc39.github.io/process-document/). This list contains
only stage 2 proposals and higher that have not yet been
withdrawn/rejected, or become finished. Stage 2 indicates that the
committee expects these features to be developed and eventually included
in the standard.

### Stage 3

<table>
<colgroup>
<col style="width: 24%" />
<col style="width: 22%" />
<col style="width: 22%" />
<col style="width: 14%" />
<col style="width: 17%" />
</colgroup>
<thead>
<tr class="header">
<th>Proposal</th>
<th>Author</th>
<th>Champion</th>
<th>Tests</th>
<th><sub>Last Presented</sub></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-regexp-legacy-features">Legacy RegExp features in JavaScript</a></td>
<td>Claude Pache</td>
<td>Mark Miller<br />Claude Pache</td>
<td><a href="https://github.com/tc39/test262/issues/2371">:question:</a></td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2017-05/may-25.md#15ia-regexp-legacy-features-for-stage-3">May 2017</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-private-methods">Private instance methods and accessors</a></td>
<td>Daniel Ehrenberg</td>
<td>Daniel Ehrenberg<br />Kevin Gibbons</td>
<td><a href="https://github.com/tc39/test262/issues/1343">:question:</a></td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-30.md#private-fields-and-methods-refresher">January 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-class-fields">Class Public Instance Fields &amp; Private Instance Fields</a></td>
<td>Daniel Ehrenberg<br />Kevin Gibbons</td>
<td>Daniel Ehrenberg<br />Jeff Morrison<br />Kevin Smith<br />Kevin Gibbons</td>
<td><a href="https://github.com/tc39/test262/issues/1161">:question:</a></td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-30.md#private-fields-and-methods-refresher">January 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="http://github.com/tc39/proposal-static-class-features/">Static class fields and private static methods</a></td>
<td>Daniel Ehrenberg<br />Kevin Gibbons<br />Jeff Morrison<br />Kevin Smith</td>
<td>Shu-Yu Guo<br />Daniel Ehrenberg</td>
<td>:question:</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-30.md#private-fields-and-methods-refresher">January 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-hashbang">Hashbang Grammar</a></td>
<td>Bradley Farias</td>
<td>Bradley Farias</td>
<td><a href="https://github.com/tc39/test262/pull/2065">:white_check_mark:</a></td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-11/nov-28.md#hash-bang-grammar">November 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-numeric-separator">Numeric separators</a></td>
<td>Sam Goto<br />Rick Waldron</td>
<td>Sam Goto<br />Rick Waldron</td>
<td><a href="https://test262.report/features/numeric-separator-literal">:white_check_mark:</a></td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-03/mar-28.md#decorator-based-extended-numeric-literals-status-update-and-numeric-separators-for-stage-3">June 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-top-level-await">Top-level <code>await</code></a></td>
<td>Myles Borins</td>
<td>Myles Borins</td>
<td><a href="https://github.com/tc39/test262/pull/2274">:white_check_mark:</a></td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-06/june-6.md#top-level-await-for-stage-3">June 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-weakrefs">WeakRefs</a></td>
<td>Dean Tribble<br />Sathya Gunasekaran</td>
<td>Dean Tribble<br />Mark Miller<br />Till Schneidereit<br />Sathya Gunasekaran<br />Daniel Ehrenberg</td>
<td><a href="https://github.com/tc39/test262/pull/2192">:white_check_mark:</a></td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-5.md#update-on-weakrefs">February 2020</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-regexp-match-Indices">RegExp Match Indices</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton</td>
<td>:question:</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-3.md#regexp-match-indices-performance-feedback">December 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-string-replaceall"><code>String.prototype.replaceAll</code></a></td>
<td>Peter Marshall<br />Jakob Gruber<br />Mathias Bynens</td>
<td>Mathias Bynens</td>
<td>:question:</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-2.md#stringprototypereplaceall-for-stage-3">October 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-promise-any"><code>Promise.any</code></a></td>
<td>Mathias Bynens<br />Kevin Gibbons<br />Sergey Rubanov</td>
<td>Mathias Bynens</td>
<td><a href="https://github.com/tc39/test262/pull/2533">:white_check_mark:</a></td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-3.md#promiseany-reprise">October 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-atomics-wait-async"><code>Atomics.waitAsync</code></a></td>
<td>Lars Hansen</td>
<td>Shu-yu Guo<br />Lars Hansen</td>
<td>:question:</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-4.md#atomicswaitasync-for-stage-3">December 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-logical-assignment">Logical Assignment Operators</a></td>
<td>Justin Ridgewell</td>
<td>Justin Ridgewell <br /> Hemanth HM</td>
<td>:question:</td>
<td><sub>March 2020</sub></td>
</tr>
</tbody>
</table>

### Stage 2

<table>
<caption>white_check_mark: means a pull request for tests was merged.</caption>
<colgroup>
<col style="width: 27%" />
<col style="width: 18%" />
<col style="width: 29%" />
<col style="width: 24%" />
</colgroup>
<thead>
<tr class="header">
<th>Proposal</th>
<th>Author</th>
<th>Champion</th>
<th><sub>Last Presented</sub></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-function.sent"><code>function.sent</code> metaproperty</a></td>
<td>Allen Wirfs-Brock</td>
<td>贺师俊 (HE Shi-Jun)</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-07/july-23.md#making-functionsent-inactive">July 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="http://github.com/tc39/proposal-decorators">Decorators</a></td>
<td>Daniel Ehrenberg</td>
<td>Yehuda Katz<br />Brian Terlson<br />Daniel Ehrenberg</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-30.md#decorators-for-stage-3">January 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-throw-expressions"><code>throw</code> expressions</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-24.md#13iiii-throw-expressions-for-stage-3">January 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-function-implementation-hiding">Function implementation hiding</a></td>
<td>Domenic Denicola<br />Michael Ficarra</td>
<td>Michael Ficarra</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-4.md#update-on-function-implementation-hiding">December 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/set-methods">New Set methods</a></td>
<td>Michał Wadas<br />Sathya Gunasekaran</td>
<td>Sathya Gunasekaran</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-29.md#update-on-set-methods">January 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-realms">Realms</a></td>
<td>Caridy Patiño<br />Jean-Francois Paradis</td>
<td>Dave Herman<br />Mark Miller<br />Caridy Patiño</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-5.md#update-on-realms">February 2020</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/domenic/proposal-arraybuffer-transfer/"><code>ArrayBuffer.prototype.transfer</code></a></td>
<td>Domenic Denicola</td>
<td>Shu-yu Guo</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-07/july-24.md#arraybufferprototypetransfer">July 2018</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-regexp-unicode-sequence-properties">Sequence properties in Unicode property escapes</a></td>
<td>Mathias Bynens</td>
<td>Mathias Bynens</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-2.md#update-on-sequence-property-escapes-in-unicode-regular-expressions">October 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-temporal">Temporal</a></td>
<td>Maggie Pint<br />Matt Johnson<br />Philipp Dunkel</td>
<td>Maggie Pint<br />Philipp Dunkel<br />Brian Terlson</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2020-02/february-6.md#strings-or-symbols-for-temporalcalendar-protocol">February 2020</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-collection-normalization">collection normalization</a></td>
<td>Bradley Farias</td>
<td>Bradley Farias</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-30.md#richer-keys-for-stage-2">January 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-array-is-template-object">Array.isTemplateObject</a></td>
<td>Mike Samuel</td>
<td>Mike Samuel</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-4.md#arrayistemplateobject-update">December 2019</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-iterator-helpers">Iterator helpers</a></td>
<td>Gus Caplan</td>
<td>Michael Ficarra<br />Jonathan Keslin</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-07/july-24.md#iterator-methods-update--stage-2">July 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-using-statement">Explicit Resource Management</a></td>
<td>Ron Buckton</td>
<td>Ron Buckton</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2018-07/july-24.md#explicit-resource-management">February 2020</a></sub></td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-upsert"><code>Map.prototype.upsert</code></a></td>
<td>Bradley Farias</td>
<td>Erica Pramer</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-2.md#mapupsert--previously-mapinsertorupdate">October 2019</a></sub></td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/dynamic-import-host-adjustment/blob/master/README.md">Dynamic Import Host Adjustment</a></td>
<td>Mike Samuel</td>
<td>Mike Samuel</td>
<td><sub><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-5.md#dynamic-import-host-adjustment-for-stage-2">December 2019</a></sub></td>
</tr>
</tbody>
</table>

:question: means there is no pull request for tests yet.

:construction: means a pull request for tests was created, but not
merged yet.

### Contributing new proposals

Please see [Contributing to
ECMAScript](https://github.com/tc39/ecma262/blob/master/CONTRIBUTING.md)
for the most up-to-date information on contributing proposals to this
standard.

### Onboarding existing proposals

Proposals that are Stage 1 and above must be transferred to [the TC39
GitHub organization](https://github.com/tc39) for discoverability and
archival purposes. To onboard a proposal that lives outside the TC39
organization:

1.  Transfer your repository to the
    \[@tc39-transfer\](http://github.com/tc39-transfer) organization

<!-- end list -->

  - if you are a TC39 delegate, but not an admin in that organization,
    please contact \[@LJHarb\](https://github.com/ljharb)

<!-- end list -->

2.  \[@bterlson\](https://github.com/bterlson),
    \[@gesa\](https://github.com/gesa), or
    \[@codehag\](https://github.com/codehag) will transfer your
    repository to the TC39 organization the next chance they get.

Note that as part of the onboarding process your repository name may be
normalized. Don’t worry, repo redirects will continue to work **as long
as** you never create a fork, or a new repository, with the same name -
although Github Pages redirects will be broken (please update your
links\!).
