# Finished Proposals

Finished proposals are proposals that have reached stage 4, and are
included in the [latest draft](https://tc39.github.io/ecma262/) of the
specification.

<table>
<colgroup>
<col style="width: 28%" />
<col style="width: 21%" />
<col style="width: 22%" />
<col style="width: 17%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th>Proposal</th>
<th>Author</th>
<th>Champion(s)</th>
<th>TC39 meeting notes</th>
<th>Expected Publication Year</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="https://github.com/tc39/Array.prototype.includes"><code>Array.prototype.includes</code></a></td>
<td>Domenic Denicola</td>
<td>Domenic Denicola<br />Rick Waldron</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2015-11/nov-17.md#arrayprototypeincludes">November 2015</a></td>
<td>2016</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-exponentiation-operator">Exponentiation operator</a></td>
<td>Rick Waldron</td>
<td>Rick Waldron</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2016-01/2016-01-28.md#5xviii-exponentiation-operator-rw">January 2016</a></td>
<td>2016</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-object-values-entries"><code>Object.values</code>/<code>Object.entries</code></a></td>
<td>Jordan Harband</td>
<td>Jordan Harband</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2016-03/march-29.md#objectvalues--objectentries">March 2016</a></td>
<td>2017</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-string-pad-start-end">String padding</a></td>
<td>Jordan Harband</td>
<td>Jordan Harband<br />Rick Waldron</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2016-05/may-25.md#stringprototypepadstartend-jhd">May 2016</a></td>
<td>2017</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-object-getownpropertydescriptors"><code>Object.getOwnPropertyDescriptors</code></a></td>
<td>Jordan Harband<br />Andrea Giammarchi</td>
<td>Jordan Harband<br />Andrea Giammarchi</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2016-05/may-25.md#objectgetownpropertydescriptors-jhd">May 2016</a></td>
<td>2017</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-trailing-function-commas">Trailing commas in function parameter lists and calls</a></td>
<td>Jeff Morrison</td>
<td>Jeff Morrison</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2016-07/jul-26.md#9ie-trailing-commas-in-functions">July 2016</a></td>
<td>2017</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/ecmascript-asyncawait">Async functions</a></td>
<td>Brian Terlson</td>
<td>Brian Terlson</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2016-07/jul-28.md#10iv-async-functions">July 2016</a></td>
<td>2017</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/ecmascript_sharedmem">Shared memory and atomics</a></td>
<td>Lars T Hansen</td>
<td>Lars T Hansen</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2017-01/jan-24.md#13if-seeking-stage-4-for-sharedarraybuffer">January 2017</a></td>
<td>2017</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-template-literal-revision">Lifting template literal restriction</a></td>
<td>Tim Disney</td>
<td>Tim Disney</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2017-03/mar-21.md#10ia-template-literal-updates">March 2017</a></td>
<td>2018</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-regexp-dotall-flag"><code>s</code> (<code>dotAll</code>) flag for regular expressions</a></td>
<td>Mathias Bynens</td>
<td>Brian Terlson<br />Mathias Bynens</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2017-11/nov-28.md#9ie-regexp-dotall-status-update">November 2017</a></td>
<td>2018</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-regexp-named-groups">RegExp named capture groups</a></td>
<td>Gorkem Yakin<br />Daniel Ehrenberg</td>
<td>Daniel Ehrenberg<br />Brian Terlson<br />Mathias Bynens</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2017-11/nov-28.md#9if-regexp-named-captures-status-update">November 2017</a></td>
<td>2018</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-object-rest-spread">Rest/Spread Properties</a></td>
<td>Sebastian Markbåge</td>
<td>Sebastian Markbåge</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-23.md#restspread-properties-for-stage-4">January 2018</a></td>
<td>2018</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-regexp-lookbehind">RegExp Lookbehind Assertions</a></td>
<td>Gorkem Yakin<br />Nozomu Katō<br />Daniel Ehrenberg</td>
<td>Daniel Ehrenberg<br />Mathias Bynens</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-23.md#conclusionresolution-16">January 2018</a></td>
<td>2018</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-regexp-unicode-property-escapes">RegExp Unicode Property Escapes</a></td>
<td>Mathias Bynens</td>
<td>Brian Terlson<br />Daniel Ehrenberg<br />Mathias Bynens</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-24.md#conclusionresolution-1">January 2018</a></td>
<td>2018</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-promise-finally"><code>Promise.prototype.finally</code></a></td>
<td>Jordan Harband</td>
<td>Jordan Harband</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-24.md#conclusionresolution-2">January 2018</a></td>
<td>2018</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-async-iteration">Asynchronous Iteration</a></td>
<td>Domenic Denicola</td>
<td>Domenic Denicola</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-01/jan-25.md#conclusionresolution">January 2018</a></td>
<td>2018</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-optional-catch-binding">Optional <code>catch</code> binding</a></td>
<td>Michael Ficarra</td>
<td>Michael Ficarra</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-05/may-22.md#conclusionresolution-7">May 2018</a></td>
<td>2019</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-json-superset">JSON superset</a></td>
<td>Richard Gibson</td>
<td>Mark Miller<br />Mathias Bynens</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-05/may-22.md#conclusionresolution-8">May 2018</a></td>
<td>2019</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-Symbol-description"><code>Symbol.prototype.description</code></a></td>
<td>Michael Ficarra</td>
<td>Michael Ficarra</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-11/nov-27.md#conclusionresolution-12">November 2018</a></td>
<td>2019</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/Function-prototype-toString-revision"><code>Function.prototype.toString</code> revision</a></td>
<td>Michael Ficarra</td>
<td>Michael Ficarra</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2018-11/nov-27.md#conclusionresolution-13">November 2018</a></td>
<td>2019</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-object-from-entries"><code>Object.fromEntries</code></a></td>
<td>Darien Maillet Valentine</td>
<td>Jordan Harband<br />Kevin Gibbons</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-29.md#objectfromentries-for-stage-4">January 2019</a></td>
<td>2019</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-well-formed-stringify">Well-formed <code>JSON.stringify</code></a></td>
<td>Richard Gibson</td>
<td>Mathias Bynens</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-29.md#well-formed-jsonstringify-for-stage-4">January 2019</a></td>
<td>2019</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-string-left-right-trim"><code>String.prototype.{trimStart,trimEnd}</code></a></td>
<td>Sebastian Markbåge</td>
<td>Sebastian Markbåge<br />Mathias Bynens</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-29,md#stringprototypetrimstarttrimend-for-stage-4">January 2019</a></td>
<td>2019</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-flatMap"><code>Array.prototype.{flat,flatMap}</code></a></td>
<td>Brian Terlson<br />Michael Ficarra<br />Mathias Bynens</td>
<td>Brian Terlson<br />Michael Ficarra</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-01/jan-29.md#arrayprototypeflatflatmap-for-stage-4">January 2019</a></td>
<td>2019</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/String.prototype.matchAll"><code>String.prototype.matchAll</code></a></td>
<td>Jordan Harband</td>
<td>Jordan Harband</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-03/mar-26.md#stringprototypematchall-for-stage-4">March 2019</a></td>
<td>2020</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-dynamic-import"><code>import()</code></a></td>
<td>Domenic Denicola</td>
<td>Domenic Denicola</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-06/june-4.md#dynamic-import-for-stage-4">June 2019</a></td>
<td>2020</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-bigint"><code>BigInt</code></a></td>
<td>Daniel Ehrenberg</td>
<td>Daniel Ehrenberg</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-06/june-4.md#bigint-to-stage-4">June 2019</a></td>
<td>2020</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-promise-allSettled"><code>Promise.allSettled</code></a></td>
<td>Jason Williams<br />Robert Pamely<br />Mathias Bynens</td>
<td>Mathias Bynens</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-07/july-24.md#promiseallsettled">July 2019</a></td>
<td>2020</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-global"><code>globalThis</code></a></td>
<td>Jordan Harband</td>
<td>Jordan Harband</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-10/october-1.md#globalthis-to-stage-4">October 2019</a></td>
<td>2020</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-for-in-order"><code>for-in</code> mechanics</a></td>
<td>Kevin Gibbons</td>
<td>Kevin Gibbons</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-4.md#for-in-order-for-stage-4">December 2019</a></td>
<td>2020</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-optional-chaining">Optional Chaining</a></td>
<td>Gabriel Isenberg<br />Claude Pache<br />Dustin Savery</td>
<td>Gabriel Isenberg<br />Dustin Savery<br />Justin Ridgewell<br />Daniel Rosenwasser</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-4.md#optional-chaining-for-stage-4">December 2019</a></td>
<td>2020</td>
</tr>
<tr class="even">
<td><a href="https://github.com/tc39/proposal-nullish-coalescing">Nullish coalescing Operator</a></td>
<td>Gabriel Isenberg</td>
<td>Gabriel Isenberg<br />Justin Ridgewell<br />Daniel Rosenwasser</td>
<td><a href="https://github.com/tc39/notes/blob/master/meetings/2019-12/december-4.md#nullish-coalescing-for-stage-4">December 2019</a></td>
<td>2020</td>
</tr>
<tr class="odd">
<td><a href="https://github.com/tc39/proposal-import-meta"><code>import.meta</code></a></td>
<td>Domenic Denicola</td>
<td>Gus Caplan</td>
<td>March 2020</td>
<td>2020</td>
</tr>
</tbody>
</table>

See also the [active proposals](README.md), [stage 1
proposals](stage-1-proposals.md), [stage 0
proposals](stage-0-proposals.md), and [inactive
proposals](inactive-proposals.md) documents.
