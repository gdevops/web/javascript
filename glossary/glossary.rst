
.. index::
   pair: Javascript ; Glossary
   ! Glossary

.. _javascript_glossary:

==============================================
Glossary
==============================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Glossary

.. glossary::

   Falsy
       A falsy (sometimes written falsey) value is a value that is
       considered false when encountered in a Boolean context.

       .. seealso::

          - https://developer.mozilla.org/en-US/docs/Glossary/Falsy

   Truthy
       a truthy value is a value that is considered true when encountered
       in a Boolean context.

       All values are truthy unless they are defined as falsy (i.e., except
       for false, 0, -0, 0n, "", null, undefined, and NaN).

       .. seealso::

          - https://developer.mozilla.org/en-US/docs/Glossary/Truthy
