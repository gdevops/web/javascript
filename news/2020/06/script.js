let app = {}
function start() {
    let answers = document.querySelectorAll('.answer');
    app.rows = Array.from(document.querySelectorAll('.row'));
    /* add "next question" button to each row */
    for (row of app.rows) {
        var div = document.createElement("div");
        div.id = 'buttons-under'
        div.innerHTML = `
            <button class="next-question"><i class="icon-down-small"></i>next question</button>
            <button class="learned-something"><i class="icon-lightbulb"></i>I learned something!</button>`
        row.appendChild(div);
        div.querySelector('.next-question').onclick = function () {
            show_next_row();
            this.parentElement.parentElement.classList.add('done');
            this.parentElement.removeChild(this);
        }
        div.querySelector('.learned-something').onclick = function() {
            learned_something(this);
        }
        if (row === app.rows[app.rows.length-1]) {
            row.classList.add('last-row');
            // don't render "next question" for the last question
            div.removeChild(div.querySelector('.next-question'));
        }
    }

    $('#button-start').onclick = function() {
        show_next_row();
        this.setAttribute('style', 'display: none;');
    }

    $('#button-reveal-all').onclick = function() {
        reveal_all();
        this.parentElement.setAttribute('style', 'display: none;');
    }
    $('#send-learned').onclick = function(event) {
        event.preventDefault();
        let summary = $('#write-learned').value;
        metrics_set_learned_summary(summary);
        $('#send-learned').classList.add('sent');
        $('#send-learned').innerText = "♥ thank you ♥";
    }
}

function reveal_all() {
    document.querySelectorAll('body')[0].classList.add('all-revealed');
}

function learned_something(button) {
    button.parentElement.parentElement.classList.add('learned');
    button.innerHTML = `<i class="icon-lightbulb"></i>I learned something!
        <object data="/confetti.svg" width="30" height = "30"> </object>
        `;
    metrics_set_learned(button.parentElement.parentElement);
}

function show_next_row() {
    if (app.rows.length > 0) {
        let row = app.rows.shift();
        row.classList.add('revealed');
        row.scrollIntoView({behavior: 'smooth', block: 'center'});
        row.querySelector('.answer').onclick = function(event) {
            this.classList.add('revealed');
            this.onclick = null;
            this.parentElement.classList.add('question-revealed');
        }
    }

}

/* some anonymous metrics */

var firedb;
var metrics = {
};


function metrics_for_path() {
    var path = metrics_path();
    if (!metrics[path]) {
        metrics[path] = {'learned': [], 'summary': []};
    }
    return metrics[path];
}


function metrics_set_learned_summary(summary) {
    metrics_init();
    metrics_for_path()['summary'].push(summary);
    metrics_for_path()['latest_timestamp'] = firebase.firestore.FieldValue.serverTimestamp();
    metrics_update();
}

function metrics_set_learned(row) {
    metrics_init();
    let timestamp = firebase.firestore.FieldValue.serverTimestamp();
    var question = row.querySelectorAll('.question')[0].children[0].innerHTML;
    metrics['latest_timestamp'] = timestamp;
    metrics_for_path()['latest_timestamp'] = timestamp;
    metrics_for_path()['learned'].push(question);
    metrics_update();
}

function metrics_path() {
    return window.location.pathname.substr(1).split('.')[0]
}

function metrics_document() {
    if (!localStorage.getItem('uuid')) {
        localStorage.setItem('uuid', metrics_uuidv4());
    }
    let uuid = localStorage.getItem('uuid');
    return firedb.collection("questions").doc(uuid)
}


function metrics_uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, (c) =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

function metrics_update() {
    metrics_document().update(metrics).catch(() => metrics_document().set(metrics));
}

function metrics_init() {
    if (firedb) {
        return;
    }
    var firebaseConfig = {
        apiKey: "AIzaSyCEbH_2zf9dDzh6Muq-DdK3byHMUIsHfCA",
        authDomain: "wizard-sql-school.firebaseapp.com",
        databaseURL: "https://wizard-sql-school.firebaseio.com",
        projectId: "wizard-sql-school",
        storageBucket: "",
        messagingSenderId: "779622015462",
        appId: "1:779622015462:web:b0ca93a1d5fbb1ea2d3627"
    };
    firebase.initializeApp(firebaseConfig);
    firedb = firebase.firestore();

    metrics['start_timestamp'] = firebase.firestore.FieldValue.serverTimestamp();
}


/* utilities */

function $(selector) {
    return document.querySelector(selector);
}

function $$(selector) {
    return document.querySelectorAll(selector);
}

function $e(tag='div', props={}, children=[]){
    let el=document.createElement(tag);
    Object.assign(el, props);
    el.append(...children);
    return el;
}
