.. index::
   pair: Javascript ; Third age

.. _js_third_age:

==============================================================================
The Third Age of JavaScript by shawn swyx wang (https://www.swyx.io/)
==============================================================================

.. seealso::

   - https://www.swyx.io/writing/js-third-age/
   - https://x.com/swyx/status/1263123032328925186
   - https://github.com/sw-yx


.. contents::
   :depth: 3


Introduction
==============

Every 10 years there is a changing of the guard in JavaScript.

I think we have just started a period of accelerated change that could
in future be regarded as the Third Age of JavaScript.


The Story So Far
=================

.. seealso::

   - https://www.swyx.io/writing/js-20-years/


.. figure:: ecmascript_history.png
   :align: center

The first age of JS, from 1997-2007, started with a bang and ended with
a whimper.

You all know Brendan Eich's story, and perhaps it is less known how
the ES4 effort faltered amidst strong competition from closed ecosystems
like Flash/Actionscript.

The full origin story of JS is better told by its principal authors,
Brendan Eich and Allen Wirfs-Brock, in JavaScript: The First 20 Years.

.. https://www.swyx.io/writing/js-20-years/


The second age of JS
========================

The second age of JS, from 2009-2019, started with the annus mirabilis
of 2009, where npm, Node.js, and ES5 were born.

With Doug Crockford showing us its good parts, users built a whole host
of JS Build Tools and libraries, and extended JS' reach to both desktop
and new fangled smart phones.

Towards 2019 we even saw the emergence of specialized runtimes for JS
on phones like Facebook's Hermes as well as compiler first frontend
frameworks like Svelte 3.


The Third Age
================

2020 feels like the start of a new Age.

If the First Age was about building out a language, and the Second Age
was about users exploring and expanding the language, the Third Age is
about clearing away legacy assumptions and collapsing layers of tooling.


.. note:: I have pitched the Collapsing Layers thesis before!

The main **legacy assumption** being cleared away is the JS ecosystem's
reliance on CommonJS, which evolved as a series of compromises.

Its replacement, **ES Modules**, has been waiting in the wings for a
while, but lacked the momentum to truly take a leap because existing
tooling was slow but "good enough".

On the frontend, modern browsers are equipped to handle these in small
amounts too, but important details haven't yet been resolved.

The **Pika/Snowpack** project is positioned to accelerate this future
by providing a facade that can disappear as ES Modules get worked out.

As a final bonus, IE11 will begin its slow march to end of life starting
this year and ending in 2029.

Layers are also collapsing in interesting ways.

:ref:`Deno <deno>`  takes a radical approach of writing a whole new
runtime, collapsing a bunch of common tools doing tasks like testing,
formatting, linting and bundling into one binary, speaking TypeScript,
and even including a standard lib.

**Rome** takes a different tack, collapsing all those layers atop of
Node.js (as far as I know, I'm not too close to it).

Even in frontend frameworks, the activity going on is fascinating.

**Svelte** collapsed everything from animations to state management into a
compiler.

**React** is exploring metaframeworks and client-server integration.
And Vue is working on an "unbundler" dev server project named Vite.

The result of all of this work is both a better developer experience
(faster builds, industry standard tooling) and user experience
(smaller bundles, faster feature delivery).

.. figure:: third_age.png
   :align: center


The Death of JavaScript ?
============================

.. seealso::

   - https://www.destroyallsoftware.com/talks/the-birth-and-death-of-javascript
   - :ref:`wasmer`

If Gary Bernhardt's predictions hold true, the Third Age may be
JavaScript's last (his timeline gives JS until 2035).

There is always the looming specter of Web Assembly - even Brendan Eich
has pivoted his famous saying to "Always Bet on JS - and WASM".

He originally thought JS could be "the universal virtual machine", but
told me once that :ref:`WASM <wasmer>`  now is the ultimate
fulfillment of that idea.

.. figure:: brendan_eich.png
   :align: center

**If so - we're in the Endgame now.**
