
.. _javascript_quoi_2020_04_06:

==============================================================================
2020-04-06 ES2020 : Quoi de neuf Javascript ?
==============================================================================

.. seealso::

   - https://www.codeheroes.fr/index.php/2020/04/06/es2020-quoi-de-neuf-javascript/


Une nouvelle version du standard ECMAScript sur lequel repose JavaScript
est prévue cette année sous le nom de ES11 ou ES2020.

Voyons voir ce que nous réserve cette nouvelle mouture.
