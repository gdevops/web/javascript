.. index::
   pair: Javascript ; News

.. _javascript_news:

==============================================================================
News
==============================================================================

- https://github.com/bestofjs/bestofjs-weekly/commits/master

.. toctree::
   :maxdepth: 4

   2025/2025
   2023/2023
   2022/2022
   2021/2021
   2020/2020
