
.. _javascript_2023_04:

==============================================================================
2023-04
==============================================================================


2023-04-14 **SpiderMonkey Newsletter (Firefox 112-113)**
==============================================================================================================

- https://spidermonkey.dev/blog/2023/04/14/newsletter-firefox-112-113.html

SpiderMonkey is the JavaScript engine used in Mozilla Firefox.

This newsletter gives an overview of the JavaScript and WebAssembly work
we’ve done as part of the Firefox 112 and 113 Nightly release cycle
