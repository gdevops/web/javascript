.. index::
   pair: Javascript; maps

.. _javascript_2023_02:

==============================================================================
2023-02
==============================================================================

2023-02-23 JavaScript Import maps, Part 1: Introduction
===========================================================

- https://spidermonkey.dev/blog/2023/02/23/javascript-import-maps-part-1-introduction.html
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules
- https://hacks.mozilla.org/2015/08/es6-in-depth-modules/
- https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/

We recently shipped import maps in Firefox 108 and this article is the
first in a series that describes what they are and the problems they
can solve.

In this first article, we will go through the background and basics of
import maps and follow up with a second article explaining more details
of import maps.

Background: JavaScript Modules
------------------------------------

If you don’t know JavaScript modules, you can read the MDN docs for
JavaScript Modules first, and there are also some related articles on
Mozilla Hacks like ES6 In Depth: Modules and ES modules: A cartoon deep-dive.

If you are already familiar with them, then you are probably familiar
with static import and dynamic import. As a quick refresher:

::

    <!-- In a module script you can do a static import like so: -->
    <script type="module">
    import lodash from "/node_modules/lodash-es/lodash.js";
    </script>

    <!-- In a classic or a module script,
    you can do a dynamic import like so: -->
    <script>
    import("/node_modules/lodash-es/lodash.js");
    </script>

Notice that in both static import and dynamic import above, you need to
provide a string literal with either the absolute path or the relative path
of the module script, so the host can know where the module script is located.
