.. index::
   pair: Javascript runtime; bun
   ! bun

.. _bun_2022_07_07:

===============================================================
**Bun is a modern JavaScript runtime like Node or Deno**
===============================================================

- :ref:`bun`
