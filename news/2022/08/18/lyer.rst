
.. _javascript_2022_08_18:

=============================================================================================================
2022-08-18 **I don't know how to say it any more directly: the JS-industrial-complex is lying to you**
=============================================================================================================

- https://x.com/slightlylate/status/1560135037479096321?s=20&t=Xty4h3D0rkxcNOlXOv-9uA

.. figure:: from_alex_russel.png
   :align: center


I don't know how to say it any more directly: the JS-industrial-complex
is lying to you.

Lying about performance.

Lying about UX

Lying about DX.

Lying about ROI.

Make/serve HTML.
