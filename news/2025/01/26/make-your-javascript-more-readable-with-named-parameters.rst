

.. _taha_Shashtari_2025_01_26:

================================================================================================
2025-01-26 **Make your JavaScript more readable with named parameters** by Taha Shashtari
================================================================================================

- https://tahazsh.com/blog/named-parameters-in-js/


Make your JavaScript more readable with named parameters
===========================================================

We write and use functions all the time in JavaScript—whether you write FP or OOP. 

Every time you call a function, you need to know which arguments to pass and 
in what order.

The position of the arguments doesn’t show what they are for. 

Take a look at this example::

    addToCard(456, 2, { color: 'blue', size: 'L' }, 50, 'new10')

We might be able to guess some of them, but it’s not very clear what is being 
passed here. Additionally, we need to mentally parse each value to map it to 
something meaningful.

The only thing I can guess here is { color: 'blue', size: 'L' }, which are 
the attributes of the product the user is purchasing. 

But what are these three numbers—456, 2, and 50—for? What’s the last string, new10, for?

Now, if it were written like the following, we would understand it instantly::

    addToCard({
      productId: 456,
      quantity: 2,
      attributes: { color: 'blue', size: 'L' },
      price: 50,
      discountCode: 'new10'
    })

That’s what we call **Named Parameters**.


How the function is defined
=====================================

The above example is for the caller side. 

When defining the function, we use destructured parameters to explicitly 
indicate what parameters we expect—it also makes the function’s parameter 
list more readable.

So instead of this::

    const addToCard = (data) => {}

Write this::

    const addToCard = ({ productId, quantity, attributes, price, discountCode }) => {}


Default values
=====================

With named parameters, you can still define default values for the parameters.

::

    const addToCard = ({
      productId,
      quantity = 1,
      attributes,
      price,
      discountCode
    }) => {}

In this example, we set quantity to 1, so you can pass the other parameters 
without quantity, and it will use 1 for it.


A common pitfall
=============================

If, for some reason, the function can be called without any parameters, like 
this addToCall(), you will get an error because it cannot destructure undefined.

::

    addToCall()
    // TypeError Cannot destructure property 'productId' of 'undefined' as it is undefined.

To fix it, set the default value of the whole object to {}.

const addToCard = ({ productId, quantity, attributes, price, discountCode } = {}) => {}

One parameter
=====================

You’ll often hear that it’s best to use named parameters when you have more 
than two or three parameters. 

I think you should use them even for one parameter, because knowing what 
you’re passing is the goal.

For example, can you tell what this argument is for ?::

    processPayment(true)

Now with named parameters::


    processPayment({ notifyUser: true })


All the good things
========================

**Named parameters can drastically improve your code**.

They make your code **easier to read—you know what arguments are being passed**.

You don’t need to remember the order of the parameters. 

You can pass addToCard({ quantity: 5, productId: 456 }) or addToCard({ productId: 456, quantity: 5 }) either works!

More flexible default values—you don’t have to define parameters with default 
values as the last ones.

More scalable—you can add more parameters without breaking current calls. 

Just add the new parameter anywhere; the order doesn’t matter.



