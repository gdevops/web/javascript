.. index::
   pair: Javascript ; Browser


.. _eloq_browser:

============================================================================================
JavaScript and the Browser
============================================================================================

.. seealso::

   - https://eloquentjavascript.net/13_browser.html
