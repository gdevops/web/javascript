.. index::
   pair: Javascript ; Eloquent JavaScript

.. _eloquentjavascript:

============================================================================================
**Eloquent JavaScript**, 3rd edition by Marijn Haverbeke (https://x.com/MarijnJH)
============================================================================================

.. seealso::

   - https://github.com/marijnh/Eloquent-JavaScript
   - https://eloquentjavascript.net/
   - https://eloquentjavascript.net/code
   - https://eloquentjavascript.net/Eloquent_JavaScript.pdf
   - https://javascript.developpez.com/tutoriels/livre-complet/eloquent-javascript/
   - https://marijnhaverbeke.nl/
   - https://x.com/MarijnJH
   - https://github.com/marijnh


.. figure:: eloquentjavascript.png
   :align: center

   https://eloquentjavascript.net/


:tool: https://codemirror.net/6/


.. toctree::
   :maxdepth: 3

   intro/intro
   source/source
   browser/browser
   modules/modules
   dom/dom
