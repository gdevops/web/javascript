
.. _eloquentjavascript_intro:

============================================================================================
**Eloquent JavaScript** introduction
============================================================================================


.. contents::
   :depth: 3

Description
============

This is a book about JavaScript, programming, and the wonders of the
digital. You can read it online here, or get your own paperback copy.

Written by Marijn Haverbeke (https://x.com/MarijnJH).

Licensed under a Creative Commons attribution-noncommercial license.

All code in this book may also be considered licensed under an MIT license.

Illustrations by various artists: Cover and chapter illustrations by
Madalina Tantareanu.

Pixel art in Chapters 7 and 16 by Antonio Perdomo Pastor.

Regular expression diagrams in Chapter 9 generated with regexper.com by
Jeff Avallone.

Village photograph in Chapter 11 by Fabrice Creuzot.

Game concept for Chapter 15 by Thomas Palef.

Other pages
================

- Code sandbox and exercise solutions
- Errata for the paper book
- This book as a single PDF file (& small version for mobile)
- This book as an EPUB file
- This book as a MOBI (Kindle) file
- The first edition of the book
- The second edition of the book
