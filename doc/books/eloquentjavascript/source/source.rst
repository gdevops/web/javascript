.. index::
   pair: Github ; Eloquent JavaScript

.. _eloquentjavascript_source:

============================================================================================
**Eloquent JavaScript** on github
============================================================================================

.. seealso::

   - https://github.com/marijnh/Eloquent-JavaScript
   - https://codemirror.net/6/


.. toctree::
   :maxdepth: 3

   README
