.. index::
   pair: Javascript ; Books

.. _javascript_books:

=======================
Javascript books
=======================

.. toctree::
   :maxdepth: 3

   eloquentjavascript/eloquentjavascript
