
.. index::
   pair: Javascript ; Frameworks
   pair: Javascript ; Technical debt
   pair:  Web ; frameworks
   ! Web frameworks

.. _javascript_frameworks_2:

==============================================
Javascript frameworks and the technical debt
==============================================

.. seealso::

   - :ref:`javascript_frameworks`


.. contents::
   :depth: 3


Problems: the valse of Javascript frameworks : the technical debt
================================================================================

.. seealso::

   - :ref:`web_javascript_technical_debt`
