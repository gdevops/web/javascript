.. index::
   pair: Browser Engine; Gecko

.. _gecko:

=====================
**gecko** engine
=====================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Mozilla/Gecko
   - https://github.com/mozilla/gecko-dev
   - https://en.wikipedia.org/wiki/Gecko_%28software%29#Web_browsers
   - https://en.wikipedia.org/wiki/Comparison_of_browser_engines_(HTML_support)


.. figure:: Mozillagecko-logo.png
   :align: center
