.. index::
   pair: Browser Engine; Blink

.. _blink:

==========================
**blink** browser engine
==========================

.. seealso::

   - https://en.wikipedia.org/wiki/Blink_(browser_engine)
   - https://chromium.googlesource.com/chromium/src/+/master/third_party/blink/
