
.. _ECMAScript_definition:

=======================================
JavaScript/ECMAScript definitions
=======================================

.. seealso::

   - https://tc39.es/ecma262/
   - https://es.discourse.group/
   - https://en.wikipedia.org/wiki/ECMAScript
   - https://fr.wikipedia.org/wiki/JavaScript

.. contents::
   :depth: 3

ECMAScript Wikipedia definition
=================================

.. seealso::

   - https://en.wikipedia.org/wiki/ECMAScript

ECMAScript (or ES) is a scripting-language specification standardized
by Ecma International. It was created to standardize JavaScript to help
foster multiple independent implementations.

JavaScript has remained the most widely used implementation of ECMAScript
since the standard was first published, with other implementations including
JScript and ActionScript.[2]

ECMAScript is commonly used for client-side scripting on the World Wide Web,
and it is increasingly being used for writing server applications and
services using Node.js.

JavaScript Wikipedia definition
=================================

.. seealso::

   - https://fr.wikipedia.org/wiki/JavaScript

**JavaScript (/ˈdʒɑːvəˌskrɪpt/)**, often abbreviated as JS, is a programming
language that conforms to the ECMAScript specification.

JavaScript is high-level, often just-in-time compiled, and multi-paradigm.

It has curly-bracket syntax, dynamic typing, prototype-based object-orientation,
and first-class functions.

Alongside HTML and CSS, JavaScript is one of the core technologies of
the World Wide Web.

JavaScript enables interactive web pages and is an essential part of
web applications. The vast majority of websites use it for client-side
page behavior, and all major web browsers have a dedicated JavaScript
engine to execute it.

As a multi-paradigm language, JavaScript supports event-driven, functional,
and imperative programming styles.

It has application programming interfaces (APIs) for working with text,
dates, regular expressions, standard data structures, and the
Document Object Model (DOM).

However, the language itself does not include any input/output (I/O),
such as networking, storage, or graphics facilities, as the host
environment (usually a web browser) provides those APIs.

JavaScript engines were originally used only in web browsers, but they
are now embedded in some servers, usually via Node.js.

They are also embedded in a variety of applications created with
frameworks such as Electron and Cordova.

Although there are similarities between JavaScript and Java, including
language name, syntax, and respective standard libraries, the two
languages are distinct and differ greatly in design.
