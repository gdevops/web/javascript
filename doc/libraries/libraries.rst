.. index::
   pair: Javascript ; Libraries

.. _javascript_libraries:

=======================
Javascript libraries
=======================

- https://weekly.bestofjs.org/
- https://gomakethings.com/whats-a-javascript-library/

.. toctree::
   :maxdepth: 3

   alpine/alpine
   datetimes/datetimes
   htmx/htmx
   jquery/jquery
   polyfill/polyfill
   popper.js/popper.js
   tom_select/tom_select
   select2/select2
   stimulus/stimulus
   turbolinks/turbolinks
