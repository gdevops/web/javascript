.. index::
   pair: Javascript Library ; datetimes
   ! datetimes

.. _javascript_datetimes:

===================================
Javascript **datetime** libraries
===================================

.. seealso::

   - :ref:`javascript_date_programming`
