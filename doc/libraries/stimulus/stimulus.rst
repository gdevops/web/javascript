.. index::
   pair: Javascript Library ; stimulus
   ! stimulus

.. _stimulus:

=============================================================================
**stimulus** A modest JavaScript framework for the HTML you already have
=============================================================================

.. seealso::

   - https://stimulusjs.org/handbook/origin
   - https://github.com/stimulusjs/stimulus

.. contents::
   :depth: 3

How Stimulus differs from mainstream JavaScript frameworks
=============================================================

This makes Stimulus very different from the majority of contemporary
JavaScript frameworks.

Almost all are focused on turning JSON into DOM elements via a template
language of some sort.

Many use these frameworks to birth an empty page, which is then filled
exclusively with elements created through this JSON-to-template rendering.

Stimulus also differs on the question of state.

Most frameworks have ways of maintaining state within JavaScript objects,
and then render HTML based on that state.

Stimulus is the exact opposite. State is stored in the HTML, so that
controllers can be discarded between page changes, but still reinitialize
as they were when the cached HTML appears again.

It really is a remarkably different paradigm.

One that I’m sure many veteran JavaScript developers who’ve been used
to work with contemporary frameworks will scoff at.

And hey, scoff away.

If you’re happy with the complexity and effort it takes to maintain an
application within the maelstrom of, say, React + Redux, then
Turbolinks + Stimulus will not appeal to you.

If, on the other hand, you have nagging sense that what you’re working
on does not warrant the intense complexity and application separation
such contemporary techniques imply, then you’re likely to find refuge
in our approach.
