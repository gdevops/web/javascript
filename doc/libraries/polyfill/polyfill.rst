.. index::
   pair: Javascript libraries; polyfill

.. _polyfill:

=====================
**polyfill**
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/Polyfill
   - https://en.wikipedia.org/wiki/Polyfill_(programming)
   - https://github.com/financial-times/polyfill-service
   - https://polyfill.io/v3/
   - https://vanillajspodcast.com/everything-you-ever-wanted-to-know-about-polyfills/


Links
======

- Polyfill.io: https://polyfill.io/
- Polyfill examples: https://gist.github.com/cferdinandi/e3bc14f7d9dece812d4124b9609ee3ea
