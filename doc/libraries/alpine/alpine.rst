.. index::
   pair: Javascript; Alpine

.. _alpine_js:

=========================================================================================
alpine (A rugged, minimal framework for composing JavaScript behavior in your markup. )
=========================================================================================

- https://github.com/alpinejs/alpine
- https://x.com/Alpine_JS
