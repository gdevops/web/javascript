.. index::
   pair: Javascript Library ; turbolinks
   ! turbolinks

.. _turbolinks:

=======================================================================
**turbolinks** Turbolinks makes navigating your web application faster
=======================================================================

.. seealso::

   - https://github.com/turbolinks/turbolinks
   - https://gomakethings.com/always-bet-on-html/
   - https://mxb.dev/blog/the-return-of-the-90s-web/
   - https://caniuse.com/#search=pushState
   - https://caniuse.com/#search=requestAnimationFrame

.. contents::
   :depth: 3

How Stimulus differs from mainstream JavaScript frameworks
=============================================================

Turbolinks® makes navigating your web application faster.

Get the performance benefits of a single-page application without the
added complexity of a client-side JavaScript framework.

Use HTML to render your views on the server side and link to pages as
usual.

When you follow a link, Turbolinks automatically fetches the page,
swaps in its <body>, and merges its <head>, all without incurring the
cost of a full page load.

Turbolinks works in all modern desktop and mobile browsers.

It depends on the HTML5 History API and Window.requestAnimationFrame.

In unsupported browsers, Turbolinks gracefully degrades to standard navigation.
