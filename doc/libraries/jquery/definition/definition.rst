
.. index::
   pair: Definition ; jQuery

.. _jquery_def:

============================
jQuery definition
============================

.. contents::
   :depth: 3

English wikipedia defintion
============================

.. seealso::

   - https://en.wikipedia.org/wiki/JQuery


jQuery is a JavaScript library designed to simplify HTML DOM tree traversal
and manipulation, as well as event handling, CSS animation, and Ajax.

It is free, open-source software using the permissive MIT License.

Web analysis (from 2017) indicates that it is the most widely deployed
JavaScript library by a large margin.

jQuery's syntax is designed to make it easier to navigate a document, select
DOM elements, create animations, handle events, and develop Ajax applications.

jQuery also provides capabilities for developers to create plug-ins on top
of the JavaScript library.

This enables developers to create abstractions for low-level interaction and
animation, advanced effects and high-level, themeable widgets.

The modular approach to the jQuery library allows the creation of powerful
dynamic web pages and Web applications.

The set of jQuery core features—DOM element selections, traversal and
manipulation—enabled by its selector engine (named "Sizzle" from v1.3),
created a new "programming style", fusing algorithms and DOM data structures.

This style influenced the architecture of other JavaScript frameworks like
YUI v3 and Dojo, later stimulating the creation of the standard Selectors API.

Microsoft and Nokia bundle jQuery on their platforms.

Microsoft includes it with Visual Studio for use within Microsoft's
ASP.NET AJAX and ASP.NET MVC frameworks while Nokia has integrated
it into the Web Run-Time widget development platform.


French wikipedia defintion
============================

.. seealso::

   - https://fr.wikipedia.org/wiki/JQuery


jQuery est une bibliothèque JavaScript libre et multiplateforme créée pour
faciliter l'écriture de scripts côté client dans le code HTML des pages web.

La première version est lancée en janvier 2006 par John Resig.

La bibliothèque contient notamment les fonctionnalités suivantes :

- parcours et modification du DOM (y compris le support des sélecteurs
  CSS 1 à 3 et un support basique de XPath) ;
- événements ;
- effets visuels et animations ;
- manipulations des feuilles de style en cascade (ajout/suppression des
  classes, d'attributs…) ;
- Ajax ;
- plugins ;
- Utilitaires (version du navigateur web…).

Depuis sa création en 2006 et notamment à cause de la complexification
croissante des interfaces Web, jQuery a connu un large succès auprès des
développeurs Web et son apprentissage est aujourd'hui un des fondamentaux
de la formation aux technologies du Web.

Il est à l'heure actuelle la librairie front-end la plus utilisée au monde
(plus de la moitié des sites Internet en ligne intègrent jQuery).
