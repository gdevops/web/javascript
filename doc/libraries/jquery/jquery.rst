.. index::
   pair: Javascript Library ; jQuery

.. _jquery:

================================
**jQuery** Javascript library
================================

.. seealso::

   - https://fr.wikipedia.org/wiki/JQuery
   - https://github.com/jquery/jquery
   - https://jquery.com/
   - https://developer.mozilla.org/en-US/docs/Glossary/jQuery


.. figure:: Jquery-logo.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   ajax/ajax
   versions/versions
