.. index::
   pair: Versions ; jQuery

.. _jquery_versions:

============================
jQuery versions
============================

.. seealso::

   - https://github.com/jquery/jquery/releases

.. toctree::
   :maxdepth: 3

   4.0.0/4.0.0
   3.5.0/3.5.0
   3.4.1/3.4.1
   3.4.0/3.4.0
   3.3.1/3.3.1
   3.2.1/3.2.1
