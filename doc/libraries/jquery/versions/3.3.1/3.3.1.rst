
.. index::
   pair: 3.3.1 ; jQuery

.. _jquery_3_3_1:

============================
jQuery 3.3.1 (2018-01-20)
============================

.. seealso::

   - https://github.com/jquery/jquery/tree/3.3.1
