

.. _simpleisbetterthancomplex_ajax:

=============================================================================================================
https://simpleisbetterthancomplex.com/tutorial/2016/08/29/how-to-work-with-ajax-request-with-django.html
=============================================================================================================

.. seealso::

   - https://simpleisbetterthancomplex.com/tutorial/2016/08/29/how-to-work-with-ajax-request-with-django.html
   - :ref:`django_javascript_ajax`
