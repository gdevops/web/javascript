

.. _jquery_ajax_examples:

=================================================================
**jQuery Ajax** examples
=================================================================

.. seealso::

   - :ref:`django_javascript_ajax`

.. toctree::
   :maxdepth: 3


   simpleisbetterthancomplex/simpleisbetterthancomplex
