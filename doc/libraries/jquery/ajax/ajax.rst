
.. index::
   pair: Ajax ; jQuery

.. _jquery_ajax:

===========================================================================================
**jQuery Ajax** Perform an asynchronous HTTP (Ajax) request), **XMLHttpRequest Web API**
===========================================================================================

.. seealso::

   - https://api.jquery.com/jQuery.ajax/#jQuery-ajax-url-settings
   - :ref:`XMLHttpRequest_web_api`
   - :ref:`fetch_web_api`
   - https://www.modernjsbyexample.net/book/010-scrapped-material
   - https://simpleisbetterthancomplex.com/tutorial/2016/08/29/how-to-work-with-ajax-request-with-django.html

.. toctree::
   :maxdepth: 3

   examples/examples
