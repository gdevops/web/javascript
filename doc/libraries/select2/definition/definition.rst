
.. _select2_def:

==============================================================================
**select2** definition
==============================================================================

.. contents::
   :depth: 3


Select2
=======

|Build Status| |Financial Contributors on Open Collective| |cdnjs|
|jsdelivr|

Select2 is a jQuery-based replacement for select boxes.

It supports searching, remote data sets, and pagination of results.

To get started, checkout examples and documentation at https://select2.org/

Use cases
---------

-  Enhancing native selects with search.
-  Enhancing native selects with a better multi-select interface.
-  Loading data from JavaScript: **easily load items via AJAX and have
   them searchable**.
-  Nesting optgroups: native selects only support one level of nesting.
   Select2 does not have this restriction.
-  Tagging: ability to add new items on the fly.
-  **Working with large, remote datasets: ability to partially load a
   dataset based on the search term**.
-  Paging of large datasets: easy support for loading more pages when
   the results are scrolled to the end.
-  Templating: support for custom rendering of results and selections.

Browser compatibility
---------------------

-  IE 8+
-  Chrome 8+
-  Firefox 10+
-  Safari 3+
-  Opera 10.6+

Usage
-----

You can source Select2 directly from a CDN like
`jsDelivr <https://www.jsdelivr.com/package/npm/select2>`__ or
`cdnjs <http://www.cdnjs.com/libraries/select2>`__, `download it from
this GitHub repo <https://github.com/select2/select2/releases>`__, or
use one of the integrations below.

Integrations
------------

Third party developers have created plugins for platforms which allow
Select2 to be integrated more natively and quickly. For many platforms,
additional plugins are not required because Select2 acts as a standard
``<select>`` box.

Plugins

-  `Django <https://www.djangoproject.com/>`__
-  `django-autocomplete-light <https://github.com/yourlabs/django-autocomplete-light>`__
-  `django-easy-select2 <https://github.com/asyncee/django-easy-select2>`__
-  `django-select2 <https://github.com/applegrew/django-select2>`__
-  `Drupal <https://www.drupal.org/>`__ -
   `drupal-select2 <https://www.drupal.org/project/select2>`__
-  `Meteor <https://www.meteor.com/>`__ -
   `meteor-select2 <https://github.com/nate-strauser/meteor-select2>`__
-  `Ruby on Rails <http://rubyonrails.org/>`__ -
   `select2-rails <https://github.com/argerim/select2-rails>`__
-  `Wicket <https://wicket.apache.org/>`__ -
   `wicketstuff-select2 <https://github.com/wicketstuff/core/tree/master/select2-parent>`__
-  `Yii 2 <http://www.yiiframework.com/>`__ -
   `yii2-widget-select2 <https://github.com/kartik-v/yii2-widget-select2>`__
-  `Angularjs <https://angularjs.org/>`__ -
   `mdr-angular-select2 <https://github.com/modulr/mdr-angular-select2>`__

Themes

-  `Bootstrap 3 <https://getbootstrap.com/>`__ -
   `select2-bootstrap-theme <https://github.com/select2/select2-bootstrap-theme>`__
-  `Bootstrap 4 <https://getbootstrap.com/>`__ -
   `select2-bootstrap4-theme <https://github.com/ttskch/select2-bootstrap4-theme>`__
-  `Flat UI <http://designmodo.github.io/Flat-UI/>`__ -
   `select2-flat-theme <https://github.com/techhysahil/select2-Flat_Theme>`__
-  `Metro UI <http://metroui.org.ua/>`__ -
   `select2-metro <http://metroui.org.ua/select2.html>`__

Missing an integration? Modify this ``README`` and make a pull request
back here to Select2 on GitHub.

Internationalization (i18n)
---------------------------

Select2 supports multiple languages by simply including the right
language JS file (``dist/js/i18n/it.js``, ``dist/js/i18n/nl.js``, etc.)
after ``dist/js/select2.js``.

Missing a language? Just copy ``src/js/select2/i18n/en.js``, translate
it, and make a pull request back to Select2 here on GitHub.

Documentation
-------------

The documentation for Select2 is available `online at the documentation
website <https://select2.org>`__ and is located within the ```docs``
directory of this
repository <https://github.com/select2/select2/tree/develop/docs>`__.

Community
---------

You can find out about the different ways to get in touch with the
Select2 community at the `Select2 community
page <https://select2.org/getting-help>`__.

Copyright and license
---------------------

The license is available within the repository in the
`LICENSE <LICENSE.md>`__ file.

Contributors
------------

Code Contributors
~~~~~~~~~~~~~~~~~

This project exists thanks to all the people who contribute.
[`Contribute <.github/CONTRIBUTING.md>`__].

Financial Contributors
~~~~~~~~~~~~~~~~~~~~~~

Become a financial contributor and help us sustain our community.
[`Contribute <https://opencollective.com/select2/contribute>`__]

Individuals
^^^^^^^^^^^

Organizations
^^^^^^^^^^^^^

Support this project with your organization. Your logo will show up here
with a link to your website.
[`Contribute <https://opencollective.com/select2/contribute>`__]

.. |Build Status| image:: https://github.com/select2/select2/workflows/CI/badge.svg
.. |Financial Contributors on Open Collective| image:: https://opencollective.com/select2/all/badge.svg?label=financial+contributors
   :target: https://opencollective.com/select2
.. |cdnjs| image:: https://img.shields.io/cdnjs/v/select2.svg
   :target: http://www.cdnjs.com/libraries/select2
.. |jsdelivr| image:: https://data.jsdelivr.com/v1/package/npm/select2/badge
   :target: https://www.jsdelivr.com/package/npm/select2
