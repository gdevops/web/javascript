.. index::
   pair: Versions ; select2

.. _select2_versions:

==============================================================================
select2 versions
==============================================================================

.. seealso::

   - https://github.com/select2/select2/releases

.. toctree::
   :maxdepth: 3

   4.0.13/4.0.13
   4.0.3/4.0.3
