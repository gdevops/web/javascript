"""Définition des formulaires de saisie concernant les achats d'articles.


"""
# https://docs.python.org/3.6/library/datetime.html#datetime.date
import logging

from dal import autocomplete
from django import forms
from django.forms.models import inlineformset_factory

from .models_achat_service import AchatService
from .models_demande_article import DemandeArticle

# https://docs.djangoproject.com/en/dev/topics/forms/modelforms/
# https://github.com/yourlabs/django-autocomplete-light

# Get an instance of a logger
logger = logging.getLogger(__name__)


__all__ = (
    "AchatServiceFormSetExtra0",
    "AchatServiceFormSetExtra1",
    "AchatServiceFormAutoComplete",
)


class AchatServiceForm(forms.ModelForm):
    """https://django-test-autocomplete.readthedocs.io/en/latest/tests/django_autocomplete_light/django_autocomplete_light.html#projects-forms-django-autocomplete-light-py"""

    class Meta:
        model = AchatService
        fields = (
            "id",
            "etat_poste",
            "article",
            "description",
            "projet",
            "prestataire",
            "quantite",
            "prix_unitaire",
            "date_butoir",
            # la partie réponse
            "reponse",
        )
        widgets = {
            "article": autocomplete.ModelSelect2(
                url="articles:api_get_articles",
                attrs={
                    # Set some placeholder
                    "data-placeholder": "Choisissez l'article",
                    # Only trigger autocompletion after n characters have been typed
                    "data-minimum-input-length": 1,
                },
            ),
            "projet": autocomplete.ModelSelect2(
                url="projets:api_get_projects_encours",
                attrs={
                    # Set some placeholder
                    "data-placeholder": "Choisissez le projet",
                    # Only trigger autocompletion after n characters have been typed
                    "data-minimum-input-length": 1,
                },
            ),
            "prestataire": autocomplete.ModelSelect2(
                url="tiers:api_get_prestataires",
                attrs={
                    # Set some placeholder
                    "data-placeholder": "Choisissez le prestataire",
                    # Only trigger autocompletion after n characters have been typed
                    "data-minimum-input-length": 1,
                },
            ),
            # issue 97
            # "quantite": forms.NumberInput(attrs={"step": 1}),
            "reponse": forms.Textarea(attrs={"cols": 80, "rows": 2}),
            "description": forms.Textarea(attrs={"cols": 80, "rows": 2}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # If you pass FormHelper constructor a form instance
        # It builds a default layout with all its fields
        self.fields["etat_poste"].required = False
        self.fields["description"].required = False
        self.fields["prix_unitaire"].required = False


"""
parent_model, model, form=ModelForm,
formset=BaseInlineFormSet, fk_name=None,
fields=None, exclude=None, extra=3, can_order=False,
can_delete=True, max_num=None, formfield_callback=None,
widgets=None, validate_max=False, localized_fields=None,
labels=None, help_texts=None, error_messages=None,
min_num=None, validate_min=False, field_classes=None
"""
# les achats liés à la demande
# https://docs.djangoproject.com/en/dev/topics/forms/modelforms/#model-formsets
# https://docs.djangoproject.com/fr/2.0/ref/forms/models/#django.forms.models.inlineformset_factory
AchatServiceFormSetExtra0 = inlineformset_factory(
    DemandeArticle,  # parent_model
    AchatService,  # model
    form=AchatServiceForm,  # form=ModelForm
    extra=0,
    can_order=True,
)
AchatServiceFormSetExtra1 = inlineformset_factory(
    DemandeArticle,  # parent_model
    AchatService,  # model
    form=AchatServiceForm,  # form=ModelForm
    extra=1,
    can_order=True,
)
