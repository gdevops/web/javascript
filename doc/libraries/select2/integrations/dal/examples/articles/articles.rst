.. index::
   pair: dal ; articles

.. _dal_articles:

==============================================================================
Django-autocomplete-light **articles example**
==============================================================================

.. seealso::

   - :ref:`dal_installation`


.. contents::
   :depth: 3


**config.settings.base.py**
==============================

.. code-block:: python
   :linenos:


    INSTALLED_APPS = (
        "django.contrib.admin",
        # https://github.com/yourlabs/django-autocomplete-light (**)
        "dal",
        "dal_select2",
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.messages",
        # https://docs.djangoproject.com/en/dev/intro/tutorial06/
        # https://docs.djangoproject.com/en/dev/howto/static-files/
        "django.contrib.staticfiles",
        # https://simpleisbetterthancomplex.com/series/2017/10/09/a-complete-beginners-guide-to-django-part-6.html
        # https://docs.djangoproject.com/en/dev/ref/contrib/humanize/
        "django.contrib.humanize",
        # https://django-import-export.readthedocs.org/en/latest/installation.html#settings
        # "import_export",
        # https://django-extensions.readthedocs.org/en/latest
        "django_extensions",
        # https://github.com/zostera/django-bootstrap4 (GUI)
        "bootstrap4",
        # https://github.com/django-crispy-forms/django-crispy-forms
        "crispy_forms",
        # https://github.com/jazzband/django-embed-video
        # "embed_video",
        # https://github.com/django-money/django-money
        # Add djmoney to your INSTALLED_APPS. This is required so that money field are displayed correctly in the admin
        "djmoney",
        # to access to the current request and user, which is useful outside of a view when the
        # “request” variable is not defined.
        # https://github.com/svetlyak40wt/django-globals
        "django_globals",
        # https://github.com/stefanfoulis/django-phonenumber-field
        "phonenumber_field",
    )


**articles.views.py**
========================

.. code-block:: python
   :linenos:


    """Vues concernant les articles.


    .. seealso::

       - https://django-test-autocomplete.readthedocs.io/en/latest/tests/django_autocomplete_light/django_autocomplete_light.html

    """
    import logging

    # https://docs.djangoproject.com/en/dev/ref/models/querysets/#q-objects
    from django.db.models import Q

    from dal import autocomplete

    from .models import Article

    # Get an instance of a logger
    logger = logging.getLogger(__name__)


    __all__ = ("ArticleAutocomplete",)


    class ArticleAutocomplete(autocomplete.Select2QuerySetView):
        """
        """

        def get_queryset(self):
            # Don't forget to filter out results depending on the visitor !
            # if not self.request.user.is_authenticated:
            #    return Article.objects.none()
            # logger.debug("ArticleAutocomplete.get_queryset() has been called")
            qs = Article.objects.all()
            if self.q:
                qs = qs.filter(Q(code__icontains=self.q) | Q(designation__icontains=self.q))

            return qs


**articles.urls.py**
======================


::


    ###########################################################################
    # https://django-test-autocomplete.readthedocs.io/en/latest/tests/django_autocomplete_light/django_autocomplete_light.html#projects-api-get-users-q-projects-urls-py
    # http://127.0.0.1:8003/articles/api/get_articles/
    # http://127.0.0.1:8003/articles/api/get_articles/?q=SECA
    url(r"^api/get_articles/$", ArticleAutocomplete.as_view(), name="api_get_articles"),


**articles.forms_achat_service.py**
======================================

Utilisation de autocomplete.ModelSelect2

.. literalinclude:: forms_achat_service.py
   :linenos:


articles.templates.articles.articles_update.html
====================================================

.. literalinclude:: articles_update.html
   :linenos:
