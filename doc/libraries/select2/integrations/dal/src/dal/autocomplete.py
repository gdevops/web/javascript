"""Optionnal shortcuts module for DAL.

This module exports all the public classes for the project. It imports dal
module classes and extension module classes by checking the INSTALLED_APPS
setting:

- if dal_select2 is present, import classes from dal_select2_*,
- with dal_queryset_sequence, import views and fields from it,
- with dal_queryset_sequence and dal_select2, import from
  dal_select2_queryset_sequence,
- with django.contrib.contenttypes, import dal_contenttypes,
- with genericm2m, import dal_genericm2m,
- with gm2m, import dal_gm2m,
- with taggit, import dal_taggit,
- with tagulous, import dal_tagulous.

Note that using this module is optional.
"""
from django.conf import settings as django_settings


def _installed(*apps):
    for app in apps:
        if app not in django_settings.INSTALLED_APPS:
            return False
    return True


if _installed("dal_select2"):
    pass

if _installed("dal_queryset_sequence"):
    pass

if _installed("dal_select2", "dal_queryset_sequence"):
    pass

if _installed("dal_select2") and _installed("taggit"):
    pass

if _installed("dal_select2") and _installed("tagging"):
    pass

if _installed("genericm2m") and _installed("dal_queryset_sequence"):
    pass

if _installed("gm2m") and _installed("dal_queryset_sequence"):
    pass
