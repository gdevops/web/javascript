.. index::
   pair: dal ; src

.. _dal_src:

==============================================================================
**Django-autocomplete-light** source code
==============================================================================

.. seealso::

   - https://github.com/yourlabs/django-autocomplete-light


.. contents::
   :depth: 3


.. _dal_auto_complete:

autocomplete
================

.. literalinclude:: dal/autocomplete.py
   :linenos:
