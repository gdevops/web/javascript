.. index::
   pair: dal ; select2

.. _select2_dal:

==============================================================================
**Django-autocomplete-light** (dal)
==============================================================================

.. seealso::

   - https://github.com/yourlabs/django-autocomplete-light


.. toctree::
   :maxdepth: 3

   installation/installation
   examples/examples
   src/src
   versions/versions
