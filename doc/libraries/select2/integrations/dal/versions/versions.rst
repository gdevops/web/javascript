.. index::
   pair: Django-autocomplete-light ; Versions

.. _dal_versions:

==============================================================================
**Django-autocomplete-light** versions
==============================================================================

.. seealso::

   - https://github.com/yourlabs/django-autocomplete-light


.. toctree::
   :maxdepth: 3

   3.5.1/3.5.1
