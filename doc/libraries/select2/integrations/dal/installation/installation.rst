.. index::
   pair: dal ; installation

.. _dal_installation:

==============================================================================
**Django-autocomplete-light** installation
==============================================================================

.. seealso::

   - https://django-autocomplete-light.readthedocs.io/en/master/install.html#install-in-your-project


.. contents::
   :depth: 3

pipenv
========

::

    pipenv install django-autocomplete-light


How to use in your project
===============================

.. seealso::

   - :ref:`dal_articles`
