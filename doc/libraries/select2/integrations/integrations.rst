.. index::
   pair: integrations ; select2

.. _select2_integrations:

==============================================================================
**select2** integrations
==============================================================================

.. seealso::

   - https://github.com/select2/select2

.. toctree::
   :maxdepth: 3

   dal/dal
