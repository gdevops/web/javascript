.. index::
   pair: Javascript Library ; select2

.. _select2_library:

==============================================================================
**select2** Javascript library is a jQuery based replacement for select boxes
==============================================================================

.. seealso::

   - https://github.com/select2/select2
   - https://select2.org/
   - https://forums.select2.org/
   - https://select2.org/troubleshooting/getting-help


.. figure:: select2_logo.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   installation/installation
   integrations/integrations
   versions/versions
