
.. _select2_install:

==============================================================================
**select2** installation
==============================================================================



tree -L 2
=========

::

    tree -L 2


::


    ├── css
    │   ├── select2.css
    │   └── select2.min.css
    └── js
        ├── i18n
        ├── select2.full.js
        ├── select2.full.min.js
        ├── select2.js
        └── select2.min.js
