
.. index::
   pair: Javascript ; history
   ! history

.. _javascript_history:

==============================================
Javascript history
==============================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Une_r%C3%A9introduction_%C3%A0_JavaScript
