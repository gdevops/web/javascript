
.. index::
   pair: yarn ; Nodejs

.. _yarn:

=================================================================
**yarn** (reliable, and secure dependency management)/facebook
=================================================================

.. seealso::

   - https://github.com/yarnpkg/yarn
   - https://yarnpkg.com/fr/
   - https://x.com/yarnpkg


.. toctree::
   :maxdepth: 4

   versions/versions
