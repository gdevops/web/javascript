
.. index::
   pair: yarn ; Versions

.. _yarn_versions:

=================================================================
yarn versions
=================================================================

.. seealso::

   - https://github.com/yarnpkg/yarn/releases
   - https://github.com/yarnpkg/yarn/releases/latest

.. toctree::
   :maxdepth: 4

   1.16.0/1.16.0
