
.. index::
   pair: Javascript ; Packaging

.. _javascript_packaging:

=====================
Javascript packaging
=====================


.. toctree::
   :maxdepth: 6

   npm/npm
   npx/npx
   yarn/yarn
