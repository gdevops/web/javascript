
.. index::
   ! npx

.. _npx:

===================================
npx (execute npm package binaries)
===================================


.. seealso::

   - https://github.com/zkat/npx
