
.. index::
   pair: npm ; Versions

.. _npm_versions:

==================================
**npm** versions
==================================

.. seealso::

   - https://github.com/npm/cli/releases

.. toctree::
   :maxdepth: 4

   7.5.3/7.5.3
   6.13.7/6.13.7
   6.9.0/6.9.0
   6.8.0/6.8.0
   6.7.0/6.7.0
