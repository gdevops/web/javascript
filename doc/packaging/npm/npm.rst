
.. index::
   pair: npm ; Nodejs

.. _npm:

==================================
**npm** (Node.js Package Manager)
==================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Npm
   - https://github.com/npm/cli
   - https://npm.community/


.. figure:: Npm-logo.svg.png
   :align: center


.. toctree::
   :maxdepth: 4

   definition/definition
   versions/versions
   npmjs_com/npmjs_com
   commands/commands
