
.. index::
   pair: id3 ; npmjs packages

.. _id3_npmjs_packages:

=========================
id3 npmjs.com packages
=========================

.. seealso::

   - https://www.npmjs.com/products/enterprise
   - https://docs.npmjs.com/


.. figure:: id3_3_packages.png
   :align: center

.. toctree::
   :maxdepth: 3

   face_detection/face_detection
   face_matching/face_matching
   face_pad/face_pad
