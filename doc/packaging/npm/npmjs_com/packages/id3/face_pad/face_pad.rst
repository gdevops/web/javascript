
.. index::
   pair: face-pad ; package


.. _id3_face_pad:

=================================
**face-pad**  package
=================================

.. seealso::

   - https://www.npmjs.com/package/@id3/face-pad


.. figure:: face_pad.png
   :align: center


Installation
==============

::

    npm i @id3/face-pad
