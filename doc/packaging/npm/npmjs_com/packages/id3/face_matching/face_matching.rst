
.. index::
   pair: face-matching ; package


.. _id3_face_matching:

=================================
**face-matching**  package
=================================

.. seealso::

   - https://www.npmjs.com/package/@id3/face-matching


.. figure:: face_matching.png
   :align: center


Installation
==============

::

    npm i @id3/face-matching
