
.. index::
   pair: face-detection ; package


.. _id3_face_detection:

=================================
**face-detection**  package
=================================

.. seealso::

   - https://www.npmjs.com/package/@id3/face-detection


.. figure:: face_detection.png
   :align: center


Installation
==============

::

    npm i @id3/face-detection
