
.. index::
   pair: tslib ; package


.. _yslib:

=====================
tslib package
=====================

.. seealso::

   - https://www.npmjs.com/package/tslib


Installing
=============

For the latest stable version, run::

    # TypeScript 2.3.3 or later
    npm install --save tslib

    # TypeScript 2.3.2 or earlier
    npm install --save tslib@1.6.1
