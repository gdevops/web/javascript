.. index::
   pair: Javascript ; FAQs

.. _javascript_faqs:

============================================================================================
Javascript FAQs
============================================================================================


.. contents::
   :depth: 3


How to disable the carriage return ?
=======================================


Use the keydown" event + event.preventDefault()
---------------------------------------------------

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Element/keydown_event
   - https://developer.mozilla.org/fr/docs/Web/API/Event/preventDefault
   - https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault


Use the keydown" event + event.preventDefault()


Example::

    /**
     * disable_carriage_return()
     * On inactive le traitement du retour chariot ainsi que la saisie des lettres
     * de a à z.
     * Voir https://developer.mozilla.org/en-US/docs/Web/API/Element/keydown_event
     *
     */
    function disable_carriage_return() {
        console.log(`disable_carriage_return()`);
        const CHAR_CARRIAGE_RETURN_13 = 13;
        const CHAR_a_65 = 65;
        const CHAR_z_90 = 90;
        {% for tuple_indice, v in dict_fiches_temps.items %}
            document.getElementById('id_fiches_temps_{{ v }}-temps_impute').addEventListener("keydown", event => {
            console.log(`${event.code}=>${event.keyCode}`);
            if (event.keyCode === CHAR_CARRIAGE_RETURN_13) {
                // saisie du retour chariot bloquée
                event.preventDefault();
            } else if ((event.keyCode < CHAR_a_65) || (event.keyCode > CHAR_z_90)) {
                // saisie des lettres de a à z bloquée
                event.preventDefault();
            }
        });
        {% endfor %}
    };
