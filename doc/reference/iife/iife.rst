.. index::
   pair: Function ; IIFE (Immediately Invokable Function Expression)
   ! IIFE

.. _functions_iife:

=========================================================
**IIFE** (Immediately Invokable **Function Expression**)
=========================================================

.. seealso::

   - :ref:`iife_mozilla`

.. toctree::
   :maxdepth: 3

   damien_cosset/damien_cosset
