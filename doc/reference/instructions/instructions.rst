.. index::
   pair: Javascript; instructions
   ! instructions

.. _javascript_instructions:

==============================================================================
Javascript **instructions**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions


.. toctree::
   :maxdepth: 3

   const/const
   let/let
