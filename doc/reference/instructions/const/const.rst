.. index::
   ! const

.. _javascript_const:

==============================================================================
**const**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const

.. contents::
   :depth: 3

Definition
============

Constants are block-scoped, much like variables defined using the let keyword.

The value of a constant can't be changed through reassignment, and it
can't be redeclared.

This declaration creates a constant whose scope can be either global or
local to the block in which it is declared.

Global constants do not become properties of the window object, unlike var variables.

An initializer for a constant is required. You must specify its value in
the same statement in which it's declared. (This makes sense, given that
it can't be changed later.)

The const declaration creates a read-only reference to a value.
It does not mean the value it holds is immutable—just that the variable
identifier cannot be reassigned.

For instance, in the case where the content is an object, this means
the object's contents (e.g., its properties) can be altered.

All the considerations about the "temporal dead zone" apply to both let and const.

A constant cannot share its name with a function or a variable in the same scope.


Définition en français
========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/const

La déclaration const permet de créer une constante nommée accessible
uniquement en lecture.

Cela ne signifie pas que la valeur contenue est immuable, uniquement que
l'identifiant ne peut pas être réaffecté.

Autrement dit la valeur d'une constante ne peut pas être modifiée par
des réaffectations ultérieures.

**Une constante ne peut pas être déclarée à nouveau**.


https://flaviocopes.com/javascript-let-const/
=================================================

.. seealso::

   - https://flaviocopes.com/javascript-let-const/

In JavaScript, we commonly declare variables using two keywords: let and const.
When should we use one vs the other?
**I always default to using const**.
Why ?
Because const guarantees the value can’t be reassigned.
When programming, I always think that the best thing that I can use is
the thing that can harm me the least.
We have an incredible amount of things that can generate problems.
The more power you give to something, the more responsibility you assign to it.
And we don’t generally want that.
Well, it’s debatable, of course, as everything. I don’t want that, and
that’s enough for me.

If I declare a variable using let, I let it be reassignable:

.. code-block:: javascript

    let number = 0
    number = 1

and in some cases this is necessary.

If I want the variable to be reassignable, let is perfect.

If I don’t, which is in 80% of the cases, I don’t even what that option
be available. I want the compiler (interpreter, in the case of JS) to
give me an error.

That’s why I default to const every time I declare a variable, and only
switch to let when I want the reassign ability to be allowed.
