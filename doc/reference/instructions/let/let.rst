.. index::
   ! let

.. _javascript_let:

==============================================================================
**let**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let
   - https://stackoverflow.com/questions/37916940/why-was-the-name-let-chosen-for-block-scoped-variable-declarations-in-javascri

.. contents::
   :depth: 3

Definition
============


The let statement declares a block-scoped local variable, optionally
initializing it to a value.


**let** allows you to declare variables that are limited to the scope
of a block statement, or expression on which it is used, unlike the
**var** keyword, which defines a variable globally, or locally to an
entire function regardless of block scope.

The other difference between var and let is that the latter is initialized
to a value only when a parser evaluates it (see below).

Just like const the let does not create properties of the window object
when declared globally (in the top-most scope).

An explanation of why the name "let" was chosen can be found here_.


.. _here:  https://stackoverflow.com/questions/37916940/why-was-the-name-let-chosen-for-block-scoped-variable-declarations-in-javascri



History
========

.. seealso::

   - https://stackoverflow.com/questions/37916940/why-was-the-name-let-chosen-for-block-scoped-variable-declarations-in-javascri

Let is a mathematical statement that was adopted by early programming
languages like Scheme and Basic.

Variables are considered low level entities not suitable for higher
levels of abstraction, thus the desire of many language designers
to introduce similar but more powerful concepts like in Clojure, F#,
Scala, where let might mean a value, or a variable that can be assigned,
but not changed, which in turn lets the compiler catch more programming
errors and optimize code better.

JavaScript has had var from the beginning, so they just needed another
keyword, and just borrowed from dozens of other languages that use let
already as a traditional keyword as close to var as possible, although
in JavaScript let creates block scope local variable instead.
