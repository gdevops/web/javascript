
.. _promise_lydia_hallie:

==============================================================================
**Promise** JavaScript Visualized: **Promises & Async/Await**
==============================================================================

.. seealso::

   - https://dev.to/lydiahallie/javascript-visualized-promises-async-await-5gke
   - https://x.com/lydiahallie
   - https://github.com/lydiahallie
   - https://github.com/lydiahallie/javascript-questions

.. contents::
   :depth: 3

Définition
============

Ever had to deal with JS code that just... didn't run the way you expected
it to? Maybe it seemed like functions got executed at random, unpredictable
times, or the execution got delayed.

There's a chance you were dealing with a cool new feature that ES2015
introduced: Promises!

My curiosity from many years ago has paid off and my sleepless nights
have once again given me the time to **make some animations**.

**Time to talk about Promises**: why would you use them, how do they
work "under the hood", and how can we write them in the most modern way ?
