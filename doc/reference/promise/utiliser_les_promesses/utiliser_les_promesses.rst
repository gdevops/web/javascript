
.. _promise_utiliser_les_promesses:

==============================================================================
**Promise** utiliser les promesses
==============================================================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_promesses


.. contents::
   :depth: 3

Définition
============

Une promesse est un objet (**Promise**) qui représente la complétion ou
l'échec d'une opération asynchrone.

La plupart du temps, on **consomme** des promesses et c'est donc ce que
nous verrons dans la première partie de ce guide pour ensuite expliquer
comment les créer.

En résumé, une promesse est un objet qui est renvoyé et auquel on attache
des callbacks plutôt que de passer des callbacks à une fonction.

Ainsi, au lieu d'avoir une fonction qui prend deux callbacks en arguments :

.. code-block:: javascript
   :linenos:

    function faireQqcALAncienne(successCallback, failureCallback){
      console.log("C'est fait");
      // réussir une fois sur deux
      if (Math.random() > .5) {
        successCallback("Réussite");
      } else {
        failureCallback("Échec");
      }
    }

    function successCallback(résultat) {
      console.log("L'opération a réussi avec le message : " + résultat);
    }


    function failureCallback(erreur) {
      console.error("L'opération a échoué avec le message : " + erreur);
    }

    faireQqcALAncienne(successCallback, failureCallback);


On aura une fonction qui renvoie une promesse et on attachera les
callbacks sur cette promesse :

.. code-block:: javascript
   :linenos:

    function faireQqc() {
      return new Promise((successCallback, failureCallback) => {
        console.log("C'est fait");
        // réussir une fois sur deux
        if (Math.random() > .5) {
          successCallback("Réussite");
        } else {
          failureCallback("Échec");
        }
      })
    }

    const promise = faireQqc();
    promise.then(successCallback, failureCallback);

ou encore :

.. code-block:: javascript
   :linenos:

    faireQqc().then(successCallback, failureCallback);

Garanties
==============

À la différence des imbrications de callbacks, une **promesse** apporte
certaines garanties :

- Les callbacks ne seront jamais appelés avant la fin du parcours de la
  boucle d'évènements JavaScript courante
- Les callbacks ajoutés grâce à then seront appelés, y compris après
  le succès ou l'échec de l'opération asynchrone
- Plusieurs callbacks peuvent être ajoutés en appelant then plusieurs
  fois, ils seront alors exécutés l'un après l'autre selon l'ordre dans
  lequel ils ont été insérés.


Chaînage des promesses
=========================

Un besoin fréquent est d'exécuter deux ou plus d'opérations asynchrones
les unes à la suite des autres, avec chaque opération qui démarre
lorsque la précédente a réussi et en utilisant le résultat de l'étape
précédente. Ceci peut être réalisé en créant une chaîne de promesses.

La méthode then() renvoie une nouvelle promesse, différente de la première :

.. code-block:: javascript
   :linenos:

    const promise = faireQqc();
    const promise2 = promise.then(successCallback, failureCallback);

ou encore :

.. code-block:: javascript
   :linenos:

    const promise2 = faireQqc().then(successCallback, failureCallback);

La deuxième promesse (promise2) indique l'état de complétion, pas
uniquement pour faireQqc() mais aussi pour le callback qui lui a été
passé (successCallback ou failureCallback) qui peut aussi être une
fonction asynchrone qui renvoie une promesse.

Lorsque c'est le cas, tous les callbacks ajoutés à promise2 forment une
file derrière la promesse renvoyée par successCallback ou failureCallback.

Autrement dit, chaque promesse représente l'état de complétion d'une
étape asynchrone au sein de cette succession d'étapes.


Auparavant, l'enchaînement de plusieurs opérations asynchrones déclenchait
une pyramide dantesque de callbacks :

.. code-block:: javascript
   :linenos:

    faireQqc(function(result) {
      faireAutreChose(result, function(newResult) {
        faireUnTroisiemeTruc(newResult, function(finalResult) {
          console.log('Résultat final :' + finalResult);
        }, failureCallback);
      }, failureCallback);
    }, failureCallback);

Grâce à des fonctions plus modernes et aux promesses, on attache les
callbacks aux promesses qui sont renvoyées.

On peut ainsi construire une chaîne de promesses :


.. code-block:: javascript
   :linenos:

    faireQqc().then(function(result) {
      return faireAutreChose(result);
    })
    .then(function(newResult) {
      return faireUnTroisiemeTruc(newResult);
    })
    .then(function(finalResult) {
      console.log('Résultat final : ' + finalResult);
    })
    .catch(failureCallback);

Les arguments passés à then sont optionnels.

La forme **catch(failureCallback)** est un alias plus court pour **then(null, failureCallback)**.

Ces chaînes de promesses sont parfois construites avec des fonctions fléchées :

.. code-block:: javascript
   :linenos:

    faireQqc()
    .then(result => faireAutreChose(result))
    .then(newResult => faireUnTroisiemeTruc(newResult))
    .then(finalResult => {
      console.log('Résultat final : ' + finalResult);
    })
    .catch(failureCallback);
