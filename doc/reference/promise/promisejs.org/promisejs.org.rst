
.. _promise_promisejs_org:

==============================================================================
**Promise** promisejs.org
==============================================================================

.. seealso::

   - https://www.promisejs.org/
   - https://github.com/ForbesLindesay/promisejs.org
   - https://www.promisejs.org/patterns/
   - https://www.youtube.com/watch?v=qbKWsbJ76-s
