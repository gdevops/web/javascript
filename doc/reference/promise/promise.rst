.. index::
   ! Promise

.. _promise:

==============================================================================
**Promise**
==============================================================================

.. toctree::
   :maxdepth: 3

   definition/definition
   flaviocopes.com/flaviocopes.com
   javascript.info/javascript.info
   lydia_hallie/lydia_hallie
   promisejs.org/promisejs.org
   utiliser_les_promesses/utiliser_les_promesses
