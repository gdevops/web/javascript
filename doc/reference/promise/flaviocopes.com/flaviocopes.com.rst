
.. _promise_flavio_copes:

==============================================================================
**Promise** by Flavio Copes (https://x.com/flaviocopes)
==============================================================================

.. seealso::

   - https://flaviocopes.com/javascript-promises/
   - https://flaviocopes.com/javascript-async-await/

.. contents::
   :depth: 3

Definition
===========

Promises are one way to deal with asynchronous code in JavaScript, without
writing too many callbacks in your code.


Introduction to promises
=============================

A promise is commonly defined as a proxy for a value that will eventually
become available.

Promises are one way to deal with asynchronous code, without writing
too many callbacks in your code.

Although they’ve been around for years, they were standardized and
introduced in ES2015, and now they have been superseded in ES2017
by async functions.

Async functions use the promises API as their building block, so
understanding them is fundamental **even if in newer code you’ll
likely use async functions instead of promises**.


Example of chaining promises
===============================

.. code-block:: javascript

    const status = response => {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
      }
      return Promise.reject(new Error(response.statusText))
    }

    const json = response => response.json()

    fetch('/todos.json')
      .then(status)
      .then(json)
      .then(data => {
        console.log('Request succeeded with JSON response', data)
      })
      .catch(error => {
        console.log('Request failed', error)
      })


So given those premises, this is what happens: the first promise in the
chain is a function that we defined, called status(), that checks the
response status and if it’s not a success response (between 200 and 299),
it rejects the promise
