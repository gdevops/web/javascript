.. index::
   pair: ES6 modules ; Javascript
   pair: modules ; Javascript
   pair: ES2015 ; modules


.. _javascript_modules:

==============================================================================
Javascript ES2015 (ES6) modules
==============================================================================

.. seealso::

   - https://caniuse.com/#feat=es6-module
   - https://ponyfoo.com/articles/es6-modules-in-depth
   - https://jakearchibald.com/2017/es-modules-in-browsers/
   - https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export
   - https://tc39.es/ecma262/#sec-exports
   - https://tech.mozfr.org/post/2018/04/06/Une-plongee-illustree-dans-les-modules-ECMAScript
   - https://dev.to/tomiiide/import-subfolder-modules-in-javascript-like-a-boss-15f7

.. toctree::
   :maxdepth: 3

   browsers/browsers
