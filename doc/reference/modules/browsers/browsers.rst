.. index::
   pair: ES2015 modules ; Browsers

.. _javascript_modules_browsers:

==============================================================================
Javascript **ES2015 (ES6) modules in the browsers**
==============================================================================

.. seealso::

   - https://jakearchibald.com/2017/es-modules-in-browsers/
   - https://stackoverflow.com/questions/950087/how-do-i-include-a-javascript-file-in-another-javascript-file
   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import
   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/import
   - https://javascript.info/import-export
   - https://medium.com/ghostcoder/using-es6-modules-in-the-browser-5dce9ca9e911
   - https://yakovfain.com/2015/05/30/ecmascript-6-modules-in-the-browser-with-traceur/

.. contents::
   :depth: 3

ECMAScript modules in browsers
=================================

.. seealso::

   - https://stackoverflow.com/questions/950087/how-do-i-include-a-javascript-file-in-another-javascript-file

Browsers have had support for loading ECMAScript modules directly
(no tools like Webpack required) since Safari 10.1, Chrome 61,
**Firefox 60** (2018-05-08), and Edge 16.

Check the current support at caniuse.

There is no need to use Node.js' .mjs extension;

**Browsers completely ignore file extensions on modules/scripts**.


https://eloquentjavascript.net/13_browser.html
================================================

.. seealso::

   - https://eloquentjavascript.net/13_browser.html
   - https://eloquentjavascript.net/10_modules.html#es

You can load ES modules (see `Chapter 10`_) in the browser by giving your
script tag a **type="module"** attribute.

Such modules can depend on other modules by using URLs relative to
themselves as module names in import declarations.

.. _`Chapter 10`:  https://eloquentjavascript.net/10_modules.html#es

Quelle est la prise en charge actuelle des modules ES ? (2018)
=================================================================

.. seealso::

   - https://tech.mozfr.org/post/2018/04/06/Une-plongee-illustree-dans-les-modules-ECMAScript
   - https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Releases/60

Avec la sortie de :ref:`Firefox 60 en mai 2018 <firefox_60_es_2015>`, l’ensemble des principaux
navigateurs prendra en charge les modules ES par défaut.

Node travaille également sur le sujet et un groupe de travail dédié se
concentre sur les problèmes de compatibilité entre les modules CommonJS
et les modules ES.

Cela signifie qu’on pourra utiliser la balise script avec **type=module**
ainsi que des imports et des exports.

D’autres fonctionnalités relatives aux modules sont dans les tuyaux.

La proposition concernant :ref:`l’import dynamique <es2020_dynamic_import>`
fait partie de :ref:`ES2020 <es2020>`.

Il en va de même avec :ref:`import.meta <es2020_import_meta>` qui aidera
à gérer certains cas d’utilisation pour Node.js.

Enfin, la proposition sur la résolution des modules aidera à atténuer
les différences entre les navigateurs et Node.js.
En bref, le meilleur reste à venir.


.. _firefox_60_es_2015:

https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Releases/60
========================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Releases/60
   - https://bugzilla.mozilla.org/show_bug.cgi?id=1438139
   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script#attr-type
   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script#attr-nomodule
   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import
   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export

JavaScript
--------------

**ECMAScript 2015 modules** have been enabled by default in (bug 1438139_).
See `ES6 In Depth Modules`_ and `ES modules A cartoon deep dive`_ for more
information, or consult MDN reference docs:

- `<script src="main.js" type="module">`_
- `<script nomodule src="fallback.js">`_
- import_ statement
- export_ statement


.. _1438139: https://bugzilla.mozilla.org/show_bug.cgi?id=1438139
.. _`ES6 In Depth Modules`: https://hacks.mozilla.org/2015/08/es6-in-depth-modules/
.. _`ES modules A cartoon deep dive`: https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/
.. _`<script src="main.js" type="module">`: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script#attr-type
.. _`<script nomodule src="fallback.js">`: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script#attr-nomodule
.. _import: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import
.. _export: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export

Updated
---------

- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export

Useful guides
--------------

- https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/
- https://hacks.mozilla.org/2015/08/es6-in-depth-modules/


ES6 module with Django template
==================================

.. toctree::
   :maxdepth: 3

   django/django
