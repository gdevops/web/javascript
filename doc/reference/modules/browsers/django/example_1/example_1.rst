
.. _es6_django_1:

==============================================================================
**ES2015 modules with django example 1**
==============================================================================

.. contents::
   :depth: 3

constants.js
================

.. code-block:: javascript

    // constants.js
    // Fichier des constantes Javascript

    export const HELP_MESSAGE_HYPERTEXTE = "Syntaxe markdown pour créer un lien hypertexte => [texte](url)";


Django template
=================

.. code-block:: django


    {% block javascript %}
        {# a employer dans les templates fils->{{ block.super }} #}
        {{ block.super }}

        <!-- datetime picker jQuery script -->
        <script src="{% static 'js/jquery.datetimepicker.full.js' %}"></script>

        <!-- Javascript for autocomplete_light -->
        <script src="{% static 'autocomplete_light/autocomplete.init.js' %}"></script>
        <script src="{% static 'select2/js/select2.full.js' %}"></script>
        <script src="{% static 'autocomplete_light/forward.js' %}"></script>
        <script src="{% static 'autocomplete_light/select2.js' %}"></script>

        {# https://github.com/sparksuite/simplemde-markdown-editor #}
        <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
        <!-- script type="module" src="{% static 'js/constants.js' %}" ></script -->

        <script type="module">
            import { HELP_MESSAGE_HYPERTEXTE } from '{% static 'js/constants.js' %}';

            const MSG_HELP_MESSAGE_HYPERTEXTE = HELP_MESSAGE_HYPERTEXTE;
            window.MSG_HELP_MESSAGE_HYPERTEXTE = MSG_HELP_MESSAGE_HYPERTEXTE;
            console.log(`message=${MSG_HELP_MESSAGE_HYPERTEXTE} window.MSG_HELP_MESSAGE_HYPERTEXTE=${window.MSG_HELP_MESSAGE_HYPERTEXTE}`);
        </script>

    {% endblock javascript %}



    ...

    $(document).ready(function() {
        try {
            console.log(`window.MSG_HELP_MESSAGE_HYPERTEXTE=${window.MSG_HELP_MESSAGE_HYPERTEXTE}`);
            console.log(`globalThis.MSG_HELP_MESSAGE_HYPERTEXTE=${globalThis.MSG_HELP_MESSAGE_HYPERTEXTE}`);
        }
        catch(err)
        {
            console.log(`${err}`);
        }
        // langue française pour les datetime pickers
        $.datetimepicker.setLocale('fr');
        {% for form_codif_nomenclature in formset_codif_nomenclature %}
                var reponse_mde_{{ forloop.counter0 }} = new SimpleMDE(
                    {
                        element: document.getElementById('id_codifnomenclature_related-{{ forloop.counter0 }}-reponse'),
                        toolbar: false,
                        status: false,
                        spellChecker: false,
                    });

                {# Par défaut le mode preview est activé #}
                reponse_mde_{{ forloop.counter0 }}.togglePreview();
                reponse_mde_{{ forloop.counter0 }}.codemirror.on("blur", function(){
                    let reponse = reponse_mde_{{ forloop.counter0 }}.value();
                    console.log("reponse=", reponse );
                    let codifnomenclature_id = $('#id_codifnomenclature_related-{{ forloop.counter0 }}-id').val();
                    let url = '{% url "articles:ajax_update_codif_nomenclature" %}';
                    $.ajax(
                            {
                                url: url,
                                data: {
                                    'id': codifnomenclature_id,
                                    'reponse': reponse,
                                },
                                dataType: 'json',
                                success: function (data) {
                                }
                        }
                    );
                });
               document.getElementById("id_reponse_toggle_preview_{{ forloop.counter0 }}").onclick = function()
               {
                   reponse_mde_{{ forloop.counter0 }}.togglePreview();
               };

        {% endfor %}

        {% for form_codif_nomenclature in formset_codif_nomenclature %}
            var designation_mde_{{ forloop.counter0 }} = new SimpleMDE(
                {
                    element: document.getElementById('id_codifnomenclature_related-{{ forloop.counter0 }}-designation'),
                    toolbar: false,
                    status: false,
                    spellChecker: false,
                });

            {# Par défaut le mode preview est activé #}
            designation_mde_{{ forloop.counter0 }}.togglePreview();

            designation_mde_{{ forloop.counter0 }}.codemirror.on("blur", function(){
                let designation = designation_mde_{{ forloop.counter0 }}.value();
                console.log("designation", designation );
                let codifnomenclature_id = $('#id_codifnomenclature_related-{{ forloop.counter0 }}-id').val();
                let url = '{% url "articles:ajax_update_codif_nomenclature" %}';
                $.ajax(
                        {
                            url: url,
                            data: {
                                'id': codifnomenclature_id,
                                'designation': designation,
                            },
                            dataType: 'json',
                            success: function (data) {
                            }
                    }
                );
            });
           document.getElementById("id_description_toggle_preview_{{ forloop.counter0 }}").onclick = function()
           {
               designation_mde_{{ forloop.counter0 }}.togglePreview();
           };

        {% endfor %}

        var decription_mde = new SimpleMDE(
            {
                element: document.getElementById('id_description'),
                toolbar: false,
                status: false,
                spellChecker: false,
            });

        {# Par défaut le mode preview est activé #}
        decription_mde.togglePreview();
        document.getElementById("id_description_toggle_preview").onclick = function()
        {
            {# https://www.w3schools.com/howto/howto_js_toggle_text.asp #}
                {# https://www.w3schools.com/icons/fontawesome_icons_webapp.asp #}
            {# <span class="fa fa-eye" style="font-size:25px" id="id_description_toggle_preview" data-toggle="tooltip" data-placement="top" title="Write"</span> #}
            let x = document.getElementById("id_description_toggle_flip");
            if (x.title === "Write") {
                x.title = "Preview";
                x.classList.remove("fa-pencil");
                x.classList.add("fa-eye");
            } else {
                x.title = "Write";
                x.classList.remove("fa-eye");
                x.classList.add("fa-pencil");
            }
            let msg_1 = document.getElementById("id_description_toggle_msg");
            if (x.title === "Write") {
                msg_1.innerHTML = globalThis.MSG_HELP_MESSAGE_HYPERTEXTE;
            } else {
                msg_1.innerHTML = "";
            }
            decription_mde.togglePreview();
        };
