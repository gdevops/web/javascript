.. index::
   pair: Javascript; statements
   ! statements

.. _javascript_statements:

==============================================================================
Javascript **statements**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements

.. toctree::
   :maxdepth: 3


   for_in/for_in
   for_of/for_of
   for_await_of/for_await_of
