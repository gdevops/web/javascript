.. index::
   pair: Javascript; for await of
   ! for await of

.. _javascript_for_await_of:

==============================================================================
for await of
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for-await...of


.. contents::
   :depth: 3

Definition
===========

The for await...of statement creates a loop iterating over async iterable
objects as well as on sync iterables, including: built-in String, Array,
Array-like objects (e.g., arguments or NodeList), TypedArray, Map, Set,
and user-defined async/sync iterables.

It invokes a custom iteration hook with statements to be executed for
the value of each distinct property of the object.

Like await operator, the statement can only be used inside an async function.


Définition en français
========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/for-await...of

L'instruction for await...of permet de créer une boucle qui parcourt les
objets itérables asynchrones de la même façon qu'on parcourt les itérables
synchrones (tels que les chaînes de caractères (String), les tableaux Array,
les objets semblables aux tableaux comme arguments ou NodeList), TypedArray,
Map, Set.

Cette instruction invoque un mécanisme d'itération spécifique et les
instructions à exécuter pour chaque propriété de l'objet
