.. index::
   pair: Javascript; for of
   ! for of

.. _javascript_for_of:

==============================================================================
for of
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of


.. contents::
   :depth: 3

Definition
===========

The for...of statement creates a loop iterating over iterable objects,
including: built-in String, Array, array-like objects (e.g., arguments or NodeList),
TypedArray, Map, Set, and user-defined iterables.

It invokes a custom iteration hook with statements to be executed for
the value of each distinct property of the object.


::

    const array1 = ['a', 'b', 'c'];

    for (const element of array1) {
      console.log(element);
    }

    // expected output: "a"
    // expected output: "b"
    // expected output: "c"


::

    let f = document.getElementById('id_fiches_temps_0-0-0-commentaire_iframe');
    let elems = f.contentDocument.getElementsByClassName('note-btn btn btn-default btn-sm');
    for (let element of elems) {
      console.log(element);
      for (let attribute of element.attributes) {
          console.log(attribute);
      }
    }
