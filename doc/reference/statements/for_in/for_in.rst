.. index::
   pair: Javascript; for in
   ! for in

.. _javascript_for_in:

==============================================================================
for in
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in


.. contents::
   :depth: 3

Definition
===========

The for...in statement iterates over all enumerable properties of an
object that are keyed by strings (ignoring ones keyed by Symbols),
including inherited enumerable properties.

Exemple

.. code-block:: javascript

    const object = { a: 1, b: 2, c: 3 };

    for (const property in object) {
      console.log(`${property}: ${object[property]}`);
    }

    // expected output:
    // "a: 1"
    // "b: 2"
    // "c: 3"
