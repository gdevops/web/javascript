
.. _info_functions:

==============================================================================
**Javascript.info functions**
==============================================================================

.. seealso::

   - https://javascript.info/function-basics

.. contents::
   :depth: 3

Description
=============

Quite often we need to perform a similar action in many places of the script.

For example, we need to show a nice-looking message when a visitor logs
in, logs out and maybe somewhere else.

Functions are the main “building blocks” of the program.
They allow the code to be called many times without repetition.

We’ve already seen examples of **built-in functions**, like:

- alert(message),
- prompt(message, default)
- and confirm(question).

But we can create functions of our own as well.

Parameters
===========

We can pass arbitrary data to functions using parameters (also called
function arguments) .

In the example below, the function has two parameters: from and text.

.. code-block:: javascript

    function showMessage(from, text) { // arguments: from, text
      alert(from + ': ' + text);
    }

::

    showMessage('Ann', 'Hello!'); // Ann: Hello!

::

    showMessage('Ann', "What's up?"); // Ann: What's up? (**)


Default values
===================

If a parameter is not provided, then its value becomes undefined.

For instance, the aforementioned function showMessage(from, text) can
be called with a single argument::

    showMessage("Ann");

That’s not an error. Such a call would output "Ann: undefined". There’s
no text, so it’s assumed that text === undefined.

If we want to use a “default” text in this case, then we can specify it after =:

.. code-block:: javascript

    function showMessage(from, text = "no text given") {
      alert( from + ": " + text );
    }

::

    showMessage("Ann"); // Ann: no text given


Alternative default parameters
==================================

Sometimes it makes sense to set default values for parameters not in the
function declaration, but at a later stage, during its execution.

To check for an omitted parameter, we can compare it with undefined:

.. code-block:: javascript

    function showMessage(text) {
      if (text === undefined) {
        text = 'empty message';
      }

      alert(text);
    }

::

    showMessage(); // 'empty message'
