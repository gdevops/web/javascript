.. index::
   pair: Javascript; functions


.. _javascript_functions:

==============================================================================
Javascript **functions**
==============================================================================

.. toctree::
   :maxdepth: 3

   javascript.info/javascript.info
   mozilla/mozilla
