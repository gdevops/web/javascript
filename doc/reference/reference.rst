
.. _javascript_reference:

==============================================================================
Javascript reference
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference
   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide
   - https://developer.mozilla.org/fr/docs/Web
   - https://javascript.info/

.. toctree::
   :maxdepth: 3

   arrow_functions/arrow_functions
   functions/functions
   instructions/instructions
   iife/iife
   promise/promise
   global_objects/global_objects
   modules/modules
   statements/statements
