
.. _mozilla_arrow_functions:

==============================================================================
Javascript mozilla **arrow functions**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions
   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/Fonctions_fl%C3%A9ch%C3%A9es
   - https://hacks.mozilla.org/2015/06/es6-in-depth-arrow-functions/

.. contents::
   :depth: 3

English Description
====================

An **arrow function expression** is a syntactically compact alternative
to a regular function expression, although without its own bindings to
the this, arguments, super, or new.target keywords.

Arrow function expressions are ill suited as methods, and they cannot
be used as constructors.

.. code-block:: javascript

    const materials = [
      'Hydrogen',
      'Helium',
      'Lithium',
      'Beryllium'
    ];

    console.log(materials.map(material => material.length));
    // expected output: Array [8, 6, 7, 9]


Description en français
========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/Fonctions_fl%C3%A9ch%C3%A9es
   - https://tech.mozfr.org/post/2015/06/10/ES6-en-details-%3A-les-fonctions-flechees

Une **expression de fonction fléchée** (arrow function en anglais) permet
d’avoir une syntaxe plus courte que les expressions de fonction et ne
possède pas ses propres valeurs pour this, arguments, super, ou new.target.

Les fonctions fléchées sont souvent anonymes et ne sont pas destinées à
être utilisées pour déclarer des méthodes.
