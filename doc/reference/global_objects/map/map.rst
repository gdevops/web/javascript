.. index::
   ! map

.. _javascript_map:

==============================================================================
Javascript **map**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map


.. contents::
   :depth: 3

Définition en français
=======================


L'objet Map représente un dictionnaire, autrement dit une carte de clés/valeurs.

N'importe quelle valeur valable en JavaScript (que ce soit les objets ou
les valeurs de types primitifs) peut être utilisée comme clé ou comme valeur.

L'ordre d'insertion des clés est mémorisé dans l'objet et les boucles
sur les Map parcourent les clés dans cet ordre.


https://flaviocopes.com
==========================

.. seealso::

   - https://flaviocopes.com/javascript-data-structures-map/#what-is-a-map

Before ES6
------------

ECMAScript 6 (also called ES2015) introduced the Map data structure to
the JavaScript world, along with Set

Before its introduction, people generally used objects as maps, by
associating some object or value to a specific key value:

.. code-block:: javascript
   :linenos:

    const car = {}
    car['color'] = 'red'
    car.owner = 'Flavio'
    console.log(car['color']) //red
    console.log(car.color) //red
    console.log(car.owner) //Flavio
    console.log(car['owner']) //Flavio


Enter Map
------------

ES6 introduced the Map data structure, providing us a proper tool to
handle this kind of data organization.

A Map is initialized by calling:


.. code-block:: javascript
   :linenos:

    const m = new Map()

Add items to a Map

You can add items to the map by using the set method:

.. code-block:: javascript
   :linenos:

    m.set('color', 'red')
    m.set('age', 2)


Initialize a map with values
--------------------------------

You can initialize a map with a set of values:

.. code-block:: javascript
   :linenos:

    const m = new Map([['color', 'red'], ['owner', 'Flavio'], ['age', 2]])


https://javascript.info/map-set
==================================

.. seealso::

   - https://javascript.info/map-set

Map is a collection of keyed data items, just like an Object.

But the main difference is that Map allows keys of any type.

Methods and properties are:

- new Map() – creates the map.
- map.set(key, value) – stores the value by the key.
- map.get(key) – returns the value by the key, undefined if key doesn’t exist in map.
- map.has(key) – returns true if the key exists, false otherwise.
- map.delete(key) – removes the value by the key.
- map.clear() – removes everything from the map.
- map.size – returns the current element count.


Example
-----------

.. code-block:: javascript
   :linenos:

    let recipeMap = new Map([
      ['cucumber', 500],
      ['tomatoes', 350],
      ['onion',    50]
    ]);

    // iterate over keys (vegetables)
    for (let vegetable of recipeMap.keys()) {
      alert(vegetable); // cucumber, tomatoes, onion
    }

    // iterate over values (amounts)
    for (let amount of recipeMap.values()) {
      alert(amount); // 500, 350, 50
    }

    // iterate over [key, value] entries
    for (let entry of recipeMap) { // the same as of recipeMap.entries()
      alert(entry); // cucumber,500 (and so on)
    }


The insertion order is used
--------------------------------

.. note:: The iteration goes in the same order as the values were inserted.
   Map preserves this order, unlike a regular Object.


Object.fromEntries: Object from Map
---------------------------------------

We’ve just seen how to create Map from a plain object with Object.entries(obj).

There’s Object.fromEntries method that does the reverse: given an array
of [key, value] pairs, it creates an object from them:

.. code-block:: javascript
   :linenos:

    let prices = Object.fromEntries([
      ['banana', 1],
      ['orange', 2],
      ['meat', 4]
    ]);

    // now prices = { banana: 1, orange: 2, meat: 4 }

    alert(prices.orange); // 2
