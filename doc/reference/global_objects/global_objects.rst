.. index::
   ! Global Objects

.. _javascript_global_objects:

==============================================================================
Javascript **Global objects**
==============================================================================

.. toctree::
   :maxdepth: 3

   object/object
   array/array
   map/map
   null/null
