.. index::
     ! null

.. _javascript_null:

==============================================================================
Javascript **null**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/null


.. contents::
   :depth: 3


Definition
=============

The value null represents the intentional absence of any object value.

It is one of JavaScript's primitive values and is treated as falsy for
boolean operations.


What is the difference between null and undefined in JavaScript ?
===================================================================

.. seealso::

   - https://flaviocopes.com/javascript-difference-null-undefined/


null and undefined are JavaScript primitive types.

The meaning of undefined is to say that a variable has declared, but it
has no value assigned.

::

    let age //age is undefined

::


    let age = null // age === null

Example
==========

.. code-block:: javascript

    function getVowels(str) {
      const m = str.match(/[aeiou]/gi);
      if (m === null) {
        return 0;
      }
      return m.length;
    }

    console.log(getVowels('sky'));
