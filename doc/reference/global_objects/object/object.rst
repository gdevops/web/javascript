.. index::
   ! object

.. _javascript_object:

==============================================================================
Javascript **object**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer


.. contents::
   :depth: 3

Définition en français
=======================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Object
   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/Initialisateur_objet

Le constructeur Object crée un wrapper d'objet.

Le constructeur Object crée un wrapper d'objet pour la valeur donnée.

Si la valeur est null ou undefined, il créera et retournera un objet
vide, sinon, il retournera un objet du Type qui correspond à la valeur
donnée.

Si la valeur est déjà un objet, le constructeur retournera cette valeur.

Lorsqu'il n'est pas appelé dans un contexte constructeur, Object se
comporte de façon identique à new Object().

Voir aussi `initialisateur d'objet / syntaxe de littéral`_.


.. _`initialisateur d'objet / syntaxe de littéral`:  https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/Initialisateur_objet


What is object destructuring in JavaScript ?
=============================================

.. seealso::

   - https://flaviocopes.com/javascript-object-destructuring/


Say you have an object with some properties:

.. code-block:: javascript

    const person = {
      firstName: 'Tom',
      lastName: 'Cruise',
      actor: true,
      age: 57
    }

You can extract just some of the object properties and put them into
named variables::

    const { firstName, age } = person

You can also automatically assign a property to a variable with another name:

::

    const { firstName: name, age } = person

Now instead of a variable named firstName, like we had in the previous
example, we have a name variable that holds the person.firstName value::

    console.log(name) // 'Tom'
