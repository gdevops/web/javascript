.. index::
     ! array

.. _javascript_array:

==============================================================================
Javascript **array**
==============================================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array


.. contents::
   :depth: 3


Definition
=============

The JavaScript Array class is a global object that is used in the
construction of arrays; which are high-level, list-like objects.


Arrays are list-like objects whose prototype has methods to perform
traversal and mutation operations.

Neither the length of a JavaScript array nor the types of its elements
are fixed.

Since an array's length can change at any time, and data can be stored
at non-contiguous locations in the array, JavaScript arrays are not
guaranteed to be dense; this depends on how the programmer chooses to
use them.

In general, these are convenient characteristics; but if these features
are not desirable for your particular use, you might consider using
typed arrays.

Arrays cannot use strings as element indexes (as in an associative array)
but must use integers.
Setting or accessing via non-integers using bracket notation (or dot notation)
will not set or retrieve an element from the array list itself, but will
set or access a variable associated with that array's object property
collection.

The array's object properties and list of array elements are separate,
and the array's traversal and mutation operations cannot be applied to
these named properties.



Définition en français
========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array

L'objet global Array est utilisé pour créer des tableaux.

Les tableaux sont des objets de haut-niveau (en termes de complexité homme-machine)
semblables à des listes.


Les tableaux sont des objets qui servent de liste et possèdent plusieurs
méthodes incorporées pour exécuter des opérations de parcours et de modification.

Ni la taille d'un tableau ni le types de ses éléments n'est fixé.

Puisque la dimension d'un tableau peut augmenter ou diminuer à tout moment,
et que les éléments du tableau peuvent être stockés à des emplacements
non contigus, les tableaux ne sont pas garantis d'être compacts.

En général, ce sont des caractéristiques pratiques, mais si ces fonctionnalités
ne sont pas souhaitables pour votre cas d'utilisation, vous pouvez envisager
d'utiliser des tableaux typés.

Les tableaux ne peuvent pas utiliser de chaîne de caractères comme indice
(comme dans un tableau associatif) mais des entiers.

Utiliser ou accéder à des index non entiers, en utilisant la notation
avec crochets (ou avec point) ne va pas définir ou récupérer un élément
sur le tableau lui-même, mais une variable associée à la  collection
de propriétés d'objet de ce tableau.

Les propriétés du tableau et la liste de ses éléments sont séparées, et
les opérations de parcours et de modification ne s'appliquent pas à ces
propriétés.



Examples
=========

::

    let data=[];
    data.push(1)
    data

::

    Array [ 1 ]

::

    data.forEach(function(item, index, array) { console.log(item);});

An introduction to JavaScript Arrays
=======================================

.. seealso::

   - https://flaviocopes.com/javascript-array/


An array is a collection of elements.
Arrays in JavaScript are not a type on their own.
Arrays are objects.

We can initialize an empty array in these 2 different ways::

    const a = []
    const a = Array()

- The first is using the array literal syntax.
- The second uses the Array built-in function.

You can pre-fill the array using this syntax::

    const a = [1, 2, 3]
    const a = Array.of(1, 2, 3)
