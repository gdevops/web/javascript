.. index::
   pair: deno; installation

.. _deno_installation:

=======================
**deno**  installation
=======================

.. seealso::

   - https://github.com/denoland/deno_install


Official documentation
=======================

.. toctree::
   :maxdepth: 3

   README


My GNU/Linux  installation
==============================

::

    curl -fsSL https://deno.land/x/install/install.sh | sh

::

    ######################################################################## 100,0%#=#=-#  #
    Archive:  /home/pvergain/.deno/bin/deno.zip
      inflating: deno
    Deno was installed successfully to /home/pvergain/.deno/bin/deno
    Manually add the directory to your $HOME/.bash_profile (or similar)
      export DENO_INSTALL="/home/pvergain/.deno"
      export PATH="$DENO_INSTALL/bin:$PATH"
    Run '/home/pvergain/.deno/bin/deno --help' to get started


::

    $ source ~/.bashrc


::

    $ which deno

::

    /home/pvergain/.deno/bin/deno
