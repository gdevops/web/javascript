
.. _deno_devs:

========================================================
**deno** developers
========================================================

.. seealso::

   - https://github.com/denoland/deno/graphs/contributors
   - https://docs.google.com/presentation/d/1wuwliPiKfg9UyHKhQGov9QQQhgllC5tnpxSWzgV8mTU/edit#slide=id.g6497c65a23_0_45


.. contents::
   :depth: 3


**Ryan Dahl**
==============

.. seealso::

   - :ref:`ryan_dahl`


Bartek Iwańczuk
==================

.. seealso::

   - https://github.com/bartlomieju
   - https://x.com/biwanczuk
   - https://deno.news/


Contributing to @denoland, author of deno-postgres, interested in
TypeScript, Rust and Python.


Bert Belder
=============

.. seealso::

   - https://github.com/piscisaureus


Kitson Kelly
==============


.. seealso::

   - https://github.com/kitsonk

Principal Technologist @ThoughtWorksInc, contributing to @denoland and
author of @oakserver


Acknowledgements
==================

.. seealso::

   - https://deno.land/v1
   - https://github.com/denoland/deno/graphs/contributors


Many thanks to the `many contributors`_ who helped make this release possible.

Especially: @kitsonk who has had a massive hand in many parts of the
system, including (but not limited to) the TypeScript compiler host,
deno_typescript, deno bundle, deno install, deno types, streams implementation.

@kevinkassimo has contributed countless bug fixes over the whole history
of the project.
Among his contributions are the timer system, TTY integration, wasm support.
Deno.makeTempFile, Deno.kill, Deno.hostname, Deno.realPath, std/node's require,
window.queueMircotask, and REPL history. He also created the logo.

@kt3k implemented the continuous benchmark system (which has been instrumental
in almost every major refactor), signal handlers, the permissions API,
and many critical bug fixes.

@nayeemrmn contributes bug fixes in many parts of Deno, most notably he
greatly improved the stack trace and error reporting, and has been a
forceful help towards the stabilizing the APIs for 1.0.

@justjavac has contributed many small but critical fixes to align deno
APIs with web standards and most famously he wrote the VS Code deno
plugin.

@zekth has contributed a lot of modules to std, among them the
std/encoding/csv, std/encoding/toml, std/http/cookies, as well as
many other bug fixes.

@axetroy has helped with all things related to prettier, contributed
many bug fixes, and has maintained the VS Code plugin.

@afinch7 implemented the plugin system.

@keroxp implemented the websocket server and provided many bug fixes.

@cknight has provided a lot of documentation and std/node polyfills.

@lucacasonato built almost the entire deno.land website.

@hashrock has done a lot of amazing artwork, like the loading page on
doc.deno.land and the lovely image at the top of this page !


.. _`many contributors`:  https://github.com/denoland/deno/graphs/contributors
