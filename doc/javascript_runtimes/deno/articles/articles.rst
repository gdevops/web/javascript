
.. _deno_articles:

========================================================
**deno** articles
========================================================

.. contents::
   :depth: 3


2020
=====


Deno for JavaScript Beginners
--------------------------------

.. seealso::

   - https://dev.to/maximousblk/deno-for-javascript-beginners-4dnf


Write your first API with Deno
---------------------------------

.. seealso::

   - https://dev.to/dsckiitdev/write-your-first-api-with-deno-1llh

We're going to be building a really simple REST API which lets us
perform CRUD operations on a database of dogs!

Make sure you've installed deno correctly.

We're going to be using the Abc deno web framework along with MongoDB.
We'll also be using Denv to manage our environment variables.

Keep in mind, that there are a ton of other web frameworks like alosaur,
oak, deno-express, pogo, servest that we can use but since deno is
pretty new and I don't really have much of a preference yet,
I'm using this one.



Your First Deno Server in 60 Lines
------------------------------------

.. seealso::

   - https://dev.to/nas5w/your-first-deno-server-in-60-lines-20kk

Today we're going to write our first Deno server in 60 lines.

Deno is self-described as a simple, modern and secure runtime for
JavaScript and TypeScript that uses V8 and is built in Rust."

I'm a huge fan of typescript, so I was really excited to hear about a
runtime that treated typescript as a first-class citizen !

From Node to Deno
------------------

.. seealso::

   - https://dev.to/aralroca/from-node-to-deno-5gpn
   - https://aralroca.com/blog/from-node-to-deno

Last week I published an article about Deno, and how to create a
Chat app with Deno and Preact. Since then, many doubts have arisen.

Mostly of them are about how to do the same thing we did in Node,
but with the new Deno ecosystem.

I've tried to collect some of the most used topics in Node, and looked
for their alternative with Deno.
First of all, I would like to make it clear that we can use many of
the current Node.js modules. There is no need to look for an alternative
for everything, as many modules are reusable.

You can visit pika.dev to look for modules to use in Deno.



🦕🦀Writing WebAssembly in Rust and running it in Deno!
------------------------------------------------------------

.. seealso::

   - https://dev.to/lampewebdev/writing-webassembly-in-rust-and-runing-it-in-deno-144j

Deno V1 : une future alternative à NodeJS ?
----------------------------------------------

.. seealso::

   - https://www.jesuisundev.com/deno-v1-une-future-alternative-nodejs/
   - https://x.com/poledesfetes
   - https://www.jesuisundev.com/pourquoi-developpeur-est-un-boulot-a-part/


Deno V1 est sorti officiellement le 13 mai 2020 et a provoqué un gros
drama dans la communauté Javascript. Ça ressemble énormément à NodeJS.

C’est normal, le créateur de Deno est le même que celui de NodeJS.

Mais c’est quoi Deno ? Et pourquoi ça pourrait devenir une alternative à NodeJS ?


What Is Deno and Why Is Everyone Talking About It ?
--------------------------------------------------------

.. seealso::

   - https://dev.to/pluralsight/what-is-deno-and-why-is-everyone-talking-about-it-336d


Deno is a hot new runtime that may replace Node.js. Everyone's talking
about it like it's the next big thing. It likely is. Here's why.

Docker+Deno: Containerize a Deno hello world server
------------------------------------------------------

.. seealso::

   - https://dev.to/francescoxx/docker-deno-containerize-a-deno-hello-world-server-1ha1

Honestly, I am very fond of node, but I was intrigued, so I created a
hello world server with deno, and immediately I tried to containerize
it with docker.

Deno - why all the buzz ?
---------------------------

.. seealso::

   - https://dev.to/areknawo/deno-why-all-the-buzz-32m8


Deno passe en version 1.0. Le runtime pour exécuter JavaScript et TypeScript
------------------------------------------------------------------------------

.. seealso::

   - https://web.developpez.com/actu/303143/Deno-passe-en-version-1-0-Le-runtime-pour-executer-JavaScript-et-TypeScript-tente-de-fournir-un-outil-autonome-pour-l-ecriture-rapide-de-fonctionnalites-complexes/


Deno 1.0 2020-05-13 Ryan Dahl, Bert Belder, and Bartek Iwańczuk
-----------------------------------------------------------------


.. seealso::

   - https://deno.land/v1


Converting a Node project to Deno
-----------------------------------

.. seealso::

   - https://dev.to/jotafeldmann/converting-a-node-project-to-deno-9dp
   - https://github.com/jotafeldmann/elevators/pull/1



Deno: Node.js Killer? Introduction & Demo | Ryan Dahl
----------------------------------------------------------

.. seealso::

   - https://dev.to/codestackr/deno-node-js-killer-introduction-demo-ryan-dahl-3cdl

What is Deno ?
++++++++++++++++

First off, it's pronounced De'no. Like Deno the dinosaur 🦕.


Deno the Node replacement? Bonus: I created a boilerplate for Deno 🦕
------------------------------------------------------------------------

.. seealso::

   - https://dev.to/lampewebdev/deno-the-node-replacement-bonus-i-created-a-boilerplate-for-deno-2g25


The Deno Handbook: a concise introduction to the wonderful jurassic world of Deno 🦕 (May 12, 2020)
------------------------------------------------------------------------------------------------------

.. seealso::

   - https://flaviocopes.com/deno/

I explore new projects every week, and it’s rare that one hits my attention
so much as Deno did.


Interview with Ryan Dahl Deno 1.0 – “Deno is a web browser for command-line scripts”
---------------------------------------------------------------------------------------

.. seealso::

   - https://jaxenter.com/deno-interview-ryan-dahl-171425.html

Deno 1.0 is scheduled to arrive on May 13. For this occasion, we spoke
to Deno creator Ryan Dahl about how Deno differs from Node, how it is
compatible with the JavaScript ecosystem and where the journey is
headed beyond v1. Can you use lodash with Deno ?

:JAXenter:
    Hi Ryan, nearly two years ago you began developing Deno, which
    started out with the idea of evolving into an alternative to Node,
    building upon different concepts.
    Now, with v1 just around the corner, what would you say is the main
    difference between Deno and Node?

:Ryan Dahl:
    Deno uses a radically simple module system where modules and
    URLs are in one-to-one correspondence.

    Furthermore, Deno can execute TypeScript code out of the box.

    These two features have the effect of greatly simplifying the developer
    workflow.
    Deno is stricter with security than Node; by default, scripts are run in
    a secure sandbox without access to the operating system – users can opt
    in conditionally to provide further functionality – this is to correspond
    to the browser security model where websites ask for permission to access
    the camera or location info.

    Generally, Deno acts more like a web browser – it is, in essence, a web
    browser for command-line scripts.

:JAXenter:
    An important question for many developers is how Deno integrates
    with the larger JavaScript ecosystem. Will it be possible to use Node
    modules in Deno?

:Ryan Dahl:
    To achieve this much simplified workflow, Deno had to pursue
    ideas that were incompatible with the existing ecosystem.

    Today it’s unfortunately generally difficult to take a Node module and
    run it on Deno and vice versa.

    That said, these are both command line JavaScript engines built on V8 –
    they are pretty similar – and we expect, ultimately, that these compatibility
    issues will be worked out.

    One of the efforts made in this area is https://deno.land/std/node/
    which exposes a Node-compatible “require” function in Deno.

:JAXenter:
    How about specific modules for Deno – are packages like
    lodash already ported to Deno?

:Ryan Dahl:
    Yep, many modules will work out of the box on Deno, lodash
    is one of them:

    ::

        import snakeCase from "https://deno.land/x/lodash/snakeCase.js";
        console.log(snakeCase("fooBar"));

    We keep an index of third party modules that work on Deno at https://deno.land/x/.


:JAXenter:
    What are your plans for stability and further development after v1 has
    been published: Will there be a guaranteed maintenance time frame for
    the version or maybe a schedule for further major releases ?

:Ryan Dahl:
    v1 is about stabilizing the built-in JS APIs so developers
    can build with the confidence that their code won’t break.
    However, we have a lot of development happening and we will continue
    to release weekly updates.

    We don’t have a timeline for 2.0 yet, or even what that would mean
    feature-wise, but people can expect regular stable releases.

:JAXenter:
    With v1 being almost finished, are there any features already scheduled
    for later releases? If so, what can we expect to see in further Deno releases?

    We’re exploring if TypeScript type-checking can be done in Rust.

:Ryan Dahl:
    After 1.0, more effort is going to be put into the Rust APIs for
    embedding Deno in other programs and for making plugins to extend
    Deno’s features.
    We are working on a built-in linter (deno lint) with which we hope to
    increase code quality.

    We’re exploring if TypeScript type-checking can be done in Rust rather
    than using the JavaScript TSC – this would dramatically improve startup time.






Learn Deno: Chat app
---------------------

.. seealso::

   - https://dev.to/aralroca/learn-deno-chat-app-37f0
   - https://github.com/aralroca/chat-with-deno-and-preact


Explain Deno Like I'm Five
----------------------------

.. seealso::

   - https://dev.to/ben/explain-deno-like-i-m-five-i4m

Getting started with Deno
--------------------------


.. seealso::

   - https://dev.to/wuz/getting-started-with-deno-e1m



2019
======


Deno TSConf 2019
-----------------

.. seealso::

   - https://docs.google.com/presentation/d/1wuwliPiKfg9UyHKhQGov9QQQhgllC5tnpxSWzgV8mTU/edit#slide=id.g6497c65a23_0_45



Ryan Dahl. Deno, a new way to JavaScript. JS Fest 2019 Spring
---------------------------------------------------------------

.. seealso::

   - https://www.youtube.com/watch?v=z6JRlx5NC9E


2018
=====


JSConf Berlin 2018 - Ryan Dahl liste 10 erreurs de conception sur Node.js
----------------------------------------------------------------------------


.. seealso::

   - https://www.developpez.com/actu/208629/JSConf-Berlin-2018-moins-Ryan-Dahl-liste-10-erreurs-de-conception-sur-Node-js-et-devoile-son-prototype-deno/
