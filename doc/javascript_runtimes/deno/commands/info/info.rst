.. index::
   pair deno ; info

.. _deno_info:

===============
deno **info**
===============

::

    $ deno help info

::

    deno-info
    Information about a module or the cache directories.

    Get information about a module:
      deno info https://deno.land/std/http/file_server.ts

    The following information is shown:

    local: Local path of the file.
    type: JavaScript, TypeScript, or JSON.
    compiled: Local path of compiled source code. (TypeScript only.)
    map: Local path of source map. (TypeScript only.)
    deps: Dependency tree of the source file.

    Without any additional arguments, 'deno info' shows:

    DENO_DIR: Directory containing Deno-managed files.
    Remote modules cache: Subdirectory containing downloaded remote modules.
    TypeScript compiler cache: Subdirectory containing TS compiler output.

    USAGE:
        deno info [OPTIONS] [file]

    OPTIONS:
            --cert <FILE>              Load certificate authority from PEM encoded file
        -h, --help                     Prints help information
        -L, --log-level <log-level>    Set log level [possible values: debug, info]
        -q, --quiet                    Suppress diagnostic output
            --unstable                 Enable unstable APIs

    ARGS:
        <file>
