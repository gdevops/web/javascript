.. index::
   pair: deno ; upgrade

.. _deno_upgrade:

=================
deno **upgrade**
=================

.. contents::
   :depth: 3


deno help upgrade
======================

::

    $ deno help upgrade

::

    deno-upgrade
    Upgrade deno executable to the given version.
    Defaults to latest.

    The version is downloaded from
    https://github.com/denoland/deno/releases
    and is used to replace the current executable.

    USAGE:
        deno upgrade [OPTIONS]

    OPTIONS:
            --dry-run                  Perform all checks without replacing old exe
        -f, --force                    Replace current exe even if not out-of-date
        -h, --help                     Prints help information
        -L, --log-level <log-level>    Set log level [possible values: debug, info]
        -q, --quiet                    Suppress diagnostic output
            --version <version>        The version to upgrade to


Example
========


deno upgrade
--------------

::

    $ deno upgrade

::

    Checking for latest version
    Version has been found
    Deno is upgrading to version 1.0.0-rc3
    downloading https://github.com/denoland/deno/releases/download/v1.0.0-rc3/deno-x86_64-unknown-linux-gnu.zip
    downloading https://github-production-release-asset-2e65be.s3.amazonaws.com/133442384/075b3800-944e-11ea-886b-a429fb86671a?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20200512%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200512T143338Z&X-Amz-Expires=300&X-Amz-Signature=4b0974dde7ddde63b20a7d8495ae863d44679d5367e2fd0e41ef6cdf32090677&X-Amz-SignedHeaders=host&actor_id=0&repo_id=133442384&response-content-disposition=attachment%3B%20filename%3Ddeno-x86_64-unknown-linux-gnu.zip&response-content-type=application%2Foctet-stream
    Archive:  /tmp/.tmpeCzQiM/deno.zip
    inflating: deno
    Upgrade done successfully

::

    $ deno

::

    Deno 1.0.0-rc3
    exit using ctrl+d or close()
    >


deno upgrade --version 1.0.0
-------------------------------

::

    deno upgrade --version 1.0.0

::

    Version has been found
    Deno is upgrading to version 1.0.0
    downloading https://github.com/denoland/deno/releases/download/v1.0.0/deno-x86_64-unknown-linux-gnu.zip
    downloading https://github-production-release-asset-2e65be.s3.amazonaws.com/133442384/82fcd800-9567-11ea-8ecb-c6c01343a23e?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20200514%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200514T051842Z&X-Amz-Expires=300&X-Amz-Signature=cd58e347cc2cd099c54d5cb4dcf773ad10eb8c8e1713cd66ef304c923e891ada&X-Amz-SignedHeaders=host&actor_id=0&repo_id=133442384&response-content-disposition=attachment%3B%20filename%3Ddeno-x86_64-unknown-linux-gnu.zip&response-content-type=application%2Foctet-stream
    Archive:  /tmp/.tmpOKRFoy/deno.zip
    inflating: deno
    Upgrade done successfully

::

    $ deno
    Deno 1.0.0
    exit using ctrl+d or close()
    >
