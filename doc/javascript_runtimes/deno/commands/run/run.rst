.. index::
   pair deno ; run

.. _deno_run:

===============
deno **run**
===============

::

    $ deno run --help

::

    deno-run
    Run a program given a filename or url to the module.

    By default all programs are run in sandbox without access to disk, network or
    ability to spawn subprocesses.
      deno run https://deno.land/std/examples/welcome.ts

    Grant all permissions:
      deno run -A https://deno.land/std/http/file_server.ts

    Grant permission to read from disk and listen to network:
      deno run --allow-read --allow-net https://deno.land/std/http/file_server.ts

    Grant permission to read whitelisted files from disk:
      deno run --allow-read=/etc https://deno.land/std/http/file_server.ts

    USAGE:
        deno run [OPTIONS] <SCRIPT_ARG>...

    OPTIONS:
        -A, --allow-all
                Allow all permissions

            --allow-env
                Allow environment access

            --allow-hrtime
                Allow high resolution time measurement

            --allow-net=<allow-net>
                Allow network access

            --allow-plugin
                Allow loading plugins

            --allow-read=<allow-read>
                Allow file system read access

            --allow-run
                Allow running subprocesses

            --allow-write=<allow-write>
                Allow file system write access

            --cached-only
                Require that remote dependencies are already cached

            --cert <FILE>
                Load certificate authority from PEM encoded file

        -c, --config <FILE>
                Load tsconfig.json configuration file

        -h, --help
                Prints help information

            --importmap <FILE>
                UNSTABLE:
                Load import map file
                Docs: https://deno.land/std/manual.md#import-maps
                Specification: https://wicg.github.io/import-maps/
                Examples: https://github.com/WICG/import-maps#the-import-map
            --inspect=<HOST:PORT>
                activate inspector on host:port (default: 127.0.0.1:9229)

            --inspect-brk=<HOST:PORT>
                activate inspector on host:port and break at start of user script

            --lock <FILE>
                Check the specified lock file

            --lock-write
                Write lock file. Use with --lock.

        -L, --log-level <log-level>
                Set log level [possible values: debug, info]

            --no-remote
                Do not resolve remote modules

        -q, --quiet
                Suppress diagnostic output
                By default, subcommands print human-readable diagnostic messages to stderr.
                If the flag is set, restrict these messages to errors.
        -r, --reload=<CACHE_BLACKLIST>
                Reload source code cache (recompile TypeScript)
                --reload
                  Reload everything
                --reload=https://deno.land/std
                  Reload only standard modules
                --reload=https://deno.land/std/fs/utils.ts,https://deno.land/std/fmt/colors.ts
                  Reloads specific modules
            --seed <NUMBER>
                Seed Math.random()

            --unstable
                Enable unstable APIs

            --v8-flags=<v8-flags>
                Set V8 command line options. For help: --v8-flags=--help


    ARGS:
        <SCRIPT_ARG>...
                script args
