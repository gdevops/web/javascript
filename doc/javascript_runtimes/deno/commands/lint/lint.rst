.. index::
   pair deno ; lint

.. _deno_lint:

===============
deno **lint**
===============

::

    $ deno lint --help

::

    deno-lint
    Lint JavaScript/TypeScript source code.
      deno lint --unstable
      deno lint --unstable myfile1.ts myfile2.js

    List available rules:
      deno lint --unstable --rules

    Ignore diagnostics on the next line by preceding it with an ignore comment and
    rule name:
      // deno-lint-ignore no-explicit-any

      // deno-lint-ignore require-await no-empty

    Names of rules to ignore must be specified after ignore comment.

    ESLint ignore comments are also supported:
      // eslint-ignore-next-line @typescrit-eslint/no-explicit-any no-empty

    Ignore linting a file by adding an ignore comment at the top of the file:
      // deno-lint-ignore-file

    USAGE:
        deno lint [OPTIONS] [files]...

    OPTIONS:
        -h, --help
                Prints help information

        -L, --log-level <log-level>
                Set log level [possible values: debug, info]

        -q, --quiet
                Suppress diagnostic output
                By default, subcommands print human-readable diagnostic messages to stderr.
                If the flag is set, restrict these messages to errors.
            --rules
                List available rules

            --unstable
                Enable unstable APIs


    ARGS:
        <files>...
