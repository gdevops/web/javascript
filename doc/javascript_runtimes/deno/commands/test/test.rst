.. index::
   pair deno ; test

.. _deno_test:

===============
deno **test**
===============


::

    $ deno help test

::

    deno-test
    Run tests using Deno's built-in test runner.

    Evaluate the given modules, run all tests declared with 'Deno.test()' and
    report results to standard output:
      deno test src/fetch_test.ts src/signal_test.ts

    Directory arguments are expanded to all contained files matching the glob
    {*_,}test.{js,ts,jsx,tsx}:
      deno test src/

    USAGE:
        deno test [OPTIONS] [files]...

    OPTIONS:
        -A, --allow-all                    Allow all permissions
            --allow-env                    Allow environment access
            --allow-hrtime                 Allow high resolution time measurement
            --allow-net=<allow-net>        Allow network access
            --allow-plugin                 Allow loading plugins
            --allow-read=<allow-read>      Allow file system read access
            --allow-run                    Allow running subprocesses
            --allow-write=<allow-write>    Allow file system write access
            --allow-none                   Don't return error code if no test files are found
            --cached-only                  Require that remote dependencies are already cached
            --cert <FILE>                  Load certificate authority from PEM encoded file
        -c, --config <FILE>                Load tsconfig.json configuration file
            --failfast                     Stop on first error
            --filter <filter>              A pattern to filter the tests to run by
        -h, --help                         Prints help information
            --importmap <FILE>             UNSTABLE: Load import map file
            --inspect=<HOST:PORT>          activate inspector on host:port (default: 127.0.0.1:9229)
            --inspect-brk=<HOST:PORT>      activate inspector on host:port and break at start of user script
            --lock <FILE>                  Check the specified lock file
            --lock-write                   Write lock file. Use with --lock.
        -L, --log-level <log-level>        Set log level [possible values: debug, info]
            --no-remote                    Do not resolve remote modules
        -q, --quiet                        Suppress diagnostic output
        -r, --reload=<CACHE_BLACKLIST>     Reload source code cache (recompile TypeScript)
            --seed <NUMBER>                Seed Math.random()
            --unstable                     Enable unstable APIs
            --v8-flags=<v8-flags>          Set V8 command line options. For help: --v8-flags=--help

    ARGS:
        <files>...    List of file names to run
