.. index::
   pair deno ; doc

.. _deno_doc:

===============
deno **doc**
===============

::

    $ deno doc --help

::

    deno-doc
    Show documentation for a module.

    Output documentation to standard output:
        deno doc ./path/to/module.ts

    Output documentation in JSON format:
        deno doc --json ./path/to/module.ts

    Target a specific symbol:
        deno doc ./path/to/module.ts MyClass.someField

    Show documentation for runtime built-ins:
        deno doc
        deno doc --builtin Deno.Listener

    USAGE:
        deno doc [OPTIONS] [ARGS]

    OPTIONS:
        -h, --help                        Prints help information
            --json                        Output documentation in JSON format.
        -L, --log-level <log-level>       Set log level [possible values: debug, info]
        -q, --quiet                       Suppress diagnostic output
        -r, --reload=<CACHE_BLACKLIST>    Reload source code cache (recompile TypeScript)
            --unstable                    Enable unstable APIs

    ARGS:
        <source_file>
        <filter>         Dot separated path to symbol.
