.. index::
   pair: deno ; commands

.. _deno_commands:

=======================
**deno**  commands
=======================

.. toctree::
   :maxdepth: 3

   help/help
   bundle/bundle
   doc/doc
   fmt/fmt
   info/info
   install/install
   lint/lint
   repl/repl
   run/run
   test/test
   types/types
   upgrade/upgrade
