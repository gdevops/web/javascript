.. index::
   pair deno ; types

.. _deno_types:

===============
deno **types**
===============

::

    $ deno types --help

::

    deno-types
    Print runtime TypeScript declarations.
      deno types > lib.deno.d.ts

    The declaration file could be saved and used for typing information.

    USAGE:
        deno types [OPTIONS]

    OPTIONS:
        -h, --help
                Prints help information

        -L, --log-level <log-level>
                Set log level [possible values: debug, info]

        -q, --quiet
                Suppress diagnostic output
                By default, subcommands print human-readable diagnostic messages to stderr.
                If the flag is set, restrict these messages to errors.
            --unstable
                Enable unstable APIs
