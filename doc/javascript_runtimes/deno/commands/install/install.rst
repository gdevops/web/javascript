.. index::
   pair: deno ; install

.. _deno_install:

=================
deno **install**
=================

::

    $ deno help install

::

    deno-install
    Installs a script as an executable in the installation root's bin directory.
      deno install --allow-net --allow-read https://deno.land/std/http/file_server.ts
      deno install https://deno.land/std/examples/colors.ts

    To change the executable name, use -n/--name:
      deno install --allow-net --allow-read -n serve https://deno.land/std/http/file_server.ts

    The executable name is inferred by default:
      - Attempt to take the file stem of the URL path. The above example would
        become 'file_server'.
      - If the file stem is something generic like 'main', 'mod', 'index' or 'cli',
        and the path has no parent, take the file name of the parent path. Otherwise
        settle with the generic name.

    To change the installation root, use --root:
      deno install --allow-net --allow-read --root /usr/local https://deno.land/std/http/file_server.ts

    The installation root is determined, in order of precedence:
      - --root option
      - DENO_INSTALL_ROOT environment variable
      - $HOME/.deno

    These must be added to the path manually if required.

    USAGE:
        deno install [OPTIONS] <cmd>...

    OPTIONS:
        -A, --allow-all                    Allow all permissions
            --allow-env                    Allow environment access
            --allow-hrtime                 Allow high resolution time measurement
            --allow-net=<allow-net>        Allow network access
            --allow-plugin                 Allow loading plugins
            --allow-read=<allow-read>      Allow file system read access
            --allow-run                    Allow running subprocesses
            --allow-write=<allow-write>    Allow file system write access
            --cert <FILE>                  Load certificate authority from PEM encoded file
        -f, --force                        Forcefully overwrite existing installation
        -h, --help                         Prints help information
        -L, --log-level <log-level>        Set log level [possible values: debug, info]
        -n, --name <name>                  Executable file name
        -q, --quiet                        Suppress diagnostic output
            --root <root>                  Installation root
            --unstable                     Enable unstable APIs

    ARGS:
        <cmd>...
