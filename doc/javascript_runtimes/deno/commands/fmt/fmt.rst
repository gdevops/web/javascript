.. index::
   pair deno ; fmt

.. _deno_fmt:

===============
deno **fmt**
===============

::

    $ deno fmt --help

::


    deno-fmt
    Auto-format JavaScript/TypeScript source code.
      deno fmt
      deno fmt myfile1.ts myfile2.ts
      deno fmt --check

    Format stdin and write to stdout:
      cat file.ts | deno fmt -

    Ignore formatting code by preceding it with an ignore comment:
      // deno-fmt-ignore

    Ignore formatting a file by adding an ignore comment at the top of the file:
      // deno-fmt-ignore-file

    USAGE:
        deno fmt [OPTIONS] [files]...

    OPTIONS:
            --check
                Check if the source files are formatted.

        -h, --help
                Prints help information

        -L, --log-level <log-level>
                Set log level [possible values: debug, info]

        -q, --quiet
                Suppress diagnostic output
                By default, subcommands print human-readable diagnostic messages to stderr.
                If the flag is set, restrict these messages to errors.

    ARGS:
        <files>...
