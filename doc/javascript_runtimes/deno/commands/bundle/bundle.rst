.. index::
   pair: deno ; bundle
   ! bundle

.. _deno_bundle:

==================
deno **bundle**
==================

::

    $ deno help bundle


::

    deno-bundle
    Output a single JavaScript file with all dependencies.
      deno bundle https://deno.land/std/examples/colors.ts colors.bundle.js

    If no output file is given, the output is written to standard output:
      deno bundle https://deno.land/std/examples/colors.ts

    USAGE:
        deno bundle [OPTIONS] <source_file> [out_file]

    OPTIONS:
            --cert <FILE>              Load certificate authority from PEM encoded file
        -c, --config <FILE>            Load tsconfig.json configuration file
        -h, --help                     Prints help information
            --importmap <FILE>         UNSTABLE: Load import map file
        -L, --log-level <log-level>    Set log level [possible values: debug, info]
        -q, --quiet                    Suppress diagnostic output
            --unstable                 Enable unstable APIs

    ARGS:
        <source_file>
        <out_file>
