.. index::
   pair: deno ; repl

.. _deno_repl:

==================
deno **repl**
==================

::

    $ deno help repl

::

    deno-repl
    Read Eval Print Loop

    USAGE:
        deno repl [OPTIONS]

    OPTIONS:
            --cert <FILE>                Load certificate authority from PEM encoded file
        -h, --help                       Prints help information
            --inspect=<HOST:PORT>        activate inspector on host:port (default: 127.0.0.1:9229)
            --inspect-brk=<HOST:PORT>    activate inspector on host:port and break at start of user script
        -L, --log-level <log-level>      Set log level [possible values: debug, info]
        -q, --quiet                      Suppress diagnostic output
            --unstable                   Enable unstable APIs
            --v8-flags=<v8-flags>        Set V8 command line options. For help: --v8-flags=--help
