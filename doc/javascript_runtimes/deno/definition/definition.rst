
.. _deno_def:

=====================
**deno**  definition
=====================

.. seealso::

   - https://github.com/denoland/deno
   - https://x.com/deno_land
   - https://www.youtube.com/watch?v=F0G9lZ7gecE

.. contents::
   :depth: 3



Introduction
==============

.. seealso::

   - https://github.com/denoland/deno/blob/master/docs/introduction.md

.. toctree::
   :maxdepth: 3

   introduction


https://blog.logrocket.com (Deno 1.0: What you need to know)
=============================================================

.. seealso::

   - https://blog.logrocket.com/deno-1-0-what-you-need-to-know/
   - https://tokio.rs/


After almost two years, the wait is nearly over.

The API has been frozen, and the countdown to Deno 1.0, officially
scheduled for release on May 13, has begun.

Due to its famous creator and forward-thinking vision, Deno is sure to
be the most exciting and controversial JavaScript-related release in
recent memory.

Deno is a general-purpose JavaScript/TypeScript programming environment.

It brings together many of the best open-source technologies and offers
a comprehensive solution in one small executable file.

Created by Ryan Dahl, best known as the mastermind behind Node.js, Deno
takes advantage of the JavaScript features that have been available
since Node.js was released back in 2009.

It also addresses the design flaws that Ryan talked about in his
“10 Things I Regret About Node.js” lecture.

Some are calling it the sequel to Node.js, although the author himself
makes no such claim.

Unlike Node.js, which was written in C++, **Deno is written in Rust**.

It builds on top of the Tokio_ platform and, like Node.js, executes
JavaScript using the V8 engine.

**One outstanding feature is that TypeScript is built in**.

While it still needs compiling into JavaScript to run, it’s done internally,
so TypeScript behaves to the user as if it were natively supported.


.. _Tokio: https://tokio.rs/

Deno uses web standards where possible
------------------------------------------

w3 takes a long time to create a web standard, and once it’s set in stone,
it’s unwise to ignore it. While frameworks come and go, web standards
will remain.

Time spent invested in learning a standardized API is never wasted
because nobody dares break the web; it could well be in use for decades,
maybe even the rest of your career.

The fetch web API provides an interface for fetching resources.

There is a JavaScript fetch() method that is available in the browser.

If you want to use this standard in Node.js, you’ll need to reach for
the third party library Node Fetch.

In Deno, it is built in and works just like the browser version, right
out of the box.

ECMAScript modules
-----------------------

One of the main breaking changes in Deno from Node.js is that Deno uses
the official ECMAScript module standard rather than legacy CommonJS.

It took Node.js until the end of 2019 to enable ECMAScript modules with
version 13.2.0, but even then, support was half-baked and it still
included the controversial .mjs file extension.

Deno breaks free of the past by using modern web standards for its
module system.

The module is referenced using a URL or file path and includes a
mandatory file extension. For example::

    import * as log from "https://deno.land/std/log/mod.ts";
    import { outputToConsole } from "./view.ts";


Package management
---------------------

Dog Sitting in Front of Cardboard BoxesThere has been a radical rethink
regarding the way package management works in Deno. Rather than relying
on a central repository, it is decentralized.

Anyone can host a package just like anyone can host any type of file on the web.

There are advantages and disadvantages to using a centralized repository
like npm, and this aspect of Deno is sure to be the most controversial.


Why Deno ? Why now ?
======================

.. seealso::

   - https://flaviocopes.com/deno/

Deno was announced almost 2 years ago by the Node.js original creator
Ryan Dahl at JSConf EU. Watch the YouTube video of the talk, it’s very
interesting and it’s a mandatory watch if you are involved in Node.js
and JavaScript in general.

Every project manager must take decisions. Ryan regretted some early
decisions in Node.

Also, technology evolves, and today JavaScript is a totally different
language than what it was back in 2009 when Node started.
Think about the modern ES6/2016/2017 features, and so on.

So he started a new project to create some sort of second wave of
JavaScript-powered server side apps.

The reason I am writing this guide now and not back then is because
technologies need a lot of time to mature.
And we have finally reached Deno 1.0 (1.0 should be released on May 13, 2020),
the first release of Deno officially declared stable.

That’s might seem just a number, but 1.0 means there will not be major
breaking changes until Deno 2.0, which is a big deal when you dive into
a new technology - you don’t want to learn something and then have it
change too fast.
