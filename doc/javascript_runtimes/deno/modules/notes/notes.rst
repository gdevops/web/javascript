.. index::
   pair: deno; modules

.. _deno_modules_notes:

========================================================
**deno** modules notes + https://www.pika.dev
========================================================

.. seealso::

   - https://www.pika.dev


.. figure:: logo_pika_dev.svg
   :align: center
   :width: 150

   https://www.pika.dev


.. contents::
   :depth: 3


https://aralroca.com (https://www.pika.dev)
================================================


.. seealso::

   - :ref:`deno_tut_arial_roca`
   - https://aralroca.com/blog/learn-deno-chat-app
   - https://www.pika.dev


We can use third-party libraries in the same way we use the Deno Standard Library,
by importing directly the URL of the module.

- STD, Deno core libraries: https://deno.land/std/
- X, Deno Third-party libraries: https://deno.land/x/

However, the ecosystem in https://deno.land/x/ is quite small yet.

But hey, I have good news for you, we can use packages from https://www.pika.dev.

**Thanks to tools like Parcel or Minibundle we can compile Node libraries
into modules to re-use them in Deno projects**.

We are going to use the camel-case package to transform every chat
message to camelCase !
