
.. _deno_x_observable:

========================================================
**deno** x **observable** module
========================================================

.. seealso::

   - https://deno.land/x/observable/
   - https://github.com/bsunderhus/deno-observable/tree/master/
   - :ref:`esnext_observable`
