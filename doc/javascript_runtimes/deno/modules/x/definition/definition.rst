
.. _deno_modules_x_def:

========================================================
**deno** x modules definition
========================================================

.. seealso::

   - https://deno.land/x/
   - https://github.com/denoland/deno_website2/blob/master/src/database.json
   - https://doc.deno.land/https/github.com/denoland/deno/releases/latest/download/lib.deno.d.ts
   - https://deno.land/manual

.. contents::
   :depth: 3

Third Party Modules
====================

This is a code hosting service for Deno scripts.

The basic format of code URLs is https://deno.land/x/MODULE_NAME@BRANCH/SCRIPT.ts.

If you leave out the branch, it will default to master.

Functionality built-in to Deno is not listed here.

The built-in runtime is documented on deno doc and in the manual.

See /std for the standard modules.

To add to this list, edit `database.json`_.

.. _`database.json`:  https://github.com/denoland/deno_website2/blob/master/src/database.json
