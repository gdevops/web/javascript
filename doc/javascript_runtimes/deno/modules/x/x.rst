.. index::
   pair: deno; x modules

.. _deno_modules_x:

========================================================
**deno** x modules
========================================================

.. seealso::

   - https://deno.land/x/
   - https://github.com/denoland/deno_website2/blob/master/src/database.json
   - https://doc.deno.land/https/github.com/denoland/deno/releases/latest/download/lib.deno.d.ts
   - https://deno.land/manual

.. toctree::
   :maxdepth: 3

   definition/definition
   lodash/lodash
   observable/observable
