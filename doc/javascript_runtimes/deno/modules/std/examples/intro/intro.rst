
.. _deno_modules_std_examples_into:

========================================================
**deno** std modules examples introduction
========================================================

.. seealso::

   - https://deno.land/std/examples/


Deno code examples
=====================

.. seealso::

   - https://flaviocopes.com/deno/

In addition to the one we ran above, the Deno website provides some
other examples you can check out: https://deno.land/std/examples/.

At the time of writing we can find:

- cat.ts prints the content a list of files provided as arguments
- catj.ts print flattened JSON to standard output
- chat/ an implementation of a chat
- colors.ts an example of
- curl.ts a simple implementation of curl that prints the content of
  the URL specified as argument
- echo_server.ts a TCP echo server
- gist.ts a program to post files to gist.github.com
- test.ts a sample test suite
- welcome.ts a simple console.log statement (the first program we ran above)
- xeval.ts allows you to run any TypeScript code for any line of standard
  input received. Once known as deno xeval but since removed from the official command.
