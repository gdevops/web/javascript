.. index::
   pair: deno; std modules

.. _deno_modules_std:

========================================================
**deno** std modules
========================================================

.. seealso::

   - https://deno.land/std/
   - https://golang.org/pkg/

.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
   node/node
