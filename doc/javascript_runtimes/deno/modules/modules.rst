.. index::
   pair: deno; modules

.. _deno_modules:

========================================================
**deno** modules
========================================================

.. seealso::

   - https://github.com/denolib
   - https://github.com/denolib/awesome-deno
   - https://x.com/denonews

.. toctree::
   :maxdepth: 3

   notes/notes
   std/std
   x/x
