.. index::
   pair: Javascript ; deno
   ! deno

.. _deno:

===============================================================
🦕 **deno** A secure JavaScript and TypeScript runtime 🦕
===============================================================

.. seealso::

   - https://github.com/denoland/deno
   - https://github.com/denoland/deno/issues
   - https://deno.land/
   - https://deno.news/
   - https://doc.deno.land/
   - https://x.com/deno_land
   - https://x.com/denonews
   - https://x.com/deno_community
   - https://x.com/ParisDeno
   - https://x.com/denopoland
   - https://x.com/kitsonk
   - https://deno.land/std/manual.md
   - https://en.wikipedia.org/wiki/Deno_(software)
   - https://github.com/denolib/awesome-deno
   - https://github.com/denoland/rusty_v8
   - https://github.com/denoland/deno_website2


.. figure:: deno_logo_3.svg
   :align: center
   :width: 200

.. toctree::
   :maxdepth: 3

   definition/definition
   installation/installation
   commands/commands
   articles/articles
   modules/modules
   tutorials/tutorials
   developers/developers
   versions/versions
