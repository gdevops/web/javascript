.. index::
   pair: deno; handbook

.. _deno_boilerplate:

===========================================================================
**Deno** the Node replacement ? Bonus: I created a boilerplate for Deno 🦕
===========================================================================

.. seealso::

   - https://dev.to/lampewebdev/deno-the-node-replacement-bonus-i-created-a-boilerplate-for-deno-2g25
   - http://x.com/lampewebdev
   - https://github.com/lampewebdev/deno-boilerplate


.. contents::
   :depth: 3

First things first
===================

Deno is right now not production-ready.
As of writing this post, it is in version 1.0.0-rc2. Does this mean we
should ignore it? No, we should start to learn it right now because it
will maybe take 1 or 2 years but if nothing tragic will happen then it
will replace Node or at least will be as popular as Node.

I'm not bashing Node here. I love Node and I think it helped so many
people but at times it can also be slow to evolve.

Like adding module support and adopting ECMAScript(javascript) standards.

Also, **NPM can be a mess** but to be fair dependency management, in general,
is not easy.

Also tooling around node can be hard to setup.
Like webpack or eslint/prettier.

I'm always using some kind of boilerplate that does not work anymore
because of so many updates. Okay, enough about Node/Npm.


Creating our boilerplate
==========================

.. seealso::

   - https://marketplace.visualstudio.com/items?itemName=axetroy.vscode-deno

First step: :ref:`install deno <deno_install>`

Now you should have the deno command at your fingertips.

In general, I would advise you to use a Unix-like OS. If you are using
Windows then you can use WSL.

Also, you should have git installed and make.

You can clone the boilerplate code like this::

    git clone https://github.com/lampewebdev/deno-boilerplate


For this tutorial, I will use VS code and you should download the
`Deno VS Code Extension`_

.. _`Deno VS Code Extension`: https://marketplace.visualstudio.com/items?itemName=axetroy.vscode-deno


makefile
----------
The next file we should have a look at is the makefile.


.. literalinclude:: makefile
   :language: makefile
   :linenos:


You can see the following commands in the makefile::

    make run: executes the index.ts
    make test: runs the tests
    make format: formats all your files
    make debug: runs the debugger starting in the index.ts
    make bundle: bundles your project into a single file in build/index

For all these commands we don't need any extra tools or dependencies.

**It's all built-in Deno. I find this great and it makes your life as a
developer so much easier**.

On thing I wish was in Deno by default would be a --watch flag that
reruns your code once a file has changed.


deps.ts
---------

Another interesting file is the **deps.ts** file. This is just a normal
Typescript file but by convention, this file is where you organize
your external modules/packages you are using.

.. literalinclude:: deps.ts
   :linenos:

Think about it like your package.json.
It is a central place where you store your dependencies.

One thing that will be interesting to see where development dependencies
should go. I have seen people doing dev-deps.ts.
I would prefer deps.<ENV>.ts. So for example deps.prod.ts, deps.dev.ts and so on.

configs.ts
-----------

**The configs.ts for now is empty**. I like dotenv files but we will see
what wins in Deno.

index.ts
-----------

Then we have the **index.ts**. It is our entry point and as a web developer
I like index but I would also have no problem to rename it.

.. literalinclude:: index.ts
   :linenos:

The rest is pretty standard stuff.

src/main.ts
-------------

We have a **main.ts** where we can start to implement things and an example
of how to implement the simplest logger in Deno.

.. literalinclude:: src/main.ts
   :linenos:

What I want to add are tests and documentation. Both are built-in Deno
and you need no extra tools.

If you want to help just contact me or create an issue or pull request
in the GitHub Repo

README
=======

.. toctree::
   :maxdepth: 3

   README
