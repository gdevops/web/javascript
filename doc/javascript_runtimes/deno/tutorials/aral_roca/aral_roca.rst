.. index::
   ! Arial Roca

.. _deno_tut_arial_roca:

=====================================================
**deno**  tutorial by https://x.com/aralroca
=====================================================

.. seealso::

   - https://aralroca.com/blog/learn-deno-chat-app
   - https://x.com/aralroca/status/1259536045407965185?s=20
   - https://github.com/aralroca/chat-with-deno-and-preact
   - https://aralroca.com/blog/app-with-react-api-without-tools-as-webpack-or-babel
   - https://x.com/aralroca
   - https://aralroca.com/
   - https://github.com/aralroca
   - https://github.com/vinissimus/next-translate
   - https://www.pika.dev

.. contents::
   :depth: 3


Introduction
===============

Node.js was writtern initially by Ryan Dahl on 2009 (in C++).

Ryan left Node.js in 2012, as at this point he felt he had more or less
fulfilled his goals.

His goals are now different.

After realizing that there were some design errors impossible to fix in
Node.js, he decided to create another JavaScript (also TypeScript)
runtime built with V8: Deno (in Rust).

Deno 1.0.0 will be finally released on 13th May 2020.


Installing Deno
===================

There are different ways to install Deno: Using curl, iwr, Homebrew,
Chocolatey...

See how to install it :ref:`here <deno_installation>`.

**Deno is a single binary executable, it has no external dependencies**.


VSCodium
==============

In case that you are using VSCodium, I recommend to install
this plugin to ease working with Deno:

- https://marketplace.visualstudio.com/items?itemName=axetroy.vscode-deno

Simple "Hello World"
======================

::

    deno run https://deno.land/std/examples/welcome.ts

::

    Download https://deno.land/std/examples/welcome.ts
    Compile https://deno.land/std/examples/welcome.ts
    Welcome to Deno 🦕


For a simple "Hello world" in Deno, we just need to create a file .js
or .ts, and execute it with deno run [file].

In case of .ts, it will compile + execute, meanwhile for .js, the file
will be executed directly:

.. literalinclude:: example.ts

And in the shell::

::

    $ deno run example.ts

::

    Compile ..javascript_runtimes/deno/tutorials/aral_roca/example.ts
    🦕 Hello from Deno 🖐 🦕


The tsconfig.json file is optional because in Deno there are some
TypeScript defaults.

To apply the tsconfig.json we should use deno run -c tsconfig.json [file].

By the way, **Deno uses web standards where possible**.

It's possible to use window, fetch, Worker...

**Our code should be compatible with both Deno and the browser**.


Serve an index.html
====================

Deno has his own standard library https://deno.land/std/ so to use their
modules we can import it directly from the URL.

One of its goals is shipping only a single executable with minimal linkage.

This way it's only necessary to import the URL to their projects, or
execute directly with deno run https://... in case of CLIs.

In order to create a **http server** and serve an index.html we are
going to use this module: https://deno.land/std/http/.


We are going to create two files: server.ts and index.html.


index.html
-------------

.. literalinclude:: index.html
   :language: html
   :linenos:

server.ts
-----------

.. literalinclude:: server.ts
   :language: typescript
   :linenos:


We can use ESmodules by default instead of Common.js, indicating the
file extension always at the end.

Moreover, it supports the latest features as async-await.

Also, we don't need to worry about formatting anymore. Instead of using
tools as Prettier, we can format the files with deno fmt command.

The first time **deno run server.ts** runs, we'll see two differences with
respect to the "Hello World" example:

- It downloads all the dependencies from http module.
- Instead of using yarn or npm install, it should install all the needed
  dependencies before running the project.
- This happens only the first time, since it's cached.
  To clean the cache you can use the :ref:`--reload command <deno_run>`.
- It throws an error Uncaught PermissionDenied: network access to "127.0.0.1:3000",
  Deno is secure by default.
  This means that we can't access to the net or read a file (index.html).

**This is one of the big improvements over Node**.

In Node any CLI library could do many things without our consent.

With Deno it's possible, for example, to allow reading access only in
one folder::

    deno --allow-read=/etc.

To see all permission flags, run :ref:`deno run -h <deno_run>`


deno run --allow-net --allow-read server.ts
-----------------------------------------------

.. seealso:

   - :ref:`deno_run`


::

    $ deno run --allow-net --allow-read server.ts

::

    Compile file:///home/pvergain/tutos/tuto_javascript/javascript_runtimes/deno/tutorials/aral_roca/server.ts
    Download https://deno.land/std/http/server.ts
    Download https://deno.land/std/encoding/utf8.ts
    Download https://deno.land/std/io/bufio.ts
    Download https://deno.land/std/testing/asserts.ts
    Download https://deno.land/std/async/mod.ts
    Download https://deno.land/std/http/_io.ts
    Download https://deno.land/std/io/util.ts
    Download https://deno.land/std/path/mod.ts
    Download https://deno.land/std/path/win32.ts
    Download https://deno.land/std/path/posix.ts
    Download https://deno.land/std/path/common.ts
    Download https://deno.land/std/path/separator.ts
    Download https://deno.land/std/path/interface.ts
    Download https://deno.land/std/path/glob.ts
    Download https://deno.land/std/path/_constants.ts
    Download https://deno.land/std/path/_util.ts
    Download https://deno.land/std/fmt/colors.ts
    Download https://deno.land/std/testing/diff.ts
    Download https://deno.land/std/path/_globrex.ts
    Download https://deno.land/std/async/deferred.ts
    Download https://deno.land/std/async/delay.ts
    Download https://deno.land/std/async/mux_async_iterator.ts
    Download https://deno.land/std/textproto/mod.ts
    Download https://deno.land/std/http/http_status.ts
    Download https://deno.land/std/bytes/mod.ts

.. figure:: example_server.png
   :align: center


.. _websockets_deno:

Using WebSockets
====================

WebSockets, UUID, and other essentials in Node are not part of the core.

This means that we need to use third-party libraries to use it.

Yet, you can use WebSockets and UUID among many others by using Deno
standard library. In other words, you don't need to worry about maintenance,
because now it will be always maintained.

To continue implementing our simple chat app, let's create a new file
chat.ts with:

.. code-block:: typescript
   :linenos:

    import {
      WebSocket,
      isWebSocketCloseEvent,
    } from "https://deno.land/std/ws/mod.ts";
    import { v4 } from "https://deno.land/std/uuid/mod.ts";

    const users = new Map<string, WebSocket>();

    function broadcast(message: string, senderId?: string): void {
      if(!message) return
      for (const user of users.values()) {
        user.send(senderId ? `[${senderId}]: ${message}` : message);
      }
    }

    export async function chat(ws: WebSocket): Promise<void> {
      const userId = v4.generate();

      // Register user connection
      users.set(userId, ws);
      broadcast(`> User with the id ${userId} is connected`);

      // Wait for new messages
      for await (const event of ws) {
        const message = typeof event === 'string' ? event : ''

        broadcast(message, userId);

        // Unregister user conection
        if (!message && isWebSocketCloseEvent(event)) {
          users.delete(userId);
          broadcast(`> User with the id ${userId} is disconnected`);
          break;
        }
      }
    }


Now, register an endpoint /ws to expose the chat on server.ts

.. code-block:: typescript
   :linenos:

    import { listenAndServe } from "https://deno.land/std/http/server.ts";
    import { acceptWebSocket, acceptable } from "https://deno.land/std/ws/mod.ts";
    import { chat } from "./chat.ts";

    listenAndServe({ port: 3000 }, async (req) => {
      if (req.method === "GET" && req.url === "/") {
        req.respond({
          status: 200,
          headers: new Headers({
            "content-type": "text/html",
          }),
          body: await Deno.open("./index.html"),
        });
      }

      // WebSockets Chat
      if (req.method === "GET" && req.url === "/ws") {
        if (acceptable(req)) {
          acceptWebSocket({
            conn: req.conn,
            bufReader: req.r,
            bufWriter: req.w,
            headers: req.headers,
          }).then(chat);
        }
      }
    });

    console.log("Server running on localhost:3000");


To implement our client-side part, we are going to choose Preact to be
able to use modules directly without the need of npm, babel and webpack,
as we saw on the previous article.

.. code-block:: html
   :linenos:

    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <title>Chat using Deno</title>
      </head>
      <body>
        <div id="app" />
        <script type="module">
          import {
            html,
            render,
            useEffect,
            useState,
          } from 'https://unpkg.com/htm/preact/standalone.module.js'

          let ws

          function Chat() {
            // Messages
            const [messages, setMessages] = useState([])
            const onReceiveMessage = ({ data }) => setMessages((m) => [...m, data])
            const onSendMessage = (e) => {
              const msg = e.target[0].value

              e.preventDefault()
              ws.send(msg)
              e.target[0].value = ''
            }

            // Websocket connection + events
            useEffect(() => {
              if (ws) ws.close()
              ws = new WebSocket(`ws://${window.location.host}/ws`)
              ws.addEventListener('message', onReceiveMessage)

              return () => {
                ws.removeEventListener('message', onReceiveMessage)
              }
            }, [])

            return html`
              ${messages.map((message) => html` <div>${message}</div> `)}

              <form onSubmit=${onSendMessage}>
                <input type="text" />
                <button>Send</button>
              </form>
            `
          }

          render(html`<${Chat} />`, document.getElementById('app'))
        </script>
      </body>
    </html>


Third-party and deps.ts convention
=======================================

.. seealso::

  - https://www.pika.dev

We can use third-party libraries in the same way we use the Deno Standard Library,
by importing directly the URL of the module.

- STD, Deno core libraries: https://deno.land/std/
- X, Deno Third-party libraries: https://deno.land/x/

However, the ecosystem in https://deno.land/x/ is quite small yet.

But hey, I have good news for you, we can use packages from https://www.pika.dev.

**Thanks to tools like Parcel or Minibundle we can compile Node libraries
into modules to re-use them in Deno projects**.

We are going to use the camel-case package to transform every chat
message to camelCase!


Let's add this import in our chat.ts file:

.. code-block:: typescript

    import { camelCase } from 'https://cdn.pika.dev/camel-case@^4.1.1';
    // ...before code
    const message = camelCase(typeof event === 'string' ? event : '')
    // ... before code

That's it. Running again the server.ts is going to download the
camel-case package. Now you can see that it works:

However, if I want to use this camelCase helper in more than one file,
it's cumbersome to add the full import everywhere.

The URL indicates which version of the package we have to use.

This means that if we want to upgrade a dependency we will need to
search and replace all the imports. This could cause us problems, but
don't worry, there is a Deno convention for the dependencies that
solves this.

Creating a deps.ts file to export all project dependencies.

.. code-block:: typescript

    // deps.ts file
    export { camelCase } from 'https://cdn.pika.dev/camel-case@^4.1.1';

and

.. code-block:: typescript

    // chat.ts file
    import { camelCase } from './deps.ts';
    // ...
    const message = camelCase(typeof event === 'string' ? event : '')



Conclusion
============

We learned about how Deno works by creating a simple chat app in
TypeScript.

We did it without npm, package.json, node_modules, webpack, babel, jest,
prettier... because we don't need them, Deno simplifies this.

We explored important things to begin with a Deno project: Permissions,
deno commands, how to use deno internals, how to use third-party dependencies,
serving a file, websockets, formating files, testing, debugging, etc.

I hope this article will be useful to start using Deno 1.0.0 in your
projects when it comes out on May 13, 2020.
