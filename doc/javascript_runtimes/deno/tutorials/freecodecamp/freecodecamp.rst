.. index::
   pair: deno; handbook

.. _deno_handbook:

===========================================================================
The 🦕 Deno Handbook: A TypeScript Runtime Tutorial with Code Examples 🦕
===========================================================================

.. seealso::

   - https://www.freecodecamp.org/news/the-deno-handbook/
   - https://flaviocopes.com/deno/
   - https://github.com/denolib/awesome-deno
   - https://x.com/flaviocopes
   - https://x.com/flaviocopes/status/1260185913952526338?s=20


.. contents::
   :depth: 3

First-class TypeScript support
=================================

Deno is written in Rust and TypeScript, two of the languages that today
are really growing fast.

In particular being written in TypeScript means we get a lot of the
benefits of TypeScript even if we might choose to write our code in
plain JavaScript.

And running TypeScript code with Deno does not require a compilation
step - Deno does that automatically for you.

You are not forced to write in TypeScript, but the fact the core of
Deno is written in TypeScript is huge.

First, an increasingly big percentage of JavaScript programmers
love TypeScript.

Second, the tools you use can infer many information about software
written in TypeScript, like Deno.

This means that while we code in VS Code for example, which obviously
has a tight integration with TypeScript since both are developed at
MicroSoft, we can get benefits like type checking as we write our code,
and advanced IntelliSense features.

In other words the editor can help us in a deeply useful way.

VSCodium
=========

Let’s run this program locally. I assume you use VSCodium, but you can
use any editor you like.

I recommend installing the Deno extension from justjavac
(there was another one with the same name when I tried, but
deprecated - might disappear in the future)


Similarities and differences with Node.js
============================================

Since Deno is basically a Node.js replacement, it’s useful to compare
the two directly.

Similarities:

- Both are developed upon the V8 Chromium Engine
- Both are great for developing server-side with JavaScript

Differences:

- Node is written in C++ and JavaScript. Deno is written in Rust and TypeScript.
- Node has an official package manager called npm. Deno does not, and
  instead lets you import any ES Module from URLs.
- Node uses the CommonJS syntax for importing pacakges. Deno uses
  ES Modules, the official way.
- Deno uses modern ECMAScript features in all its API and standard
  library, while Node.js uses a callbacks-based standard library and
  has no plans to upgrade it.
- Deno offers a sandbox security layer through permissions.
  A program can only access the permissions set to the executable as
  flags by the user. A Node.js program can access anything the user
  can access Deno has a for a long time envisioned the possibility of
  compiling a program into an executable that you can run without
  external dependencies, like Go, but it’s still not a thing yet_.
  That’d be a game changer.

.. _yet: https://github.com/denoland/deno/issues/986

Is there an Express/Hapi/Koa/* for Deno ?
===========================================

.. seealso::

   - https://www.freecodecamp.org/news/the-deno-handbook/#is-there-an-express-hapi-koa-for-deno

Yes, definitely. Check out projects like

- deno-drash_
- deno-express_
- oak_
- pogo_
- servest_


.. _deno-drash: https://github.com/drashland/deno-drash
.. _deno-express: https://github.com/NMathar/deno-express
.. _oak: https://github.com/oakserver/oak
.. _pogo: https://github.com/sholladay/pogo
.. _servest: https://github.com/keroxp/servest


Example: use Oak to build a REST API
=========================================

I want to make a simple example of how to build a REST API using Oak.

Oak is interesting because it’s inspired by Koa, the popular Node.js
middleware, and due to this it’s very familiar if you’ve used that before.

The API we’re going to build is very simple.

Our server will store, in memory, a list of dogs with name and age.

We want to:

- add new dogs
- list dogs
- get details about a specific dog
- remove a dog from the list
- update a dog age
