
.. index::
   pair: 10.0.0 ; Nodejs
   pair: Dubnium ; Nodejs


.. _nodejs_10_0:

====================================
nodejs 10.0 (Dubnium, 2018-03-02)
====================================


.. seealso::

   - https://github.com/nodejs/node/tree/v10.0.0
   - https://github.com/nodejs/Release
   - https://nodesource.com/blog/what-you-can-expect-from-node-js-10
   - https://medium.com/the-node-js-collection/say-hello-to-http-2-for-node-js-core-261ba493846e


.. contents::
   :depth: 3

HTTP/2 in Node.js 10
=====================

The implementation of HTTP/2 in Node.js landed in Node.js 8 LTS, as of
Node.js 8.4.0. That said, there hasn’t been much discourse around or a
major move towards adoption of the new implementation beyond the bounds
of the Node.js core team.

The HTTP/2 implementation is a pretty great addition to Node.js core,
and is,in my opinion, important for the continued evolution of Node.js
for web applications and the web platform.

For more context on the release and usage of HTTP/2, checkout James
Snell's `post on the subject`_


.. _`post on the subject`:  https://medium.com/the-node-js-collection/say-hello-to-http-2-for-node-js-core-261ba493846e


ESM and Node.js
=================

ECMAScript Modules (a.k.a. ES Modules or ESM) are one of the most
challenging and important hurdles for Node.js in the coming future.

ECMAScript, which is what we're actually using when we use "JavaScript™",
outlined its own modules system ECMAScript 2015 (ES6) spec.

The new, native implementation is at odds with how Node.js has implemented
modules. This has caused a significant amount of discourse, both within
the Node.js project and the broader JavaScript community, who now
largely use both Node.js and npm as a platform for ecosystem tooling
and module delivery.

We’re not going to see a full implementation of ESM in Node.js v10,
but we are seeing continuous iteration and development in this area.

The Modules Team was spun up a few months ago, and has been actively
working on discussing the needs and implementation details around Node.js
and ESM. This team is one of the largest active teams in Node.js, with
over 30 active contributors.

For more info on ES Modules and Node.js, check out Myles Borins post
on the subject.
