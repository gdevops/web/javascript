.. index::
   pair: Versions ; Nodejs

.. _nodejs_versions:

=====================
nodejs versions
=====================

.. seealso::

   - https://github.com/nodejs/Release
   - https://github.com/nodejs/Release#lts-schedule

.. toctree::
   :maxdepth: 5

   15.8.0/15.8.0
   14.3.0/14.3.0
   14.2.0/14.2.0
   14.0/14.0
   13.9.0/13.9.0
   13.2.0/13.2.0
   12.0.0/12.0.0
   11.14.0/11.14.0
   11.0.0/11.0.0
   10.0.0/10.0.0
