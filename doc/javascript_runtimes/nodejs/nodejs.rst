.. index::
   pair: Javascript ; Nodejs

.. _nodejs:

=====================
**nodejs**
=====================


- https://fr.wikipedia.org/wiki/Node.js
- https://github.com/nodejs/node
- https://x.com/nodesource
- https://x.com/nodejs


.. figure:: Node.js_logo.svg.png
   :align: center

.. toctree::
   :maxdepth: 5

   definition/definition
   versions/versions
