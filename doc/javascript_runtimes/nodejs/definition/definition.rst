

.. index::
   pair: Nodejs ; Definition


.. _nodejs_def:

==========================
Nodejs definition
==========================

.. contents::
   :depth: 3

English wikipedia definition
=============================

.. seealso::

   - https://en.wikipedia.org/wiki/Node.js

Node.js is an open-source, cross-platform JavaScript run-time environment that
executes JavaScript code outside of a browser.

Node.js lets developers use JavaScript to write command line tools and for
server-side scripting—running scripts server-side to produce dynamic web page
content before the page is sent to the user's web browser.

Consequently, Node.js represents a "JavaScript everywhere" paradigm, unifying
web application development around a single programming language, rather
than different languages for server side and client side scripts.

Though .js is the standard filename extension for JavaScript code, the name
"Node.js" does not refer to a particular file in this context and is merely
the name of the product.

Node.js has an event-driven architecture capable of asynchronous I/O.

These design choices aim to optimize throughput and scalability in web
applications with many input/output operations, as well as for real-time Web
applications (e.g., real-time communication programs and browser games).

The Node.js distributed development project, governed by the Node.js Foundation,
is facilitated by the Linux Foundation's Collaborative Projects program.


French wikipedia definition
=============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Node.js


Node.js est une plateforme logicielle libre et événementielle en JavaScript
orientée vers les applications réseau qui doivent pouvoir monter en charge.

Elle utilise la machine virtuelle V8 et implémente sous licence MIT les
spécifications CommonJS.

**Parmi les modules natifs de Node.js, on retrouve http qui permet le développement
de serveur HTTP**.

Il est donc possible de se passer de serveurs web tels que Nginx ou Apache
lors du déploiement de sites et d'applications web développés avec Node.js.

Concrètement, Node.js est un environnement bas niveau permettant l’exécution
de JavaScript côté serveur.
