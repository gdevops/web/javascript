.. index::
   pair: Javascript runtime; bun
   ! bun

.. _bun:

===============================================================
**Bun is a modern JavaScript runtime like Node or Deno**
===============================================================

- https://x.com/jarredsumner
- https://github.com/Jarred-Sumner/bun


.. figure:: images/logo.png
   :align: center

Description
==============

bun is a new:

- JavaScript runtime with Web APIs like fetch, WebSocket, and several
  more builtin. bun embeds JavaScriptCore, which tends to be faster
  and more memory efficient than more popular engines like V8
  (though harder to embed)
- JavaScript/TypeScript/JSX transpiler
- JavaScript & CSS bundler
- Task runner for package.json scripts
- npm-compatible package manager

All in one fast & easy-to-use tool. Instead of 1,000 node_modules for
development, you only need bun.


How does Bun work ?
=======================

Bun.js uses the JavaScriptCore engine, which tends to start and perform
a little faster than more traditional choices like V8.
Bun is written in `Zig <https://ziglang.org/>`_, a low-level programming
language with manual memory management.

Most of Bun is written from scratch including the JSX/TypeScript transpiler,
npm client, bundler, SQLite client, HTTP client, WebSocket client and more.

Why is Bun fast ?
=====================

An enormous amount of time spent profiling, benchmarking and optimizing things.

The answer is different for every part of Bun, but one general theme:
zig's low-level control over memory and lack of hidden control flow makes
it much simpler to write fast software.
