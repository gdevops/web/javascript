.. index::
   pair: Javascript ; Typescript

.. _typescript:

=====================
Typescript
=====================

.. seealso::

   - https://www.typescriptlang.org/
   - https://github.com/Microsoft/TypeScript
   - https://x.com/typescript



.. toctree::
   :maxdepth: 3

   versions/versions
