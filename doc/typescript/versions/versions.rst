.. index::
   pair: Versions ; Typescript

.. _typescript_versions:

=====================
Typescript versions
=====================

.. seealso::

   - https://x.com/drosenwasser

.. toctree::
   :maxdepth: 3

   3.9.2/3.9.2
   3.8.2/3.8.2
   3.5.2/3.5.2
   3.4/3.4
   3.1/3.1
