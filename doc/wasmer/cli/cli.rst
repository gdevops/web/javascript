

.. index::
   pair: CLI ; Wasmer Javascript


.. _wasmer_javascript_cli:

====================================
Wasmer Javascript CLI (2019-11-05)
====================================


.. seealso::

   - https://x.com/wasmerio/status/1191490980777623554?s=20

::

    Wasmer
    @wasmerio
    ·
    8h
    🚀
     We are super excited to announce the Wasmer #JavaScript CLI!

    With it, you can now run any WebAssembly WASI files directly in Node
    ✨


    👇

    npm install -g @wasmer/cli
    wasmer-js run cowsay.wasm Hello
