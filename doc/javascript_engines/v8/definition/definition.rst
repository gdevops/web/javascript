
.. _v8_def:

=====================
v8 definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Chrome_V8
   - https://v8.dev/docs

.. contents::
   :depth: 3


What is V8 ?
===============

V8 is Google’s open source high-performance JavaScript and WebAssembly
engine, written in C++.

It is used in Chrome and in Node.js, among others.

It implements ECMAScript and WebAssembly, and runs on Windows 7 or later,
macOS 10.12+, and Linux systems that use x64, IA-32, ARM, or MIPS processors.

V8 can run standalone, or can be embedded into any C++ application.

Documentation
===============

.. seealso::

   - https://v8.dev/docs

V8 is Google’s open source high-performance JavaScript and WebAssembly
engine, written in C++. It is used in Chrome and in Node.js, among others.

This documentation is aimed at C++ developers who want to use V8 in
their applications, as well as anyone interested in V8’s design and
performance.

This document introduces you to V8, while the remaining documentation
shows you how to use V8 in your code and describes some of its design
details, as well as providing a set of JavaScript benchmarks for
measuring V8’s performance.

About V8
=========

V8 implements ECMAScript and WebAssembly, and runs on Windows 7 or later,
macOS 10.12+, and Linux systems that use x64, IA-32, ARM, or MIPS processors.

V8 can run standalone, or can be embedded into any C++ application.

V8 compiles and executes JavaScript source code, handles memory allocation
for objects, and garbage collects objects it no longer needs.

V8’s stop-the-world, generational, accurate garbage collector is one of
the keys to V8’s performance.

JavaScript is commonly used for client-side scripting in a browser,
being used to manipulate Document Object Model (DOM) objects for example.

The DOM is not, however, typically provided by the JavaScript engine
but instead by a browser.

The same is true of V8 — Google Chrome provides the DOM. V8 does however
provide all the data types, operators, objects and functions specified
in the ECMA standard.

V8 enables any C++ application to expose its own objects and functions
to JavaScript code.

It’s up to you to decide on the objects and functions you would like
to expose to JavaScript.


Wikipedia definition
=====================


Chrome V8, or simply V8, is an open-source JavaScript engine developed
by The Chromium Project for Google Chrome and Chromium web browsers.

The project’s creator is Lars Bak.

The first version of the V8 engine was released at the same time as the
first version of Chrome: September 2, 2008. It has also been used in
Couchbase, MongoDB and Node.js that are used server-side.

V8 compiles JavaScript directly to native machine code before executing
it, instead of more traditional techniques such as interpreting bytecode
or compiling the whole program to machine code and executing it from a
filesystem.

The compiled code is additionally optimized (and re-optimized) dynamically
at runtime, based on heuristics of the code's execution profile.

Optimization techniques used include inlining, elision of expensive
runtime properties, and inline caching. The garbage collector is a
generational incremental collector.

V8 can compile to x86, ARM or MIPS instruction set architectures in
both their 32- and 64-bit editions; as well, it has been ported to
PowerPC and IBM s390 for use in servers.
