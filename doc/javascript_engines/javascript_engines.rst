.. index::
   pair: Javascript ; Engines


.. _javascript_engines:

=====================
Javascript engines
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Comparison_of_JavaScript_engines

.. toctree::
   :maxdepth: 3

   spidermonkey/spidermonkey
   v8/v8
