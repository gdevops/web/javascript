.. index::
   pair: Javascript Engine; SpiderMonkey

.. _spidermonkey:

=====================================
**SpiderMonkey** Javascript engine
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey
   - https://x.com/SpiderMonkeyJS
   - https://x.com/FirefoxNightly
