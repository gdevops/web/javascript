.. index::
   pair: Javascript ; People

.. _javascript_people:

=======================
Javascript people
=======================

.. toctree::
   :maxdepth: 3

   axel_rauschmayer/axel_rauschmayer
   chris_ferdinandi/chris_ferdinandi
   flavio_copes/flavio_copes
   ryan_dahl/ryan_dahl
