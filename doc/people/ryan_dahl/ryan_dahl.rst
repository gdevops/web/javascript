.. index::
   pair: Javascript ; Ryan Dahl
   ! Ryan Dahl

.. _ryan_dahl:

=======================
**Ryan Dahl**
=======================

.. seealso::

   - https://github.com/ry
   - https://github.com/denoland/deno_website2
   - https://github.com/denoland/deno
   - https://tinyclouds.org/
   - https://en.wikipedia.org/wiki/Ryan_Dahl
   - https://github.com/ry?from=2020-05-01&to=2020-05-11&org=denoland&year_list=1


.. contents::
   :depth: 3



Wikipedia article
===================

Early life and education
-----------------------------

Dahl (born in 1981) grew up in San Diego, California.

His mother got him an Apple IIc when he was six years old, one of his
first experiences with technology. Ryan attended a community college in
San Diego and later transferred into UC San Diego where he studied
mathematics. He went on to attend grad school for Mathematics at the
University of Rochester where he studied algebraic topology, which he
once found "very abstract and beautiful" for a couple of years but later
got bored of it because "it was not so applicable to real life."

Once he realized that he did not want to be a mathematician for the rest
of his life, he dropped out of the PhD program and bought a one-way
ticket to South America and lived there for a year, where he found a
job as a web developer.

He worked on a **Ruby on Rails website** for a snowboard company


Deno
------

In a talk on "10 Things I Regret About Node.js" Dahl announced in
2018 Deno, a secure JavaScript/TypeScript runtime built with V8, Rust,
and Tokio.


Wiki2.org
==========

.. seealso::

   - https://wiki2.org/en/Ryan_Dahl

Articles
=========

2019
-----

.. seealso::

   - https://www.jesuisundev.com/deno-le-nouveau-nodejs/

2017
-----

.. seealso::

   - https://mappingthejourney.com/single-post/2017/08/31/episode-8-interview-with-ryan-dahl-creator-of-nodejs/





Denoland
=========

.. seealso::

   - https://github.com/denoland
