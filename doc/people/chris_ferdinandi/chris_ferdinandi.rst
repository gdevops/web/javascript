.. index::
   pair: Javascript ; ChrisFerdinandi
   ! ChrisFerdinandi

.. _chris_ferdinandi:

===============================================================================
**ChrisFerdinandi** The vanilla JS guy (https://x.com/ChrisFerdinandi)
===============================================================================

.. seealso::

   - https://x.com/ChrisFerdinandi
