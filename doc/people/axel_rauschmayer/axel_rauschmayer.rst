.. index::
   pair: Javascript ; Axel Rauschmayer

.. _axel_rauschmayer:

=======================
**Axel Rauschmayer**
=======================

.. seealso::

   - https://x.com/rauschma
   - https://2ality.com/
   - https://exploringjs.com/


Books
=======

.. seealso::

   - https://exploringjs.com/
