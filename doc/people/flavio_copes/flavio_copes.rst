.. index::
   pair: Javascript ; Flavio Copes
   ! Flavio Copes

.. _flavio_copes:

=======================
**Flavio Copes**
=======================

.. seealso::

   - https://flaviocopes.com/
   - https://x.com/flaviocopes
